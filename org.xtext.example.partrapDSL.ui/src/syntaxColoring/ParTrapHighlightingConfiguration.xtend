/*-
 * #%L
 * This file is part of Partrap IDE
 * Please visit http://vasco.imag.fr/tools/partrap/ for further information
 * 
 * Authors : Yoann Blein , Ansem Ben Cheikh 
 *           Laboratoire d'Informatique de Grenoble, Team VASCO
 * %-
 * Copyright (C) 2017 - 2019 University of Grenoble Alpes
 * %-
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package syntaxColoring

import org.eclipse.xtext.ui.editor.syntaxcoloring.IHighlightingConfigurationAcceptor
import org.eclipse.xtext.ui.editor.utils.TextStyle
import org.eclipse.swt.graphics.RGB
import org.eclipse.swt.SWT
import org.eclipse.xtext.ui.editor.syntaxcoloring.IHighlightingConfiguration

class ParTrapHighlightingConfiguration implements IHighlightingConfiguration{
	
	public static final String PYTHON_ID = "python";
    public static final String NUMBER_ID = "number";
    public static final String DEFAULT_ID = "default";
    public static final String KEYWORD_ID = "keyword";
    public static final String METHOD_ID = "method";
    public static final String PROP_ID = "property";
    public static final String STRING_ID = "string";
    public static final String COMMENT_ID = "comment";
	
	 
	
    
	
	override void configure(IHighlightingConfigurationAcceptor acceptor) {
		
			
		acceptor.acceptDefaultHighlighting(PYTHON_ID, "python", pythonTextStyle());
        acceptor.acceptDefaultHighlighting(NUMBER_ID, "Number", numberTextStyle());
        acceptor.acceptDefaultHighlighting(DEFAULT_ID, "Default", defaultTextStyle());
        acceptor.acceptDefaultHighlighting(KEYWORD_ID, "Keyword", keywordTextStyle());
        acceptor.acceptDefaultHighlighting(METHOD_ID, "Method", methodTextStyle());
        acceptor.acceptDefaultHighlighting(PROP_ID, "property", propTextStyle());
        acceptor.acceptDefaultHighlighting(STRING_ID, "string", stringTextStyle());
        acceptor.acceptDefaultHighlighting(COMMENT_ID, "comment", commentTextStyle());
	}
	
	def TextStyle pythonTextStyle() {
        var textStyle = new TextStyle();
        textStyle.setColor(new RGB(0,90,70));
        return textStyle;
    }

    def TextStyle numberTextStyle() {
       	var TextStyle textStyle = new TextStyle();
        textStyle.setColor(new RGB(127, 127, 127));
        return textStyle;
    } 
     def TextStyle stringTextStyle() {
       	var TextStyle textStyle = new TextStyle();
        textStyle.setColor(new RGB(0,0,200));
        return textStyle;
    } 
    
   def TextStyle defaultTextStyle() {
        var textStyle = new TextStyle();
        textStyle.setColor(new RGB(0, 0, 0));
        return textStyle;
    }

    def  TextStyle keywordTextStyle() {
        var textStyle = defaultTextStyle().copy();
        textStyle.setColor(new RGB(127, 0, 85));
        textStyle.setStyle(SWT.BOLD);
        return textStyle;
    }

    def TextStyle methodTextStyle() {
        var textStyle = defaultTextStyle().copy();
        textStyle.setColor(new RGB(85, 0, 127));
        textStyle.setStyle(SWT.ITALIC);
        return textStyle;
    }
     def TextStyle propTextStyle() {
        var textStyle = defaultTextStyle().copy();
        textStyle.setColor(new RGB(205, 92, 92));
        textStyle.setStyle(SWT.BOLD);
        return textStyle;
    }
    def TextStyle commentTextStyle() {
       	var TextStyle textStyle = new TextStyle();
        textStyle.setColor(new RGB(150,193,158));
        return textStyle;
    }

}
