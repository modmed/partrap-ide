/*-
 * #%L
 * This file is part of Partrap IDE
 * Please visit http://vasco.imag.fr/tools/partrap/ for further information
 * 
 * Authors : Yoann Blein , Ansem Ben Cheikh 
 *           Laboratoire d'Informatique de Grenoble, Team VASCO
 * %-
 * Copyright (C) 2017 - 2019 University of Grenoble Alpes
 * %-
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.xtext.example.partrapDSL.ui

import java.io.File
import java.io.PrintStream
import java.util.logging.Level
import java.util.logging.Logger
import org.eclipse.core.commands.AbstractHandler
import org.eclipse.core.commands.ExecutionEvent
import org.eclipse.core.commands.ExecutionException
import org.eclipse.core.commands.IHandler
import org.eclipse.core.resources.IFile
import org.eclipse.core.resources.IProject
import org.eclipse.ui.IEditorPart
import org.eclipse.ui.console.MessageConsole
import org.eclipse.ui.handlers.HandlerUtil
import org.eclipse.xtext.resource.XtextResource
import org.eclipse.xtext.ui.editor.XtextEditor
import org.eclipse.xtext.util.concurrent.IUnitOfWork
import org.xtext.example.partrapDSL.interpreter.ParTrapInterpreter
import org.xtext.example.partrapDSL.interpreter.PartrapInterpreterWithoutPython
import org.xtext.example.partrapDSL.parTrap.Model
import partrap.util.MyFormatter
import partrap.util.My_interprete_ConsoleHandler
//import partrap.util.ConsoleOutputCapturer
import java.io.FilterOutputStream
import java.io.ByteArrayOutputStream
import java.nio.charset.StandardCharsets
import org.eclipse.core.runtime.IPath

public class InterpretCodeHandler extends AbstractHandler implements IHandler {
	
    protected PrintStream parout;

	protected PrintStream parerr;
	

    
   override public Object execute(ExecutionEvent event) throws ExecutionException {
   	
   	var MessageConsole parConsole = ParLangConsoleUtils.getParLangConsole();
   //val IConsole iconsole = ParLangConsoleUtils.igetparLangConsole()
   //	val IConsole parConsole = BeeLangConsoleUtils.getparLangConsole()

		parout = ParLangConsoleUtils.getConsoleOutputStream(parConsole);

		parerr = ParLangConsoleUtils.getConsoleErrorStream(parConsole);

		try {
			System.out.flush
			parConsole.clearConsole
			parConsole.activate
			parout.println("Partrap: Interpreting Properties...");
			var IEditorPart activeEditor = HandlerUtil.getActiveEditor(event);
        	var IFile file =  activeEditor.getEditorInput().getAdapter(typeof(IFile)) as IFile;
        	if (file !== null) {
            	var IProject project = file.getProject();
            	val String mypath = project.getLocationURI().normalize().path;
            	mypath.replace("\\","/")
				val File directory = new File(mypath+"/python-utils");
           		if (activeEditor instanceof XtextEditor) {
           			//var ConsoleOutputCapturer capturer= new ConsoleOutputCapturer()
           			//capturer.start()
           			(activeEditor as XtextEditor).getDocument().readOnly(new IUnitOfWork<Boolean, XtextResource>() {
						override public Boolean exec(XtextResource state) throws Exception {
                    		var Model model =  state.getContents().get(0) as Model;
                    		
                    		var My_interprete_ConsoleHandler chh = new My_interprete_ConsoleHandler(parout, parerr);
							chh.setLevel(Level.CONFIG); // pour n'accepter que les message de niveau CONFIG et plus haut
							chh.setFormatter(new MyFormatter());
							
							if(directory.exists()){
                    			var ParTrapInterpreter inter= new ParTrapInterpreter();
                    			var  Logger loggers= inter.logger
                    			loggers.useParentHandlers = false
                    			loggers.addHandler(chh)
                    			println(loggers.handlers)
                    			inter.eval(model, mypath);
                    			chh.close
                    			return Boolean.TRUE;
                    		}
                    		else{
                    			var PartrapInterpreterWithoutPython inter= new PartrapInterpreterWithoutPython();
                    			var  Logger loggers= inter.logger
                    			loggers.useParentHandlers = false
                    			loggers.addHandler(chh)
                    			println(loggers.handlers)
                    			inter.eval(model, mypath);
                    			chh.close
                    			return Boolean.TRUE;
                    		}
                    }
                });
                var PrintStream outStream = new PrintStream(new FilterOutputStream(System.err), true);
                var ByteArrayOutputStream baos = new ByteArrayOutputStream();
				outStream = new PrintStream(baos);
                 parerr.println( new String(baos.toByteArray(), StandardCharsets.UTF_8))
                 outStream.close
                 baos.close

            }
        }
		}
		finally {
			try {
				parout.close();
				parout = null;
			}
			catch(Throwable t) {
			}
			try {
				parerr.close();
				parerr = null;
			}
			catch(Throwable t) {
			}
			
			}
			return null;
    }


    override public boolean isEnabled() {
        return true;
    }

}
