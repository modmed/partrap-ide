/*-
 * #%L
 * This file is part of Partrap IDE
 * Please visit http://vasco.imag.fr/tools/partrap/ for further information
 * 
 * Authors : Yoann Blein , Ansem Ben Cheikh 
 *           Laboratoire d'Informatique de Grenoble, Team VASCO
 * %-
 * Copyright (C) 2017 - 2019 University of Grenoble Alpes
 * %-
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.xtext.example.partrapDSL.ui;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.xtext.builder.EclipseResourceFileSystemAccess2;
import org.eclipse.xtext.resource.IResourceDescriptions;
import org.eclipse.xtext.ui.resource.IResourceSetProvider;
import org.eclipse.xtext.xbase.compiler.JvmModelGenerator;

import com.google.inject.Inject;
import com.google.inject.Provider;;


@SuppressWarnings("restriction")
public class Load_trace_file extends AbstractHandler implements IHandler{
	
	@Inject
	JvmModelGenerator jvmModelGenerator;
	@Inject
    private Provider<EclipseResourceFileSystemAccess2> fileAccessProvider;
     
    @Inject
    IResourceDescriptions resourceDescriptions;
     
    @Inject
    IResourceSetProvider resourceSetProvider;
    
    private Font font = new Font("SanSerif",Font.PLAIN,15);

public Object execute(ExecutionEvent event) throws ExecutionException{
	// TODO Auto-generated method stub
	String path= this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
	path= path.substring(0, path.length()-4);
	    JFileChooser chooser = chooser("Choose a Trace file or a directory");//choose a trace file or a directory of traces
	    ISelection selection = HandlerUtil.getCurrentSelection(event);
        if (selection instanceof IStructuredSelection) {
            IStructuredSelection structuredSelection = (IStructuredSelection) selection;
            Object firstElement = structuredSelection.getFirstElement();
            if (firstElement instanceof IFile) {
                IFile file = (IFile) firstElement;
                IProject project = file.getProject();
                path = project.getLocationURI().normalize().getPath();
                IFolder srcGenFolder = project.getFolder("src-gen");
                if (!srcGenFolder.exists()) {
                    try {
                        srcGenFolder.create(true, true,
                                new NullProgressMonitor());
                    } catch (CoreException e) {
                        return null;
                    }
                }
            try {
            	File myfile = chooser.getSelectedFile();
            	if (myfile.isDirectory()){
        			//var filter = new FileNameExtensionFilter("Json", "json");
        			 File[] file_list= myfile.listFiles(new FilenameFilter() {
        				 @Override
        				 public boolean accept(File file, String name) {
        					 return name.toLowerCase().endsWith(".json");
        				 }
        			 });
        			 // while used to guarantee the choice of a directory containing json files
        			 while(file_list.length==0 && myfile.isDirectory()) {
        				 chooser = chooser("This is a not a valid Trace directory : choose a Trace file or a directory");
        				 myfile = chooser.getSelectedFile();
     	            	if (myfile.isDirectory()){
     	        			//var filter = new FileNameExtensionFilter("Json", "json");
     	            		file_list=null;
     	        			 file_list= myfile.listFiles(new FilenameFilter() {
     	        				 @Override
     	        				 public boolean accept(File file, String name) {
     	        					 return name.toLowerCase().endsWith(".json");
     	        				 }
     	        			 });
     	            	}
        			 }
            	}
            		        		
            	String filepath= chooser.getSelectedFile().getAbsolutePath();
        		String filepath2=filepath.replace("\\","/");
        		
        		path = path +"/trace_files.txt";
        		System.out.println("the path for traces is : "+path);
        		FileWriter fw= new FileWriter(path);
        		fw.write("");
        		
                BufferedWriter out = new BufferedWriter(fw);
                out.flush();
                out.write(filepath2);
                out.close();
            }
            catch (IOException e)
            {
                System.out.println("Exception ");       
            }
            
           
                    
                    final EclipseResourceFileSystemAccess2 fsa = fileAccessProvider.get();
                    fsa.setOutputPath("src-gen");
                    fsa.setMonitor(new NullProgressMonitor());
                    fsa.setProject(project);
                     
                    URI uri = URI.createPlatformResourceURI(file.getFullPath().toString(), true);
                    ResourceSet rs = resourceSetProvider.get(project);
                    Resource r = rs.getResource(uri, true);
                    jvmModelGenerator.doGenerate(r, fsa);
           
        }
            
        }
        return null;
}
@Override
public boolean isEnabled() {
    return true;
}

private void setFileChooserFont(Component[] comp)
{  
  for(int x = 0; x < comp.length; x++)  
  {  
    if(comp[x] instanceof Container) setFileChooserFont(((Container)comp[x]).getComponents());  
    try{comp[x].setFont(font);}  
    catch(Exception e){}//do nothing  
  }  
}  
private JFileChooser chooser(String message){
	
	JFileChooser chooser = new JFileChooser();
	chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
    FileNameExtensionFilter filter = new FileNameExtensionFilter("Json", "json");
    chooser.setFileFilter(filter);
    chooser.setPreferredSize(new Dimension(1200, 800));
    chooser.setDialogTitle(message);
    setFileChooserFont(chooser.getComponents());
    int returnVal = chooser.showOpenDialog(null);
    if(returnVal == JFileChooser.APPROVE_OPTION) {
        System.out.println("You chose to open this file: " +
                chooser.getSelectedFile().getName());
        return chooser;
	
    }
    else {
    	return null;
    }
}

}

