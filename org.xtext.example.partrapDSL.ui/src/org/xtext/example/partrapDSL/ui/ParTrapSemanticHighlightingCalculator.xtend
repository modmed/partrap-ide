/*-
 * #%L
 * This file is part of Partrap IDE
 * Please visit http://vasco.imag.fr/tools/partrap/ for further information
 * 
 * Authors : Yoann Blein , Ansem Ben Cheikh 
 *           Laboratoire d'Informatique de Grenoble, Team VASCO
 * %-
 * Copyright (C) 2017 - 2019 University of Grenoble Alpes
 * %-
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.xtext.example.partrapDSL.ui

import org.eclipse.xtext.ide.editor.syntaxcoloring.DefaultSemanticHighlightingCalculator
import com.google.inject.Inject
import org.xtext.example.partrapDSL.services.ParTrapGrammarAccess
import org.eclipse.xtext.resource.XtextResource
import org.eclipse.xtext.ide.editor.syntaxcoloring.IHighlightedPositionAcceptor
import org.eclipse.xtext.util.CancelIndicator
import org.eclipse.xtext.nodemodel.INode
import syntaxColoring.ParTrapHighlightingConfiguration

class ParTrapSemanticHighlightingCalculator extends DefaultSemanticHighlightingCalculator{

@Inject
ParTrapGrammarAccess ga;

override void provideHighlightingFor(XtextResource resource, IHighlightedPositionAcceptor acceptor,
CancelIndicator cancelIndicator) {
var rootNode = resource.getParseResult().getRootNode();

for (INode node : rootNode.getAsTreeIterable()) {
if (node.getGrammarElement() === ga.terminal_propertyAccess.pyPYTHONTerminalRuleCall_1_1_3_1_0) {
acceptor.addPosition(node.getOffset(), node.getLength(), ParTrapHighlightingConfiguration.PYTHON_ID);
}
if (node.getGrammarElement() === ga.singleeventAccess.pyPYTHONTerminalRuleCall_1_1_1_1_0) {
acceptor.addPosition(node.getOffset(), node.getLength(), ParTrapHighlightingConfiguration.PYTHON_ID);
}
if (node.getGrammarElement() === ga.named_PropertyAccess.nameIDTerminalRuleCall_0_0) {
acceptor.addPosition(node.getOffset(), node.getLength(), ParTrapHighlightingConfiguration.PROP_ID);
}
if (node.getGrammarElement() === ga.ML_COMMENTRule) {
acceptor.addPosition(node.getOffset(), node.getLength(), ParTrapHighlightingConfiguration.COMMENT_ID);
}
if (node.getGrammarElement() === ga.SL_COMMENTRule) {
acceptor.addPosition(node.getOffset(), node.getLength(), ParTrapHighlightingConfiguration.COMMENT_ID);
}
}
super.provideHighlightingFor(resource, acceptor, cancelIndicator);
}

}
