/*-
 * #%L
 * This file is part of Partrap IDE
 * Please visit http://vasco.imag.fr/tools/partrap/ for further information
 * 
 * Authors : Yoann Blein , Ansem Ben Cheikh 
 *           Laboratoire d'Informatique de Grenoble, Team VASCO
 * %-
 * Copyright (C) 2017 - 2019 University of Grenoble Alpes
 * %-
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.xtext.example.partrapDSL.ui

import java.io.PrintStream;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.console.ConsolePlugin;
import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.console.IConsoleManager;
import org.eclipse.ui.console.MessageConsole;
import org.eclipse.ui.console.MessageConsoleStream;

public class ParLangConsoleUtils {

	public static final String PAR_LANG_CONSOLE = "Partrap Console";

	def public static MessageConsole getParLangConsole() {
		var IConsoleManager consoleManager = ConsolePlugin.getDefault().getConsoleManager()
		var  existing = consoleManager.getConsoles()

		for(var i = 0; i < existing.length; i++){
			
			if(PAR_LANG_CONSOLE.equals(existing.get(i).getName()))
				return  (existing.get(i) as MessageConsole)
				
				}

		// no console found, so create a new one
		var MessageConsole ParLangConsole = new MessageConsole(PAR_LANG_CONSOLE, null)
		// TODO: preference page defining the console colors
		
		var IConsole[] x= #{ParLangConsole}
		consoleManager.addConsoles(x)
		return ParLangConsole;
	
	
	}
	def public static IConsole igetparLangConsole() {
		var IConsoleManager consoleManager = ConsolePlugin.getDefault().getConsoleManager()
		var  existing = consoleManager.getConsoles()

		for(var i = 0; i < existing.length; i++){
			
			if(PAR_LANG_CONSOLE.equals(existing.get(i).getName()))
				return  (existing.get(i))
				
				}

		// no console found, so create a new one
		var IConsole parLangConsole = new MessageConsole(PAR_LANG_CONSOLE, null)
		// TODO: preference page defining the console colors
		// beeLangConsole.setBackground(???);
		var IConsole[] x= #{parLangConsole}
		consoleManager.addConsoles(x)
		return parLangConsole;
	
	
	}

	def public static PrintStream getConsoleErrorStream(MessageConsole console) {
		var MessageConsoleStream stream = console.newMessageStream();
		// TODO: preference page defining the console colors
		var Display display = PlatformUI.getWorkbench().getDisplay();
		stream.setColor(display.getSystemColor(SWT.COLOR_RED));
		return new PrintStream(stream);
	}

	def public static PrintStream getConsoleOutputStream(MessageConsole console) {
		var MessageConsoleStream stream = console.newMessageStream();
		// TODO: preference page defining the console colors
		// stream.setColor(???);
		return new PrintStream(stream);
	}

}
