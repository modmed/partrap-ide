###
# #%L
# This file is part of Partrap IDE
# Please visit http://vasco.imag.fr/tools/partrap/ for further information
# 
# Authors : Yoann Blein , Ansem Ben Cheikh 
#           Laboratoire d'Informatique de Grenoble, Team VASCO
# %-
# Copyright (C) 2017 - 2019 University of Grenoble Alpes
# %-
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Lesser Public License for more details.
# 
# You should have received a copy of the GNU General Lesser Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/lgpl-3.0.html>.
# #L%
###
import math
import ast

def Interpreter(d, ss):
    #d_list = d.replace("=",":")
    mydict=ast.literal_eval(d)
    if type(mydict)==dict:
        for x in mydict.keys():
            s= objectview(mydict[x])
            globals()[x]= s
        print(globals())
        st= ss
        c=eval(st)
        bo=""
        if type(c)==str:
            bo=ast.literal_eval(c)
        else:
            bo=c
        print(bo)
        print(type(bo))
        return(bo)

class objectview(object):
    def __init__(self, d):
        self.__dict__ = d
