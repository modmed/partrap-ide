/*-
 * #%L
 * This file is part of Partrap IDE
 * Please visit http://vasco.imag.fr/tools/partrap/ for further information
 * 
 * Authors : Yoann Blein , Ansem Ben Cheikh 
 *           Laboratoire d'Informatique de Grenoble, Team VASCO
 * %-
 * Copyright (C) 2017 - 2019 University of Grenoble Alpes
 * %-
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.xtext.example.partrapDSL.interpreter

import java.io.BufferedReader
import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.FileReader
import java.util.ArrayList
import java.util.HashMap
import java.util.Hashtable
import java.util.List
import java.util.logging.Level
import java.util.logging.Logger
import java.util.logging.StreamHandler
import jep.Jep
import jep.JepConfig
import jep.JepException
import org.eclipse.xtext.nodemodel.util.NodeModelUtils
import org.xtext.example.partrapDSL.parTrap.Add_expr
import org.xtext.example.partrapDSL.parTrap.And_expr
import org.xtext.example.partrapDSL.parTrap.And_property
import org.xtext.example.partrapDSL.parTrap.BoolConstant
import org.xtext.example.partrapDSL.parTrap.DoubleConstant
import org.xtext.example.partrapDSL.parTrap.Equal_expr
import org.xtext.example.partrapDSL.parTrap.Equiv_property
import org.xtext.example.partrapDSL.parTrap.Expression
import org.xtext.example.partrapDSL.parTrap.IDConstant
import org.xtext.example.partrapDSL.parTrap.Implies_property
import org.xtext.example.partrapDSL.parTrap.IntConstant
import org.xtext.example.partrapDSL.parTrap.Model
import org.xtext.example.partrapDSL.parTrap.Or_expr
import org.xtext.example.partrapDSL.parTrap.Or_property
import org.xtext.example.partrapDSL.parTrap.Par_property
import org.xtext.example.partrapDSL.parTrap.Pattern
import org.xtext.example.partrapDSL.parTrap.Property
import org.xtext.example.partrapDSL.parTrap.Quantified_property
import org.xtext.example.partrapDSL.parTrap.Record_expr
import org.xtext.example.partrapDSL.parTrap.Reference_expr
import org.xtext.example.partrapDSL.parTrap.Scope
import org.xtext.example.partrapDSL.parTrap.Scoped_property
import org.xtext.example.partrapDSL.parTrap.StringConstant
import org.xtext.example.partrapDSL.parTrap.Terminal
import org.xtext.example.partrapDSL.parTrap.Unary_expr
import org.xtext.example.partrapDSL.parTrap.Unary_property
import org.xtext.example.partrapDSL.parTrap.named_Property
import org.xtext.example.partrapDSL.parTrap.ref_named_Property
import partrap.util.And_Result
import partrap.util.Composite_Result
import partrap.util.Convert_occurrences
import partrap.util.Convertjsonlist
import partrap.util.Equiv_Result
import partrap.util.Evaluate_pyexpr
import partrap.util.Exists_Result
import partrap.util.Forall_Result
import partrap.util.IDuration
import partrap.util.IEvent
import partrap.util.IPattern
import partrap.util.IScope
import partrap.util.Implies_Result
import partrap.util.Load_Trace_file
import partrap.util.Long_Result_Explanation
import partrap.util.Low_Result
import partrap.util.MyConsoleHandler
import partrap.util.MyFormatter
import partrap.util.Not_Result
import partrap.util.Null_Result
import partrap.util.Occurrence
import partrap.util.Or_Result
import partrap.util.Prop_Result
import partrap.util.PyExpression_interpreter
import partrap.util.Scope_display
import partrap.util.Scopereturns
import partrap.util.Short_Result_Explanation
import partrap.util.TreeNode
import java.util.Map
import org.apache.commons.io.FileUtils
import org.apache.commons.io.FilenameUtils
import partrap.util.Create_result_file
import org.apache.commons.io.IOUtils
import java.io.InputStream

@SuppressWarnings("all")
class ParTrapInterpreter {
	public Hashtable<String, Object> variables = new Hashtable()// global variables declared in partrap properties
	public String mypath = null// the path of the runtime application used for documents loads and save
	public String[] package_py=null// list of the imported python packages
	public Jep jep = null// unique jep instance used buy the interpreter
	public Logger logger= Logger.getLogger("InterpreteLogger")
	public StringBuilder short_log= new StringBuilder
	public StringBuilder long_log= new StringBuilder
	public StringBuilder debug_log= new StringBuilder
	public StringBuilder html_result= new StringBuilder
	public String model_name= null
/**
	 * The dispatch method {@code infer} is called for each instance of the
	 * model element's type that is contained in a resource.
	 * */	
	@SuppressWarnings("all")
	def dispatch void eval(Model element, String thispath) throws JepException{
		var long startTime = System.nanoTime();
		if(System.getProperty("os.name").toLowerCase.contains("win")){
			mypath=thispath.substring(1,thispath.length)
				}
				println(System.getProperty("os.name").toLowerCase.contains("win")+ "  win res")
		mypath= mypath.replace("\\","/")
		mypath= mypath.replace("\\\\","/")
		model_name = element.eResource.URI.trimFileExtension.lastSegment
		/* Configure Logging */
		logger.setLevel(Level.ALL);
		logger.setUseParentHandlers(false);
		var MyConsoleHandler ch = new MyConsoleHandler();
		ch.setLevel(Level.CONFIG); // pour n'accepter que les message de niveau CONFIG et plus haut
		ch.setFormatter(new MyFormatter());
		logger.addHandler(ch);
		
		var File directory = new File(mypath+"/results");
		if (! directory.exists()){
			directory.mkdir();
		}
		var FileOutputStream out = new FileOutputStream(mypath+"/results/"+model_name+"_FINEtext.txt");
		var StreamHandler ssfine= new StreamHandler(out, new MyFormatter());
		ssfine.setLevel(Level.FINE); // pour n'accepter que les message de niveau >FINE
		logger.addHandler(ssfine);
		var FileOutputStream out2 = new FileOutputStream(mypath+"/results/"+model_name+"_FINESTtext.txt");
		var StreamHandler ssfinest= new StreamHandler(out2, new MyFormatter());
		ssfinest.setLevel(Level.FINEST); // pour n'accepter que les message de niveau >FINEST
		logger.addHandler(ssfinest);
		/* Configure html results file */
    	var InputStream template = this.class.getResourceAsStream("/result_template.html");
    	var String htmlString = IOUtils.toString(template);
		var title = model_name;
		htmlString = htmlString.replace("$title", title);
		htmlString = htmlString.replace("$summary", "");
					
		/* Load the trace file or create it if not exists */
		package_py=element.package;
		debug_log.append("Working path: "+thispath+"\n")
		var tracepath = mypath +"/trace_files.txt"
		println("tracepath "+tracepath)	
		println("mypath "+mypath)	
		/* Initializing JEP and calling Python imports */
		var String[] package_py=element.package;// list of the imported python packages
		var pypath = mypath+"/python-utils"
		println("pypath "+pypath)	
		 var JepConfig config = new JepConfig().setRedirectOutputStreams(true).setIncludePath(pypath);
	    jep = new Jep(config)
	    debug_log.append("Jep configuration completed: "+pypath+"\n")
	    if (package_py!==null) {
	    	for (String mod: package_py) {
	    		try{
	    			jep.eval("from "+mod+" import *")
	    			debug_log.append("Python package: "+mod+" successfully imported\n")
	    		}catch(JepException e) {
	    			logger.severe("Error when importing Python package: "+mod)
	    			logger.severe(e.message)
	    			return;
	    		}
	    	}
	    }
	    
	    /* Loading trace file or creating it if does not exist */
		var tfile= new File(tracepath)
		var boolean created = false 
		if(!tfile.exists){
			var vv= new Load_Trace_file()   
			created = vv.main(tracepath)
		}
		val chosen_file = new FileReader(tracepath)
		val BufferedReader br = new BufferedReader(chosen_file)
		val StringBuilder sb = new StringBuilder()
    	try {
        	var String line = br.readLine()
			sb.append(line)
        } finally {
        	br.close()
    	}
   		val String trace = sb.toString
   		debug_log.append("Ready to load trace file: "+trace+" \n")
		val file = new File(trace)
		
		/* check the selected type of file and create a tree of traces to evaluate them */
		var TreeNode<String> trace_tree= null;
		if (file.isDirectory){
			trace_tree= new TreeNode<String>(trace)
			trace_tree=listf(file.absolutePath, trace_tree)
			debug_log.append("It is a directory and the trace tree is successfully created\n")
		}
		else {
			if (file.isFile){
				trace_tree= new TreeNode<String>(file.parent);
				var HashMap<String, Boolean> leaf= new HashMap <String, Boolean>();
				leaf.put(trace, false);
				trace_tree.addleaf(leaf)
			}
		}
		logger.finer(debug_log.toString)
		debug_log= new StringBuilder
		/* Interprete each property for each trace file and putting results in "results" variable */
		var Hashtable<String, TreeNode<String>> results= new Hashtable<String, TreeNode<String>>()
		var vv= new Create_result_file()  
		var StringBuilder preresult_html= new StringBuilder();
		preresult_html.append("<table style=\"float: left\" border = \"1\"><tr><td>Traces:</td></tr>");
		preresult_html.append("<tr><td>Success Rate </td></tr>" );
		preresult_html.append(vv.create_trace_table(trace_tree, html_result).toString);
		preresult_html.append("</table>");
		for (named_property : element.named_property) {
			html_result= new StringBuilder
			short_log.append("************ property :\'"+named_property.name+"\' ************\n")
			debug_log.append("************ property :\'"+named_property.name+"\' ************\n")
			long_log.append("************ property :\'"+named_property.name+"\' ************\n")
			preresult_html.append("<table style=\"float: left\" border = \"1\"><tr><td><a href=\""+mypath+"/src/"+model_name+".partrap"+"\" style=\"visited.color:slateblue; color: slateblue;\">"+named_property.name+"</a></td></tr>");
			var new_trace_tree= execute_on_tree(named_property, trace_tree)
			var Hashtable<String, TreeNode<String>> unitresults= new Hashtable<String, TreeNode<String>>()
			unitresults.put(named_property.name, new_trace_tree)
			var float percent= vv.create_percentage(unitresults);
			preresult_html.append("<tr><td><font color=\"blue\" >"+percent+"%</font></td></tr>" );
			preresult_html.append(html_result.toString() );
			results.put(named_property.name, new_trace_tree)
			preresult_html.append("</table>");
		}
		html_result= preresult_html;
			
		
		var ipath = mypath +"/"+model_name+"_results.txt";
				
		/* Creating the result file */
		vv.create_from_tree(results,ipath)
		htmlString = htmlString.replace("$body", html_result);
		//StringBuilder sb_summary=create_result.create_from_tree2(results);
		//htmlString = htmlString.replace("$summary", sb_summary);
		var File newHtmlFile = new File(thispath+"/results/"+model_name+"/"+model_name+".html");
        FileUtils.writeStringToFile(newHtmlFile, htmlString);
		jep.close
		var long endTime = System.nanoTime;
		System.out.println("Took "+(endTime - startTime)/1000000 + " ms"); 
		logger.info("Took "+(endTime - startTime)/1000000 + " ms");
		ssfine.close
		ssfinest.close
		ch.close
		short_log= new StringBuilder
		long_log= new StringBuilder
		debug_log= new StringBuilder
	}
/****************************** */	
/** Interpreting a main property by type and contents: types are partrap grammar elements */
@SuppressWarnings("rawtypes")
	def private <T> boolean interprete(Property property, List<Hashtable> tracelist, List<Composite_Result> limitlist){
		var boolean result=true;
		debug_log.append("Interpreting: ")
		if(!limitlist.empty){
		if (property instanceof Scoped_property){
			debug_log.append("a scoped property")
			var Scope scope = property.scope
			var newlimitlist = scope.evaluates(tracelist, limitlist)
			var scoped_prop= property.property
			result = scoped_prop.interprete(tracelist,newlimitlist)
		}
		if (property instanceof Unary_property){
			debug_log.append("a unary property")
			var unary_prop= property.property
			var ishort_log= new StringBuilder
			var ilong_log= new StringBuilder
			for(ul:limitlist){
				var unary_list= new ArrayList<Composite_Result>;
				var Prop_Result prop_r= new Prop_Result(ul.scope, null, true)
				var Not_Result not_r= new Not_Result(prop_r, true)//default value for not properties 'true' updated if needed
				not_r.parent= ul;
				prop_r.parent= not_r;
				ul.add(not_r);
				unary_list.add(prop_r);
				short_log = new StringBuilder;
				long_log = new StringBuilder;
				var boolean iresult = !unary_prop.interprete(tracelist,unary_list);
				if(iresult){
					ilong_log.append("True because the inner property is false: --"+short_log.toString);
				}
				else{
					not_r.verdict=false
					ul.update_false_verdict;
					ishort_log.append("False because the inner property is true :\n--"+long_log.toString)
				}
				
				result = iresult&&result;
			}
			short_log=ishort_log;
			long_log=ilong_log;
			
			
		}
		if (property instanceof Equiv_property){
			debug_log.append("an Equiv Property")
			if(property.right!==null){
				var prop1= property.left
				var prop2 = property.right
				var left_list= new ArrayList<Composite_Result>;
				var right_list= new ArrayList<Composite_Result>;
				for(ul:limitlist){
					var Prop_Result prop_right= new Prop_Result(ul.scope, ul.iScope, true)
					var Prop_Result prop_left= new Prop_Result(ul.scope, ul.iScope, true)
					var Equiv_Result equiv_r= new Equiv_Result( prop_left,prop_right, true)
					equiv_r.parent= ul;
					ul.add(equiv_r);
					prop_right.parent= equiv_r
					prop_left.parent= equiv_r
					left_list.add(prop_left)
					right_list.add(prop_right)
					var boolean val1= prop1.interprete(tracelist,left_list) 
				var StringBuilder short_log1= short_log;
				var StringBuilder long_log1 = long_log;
				long_log = new StringBuilder();
				short_log = new StringBuilder();
				var boolean val2= prop2.interprete(tracelist,right_list) 
				var StringBuilder short_log2= short_log;
				var StringBuilder long_log2 = long_log;
				long_log = new StringBuilder();
				short_log = new StringBuilder();
				if(val1 && !val2){
					short_log.append(" First property in equiv statement is true while the second is false : \n--"+short_log2.toString());
				}
				else {
					if (!val1 && val2){
						short_log.append(" First property in equiv statement is false : \n "+short_log1 +"while the second is true \n"+"(The second property should be false)\n"+long_log2.toString);
					}
					if (!val1 && !val2){
						long_log.append(" The two properties are false : \n"+short_log1.toString+" \n And\n"+short_log2.toString);
					}
					if (val1 && val2){
						long_log.append("The two properties are true ")
						long_log.append("The first property is  true : \n"+long_log1.toString)
						long_log.append("The second property is  true : \n"+long_log2.toString)
					}
				}
				result = result&&((!val1 || val2) && (!val2 || val1))
				}
				
			}
			else{
				var prop1= property.left
				result = prop1.interprete(tracelist,limitlist)
			}
		}
		if (property instanceof Implies_property){
			debug_log.append("an Implies property")
			if(property.right!==null){
				var prop1= property.left
				var prop2 = property.right
				var left_list= new ArrayList<Composite_Result>;
				var right_list= new ArrayList<Composite_Result>;
				for(ul:limitlist){
					var Prop_Result prop_right= new Prop_Result(ul.scope, ul.iScope, true)
					var Prop_Result prop_left= new Prop_Result(ul.scope, ul.iScope, true)
					var Implies_Result implies_r= new Implies_Result( prop_left,prop_right, true)
					implies_r.parent= ul;
					ul.add(implies_r);
					prop_right.parent= implies_r
					prop_left.parent= implies_r
					left_list.add(prop_left)
					right_list.add(prop_right)
				}
				var boolean val1= prop1.interprete(tracelist,left_list) 
				var StringBuilder short_log1= short_log;
				var StringBuilder long_log1 = long_log;
				long_log = new StringBuilder();
				short_log = new StringBuilder;
				var boolean val2= prop2.interprete(tracelist,right_list) 
				var StringBuilder short_log2= short_log;
				var StringBuilder long_log2 = long_log;
				long_log = new StringBuilder();
				short_log = new StringBuilder;
				if(val1 && !val2){
					short_log.append(" First property in implies statement is true while the second is false \n"+short_log2.toString());
				}
				else{
					if(!val1 && !val2){
						long_log.append("The two properties are false : \n"+short_log1.toString+ " And \n"+ short_log2.toString)
					}
					if(val1 && val2){
						long_log.append("The two properties are true ")
						long_log.append("The first property is  true : \n"+long_log1.toString)
						long_log.append("The second property is  true : \n"+long_log2.toString)
					}
					if(!val1 && val2){
						long_log.append("The first property is false : \n"+short_log1.toString)
						long_log.append("The second property is  true : \n"+long_log2.toString)
					}
				}
				short_log = short_log1;
				result = !val1 || val2
			}
			else{
				var prop1= property.left
				result = prop1.interprete(tracelist,limitlist)
			}
		}
		if (property instanceof Or_property){
			debug_log.append("an Or property")
			if(property.right!==null){
				var prop1= property.left
				var prop2 = property.right
				var left_list= new ArrayList<Composite_Result>;
				var right_list= new ArrayList<Composite_Result>;
				for(ul:limitlist){
					var Prop_Result prop_right= new Prop_Result(ul.scope, ul.iScope, true)
					var Prop_Result prop_left= new Prop_Result(ul.scope, ul.iScope, true)
					var Or_Result or_r= new Or_Result( prop_left,prop_right, true)
					or_r.parent= ul;
					ul.add(or_r);
					prop_right.parent= or_r
					prop_left.parent= or_r
					left_list.add(prop_left)
					right_list.add(prop_right)
				}
				var boolean val1= prop1.interprete(tracelist,left_list) 
				var StringBuilder short_log1 = short_log;
				short_log = new StringBuilder();
				var StringBuilder long_log1 = long_log;
				long_log = new StringBuilder();
				var boolean val2= prop2.interprete(tracelist,right_list) 
				var StringBuilder short_log2 = short_log;
				var StringBuilder long_log2 = long_log;
				long_log = new StringBuilder();
				short_log = new StringBuilder();
				if(!val1 && !val2){
					short_log.append(short_log1.toString()+"\n OR \n"+short_log2.toString());
				}
				else{
					if(val1 && val2){
						long_log.append("The two properties are true \n")
						long_log.append("The first property is  true : \n"+long_log1.toString)
						long_log.append("The second property is  true : \n"+long_log2.toString)
					}
					if(val1 && !val2){
						long_log.append("The first property is  true : \n"+long_log1.toString)
						long_log.append("The second property is false :\n "+ short_log2.toString)
					}
					if(!val1 && val2){
						long_log.append("The first property is  false : \n"+short_log1.toString)
						long_log.append("The second property is true :\n "+ long_log2.toString)
					}
				}
				short_log = short_log1;
				result = val1 || val2
			}
			else{
				var prop1= property.left
				result = prop1.interprete(tracelist,limitlist)
			}
		}
		if (property instanceof And_property){
			debug_log.append("an And property")
			if(property.right!==null){
				var prop1= property.left
				var prop2 = property.right
				var left_list= new ArrayList<Composite_Result>;
				var right_list= new ArrayList<Composite_Result>;
				for(ul:limitlist){
					var Prop_Result prop_right= new Prop_Result(ul.scope, ul.iScope, true)
					var Prop_Result prop_left= new Prop_Result(ul.scope, ul.iScope, true)
					var And_Result and_r= new And_Result( prop_left,prop_right, true)
					and_r.parent= ul;
					ul.add(and_r);
					prop_right.parent= and_r
					prop_left.parent= and_r
					left_list.add(prop_left)
					right_list.add(prop_right)
				}
				var boolean val1= prop1.interprete(tracelist,left_list) 
				var StringBuilder short_log1 = short_log;
				var StringBuilder long_log1 = long_log;
				short_log = new StringBuilder();
				long_log = new StringBuilder();
				var boolean val2= prop2.interprete(tracelist,right_list)
				var StringBuilder short_log2 = short_log; 
				var StringBuilder long_log2 = long_log;
				long_log = new StringBuilder();
				short_log = new StringBuilder();
				if(!val1 && !val2){
					short_log.append(short_log1.toString()+" AND \n"+short_log2.toString());
				}
				else{
					if(val1 && val2){
						long_log.append("The two properties are true \n")
						long_log.append("The first property is  true : \n"+long_log1.toString)
						long_log.append("The second property is  true : \n"+long_log2.toString)
					}
					if(val1 && !val2){
						long_log.append("The first property is  true : \n"+long_log1.toString)
						long_log.append("The second property is false :\n "+ short_log2.toString)
						short_log.append("The second property is false :\n --"+ short_log2.toString)
					}
					if(!val1 && val2){
						long_log.append("The first property is  false : \n"+short_log1.toString)
						long_log.append("The second property is true :\n "+ long_log2.toString)
						short_log.append("The first property is false : \n --"+short_log1.toString)
					}
				}
				result = val1 && val2
			}
			else{
				var prop1= property.left
				result = prop1.interprete(tracelist,limitlist)
			}
		}
		
		if (property instanceof Quantified_property){
			debug_log.append("a Quantified property")
			var qproperty = property.property
			var id  = property.id
			var List<?> mylist= null
			var qresult = true;
			if (property.name.equals("exists")){
				qresult=false;
			}
			
			for (Composite_Result cr : limitlist) {
				var scopereturns= cr.scope
				var Hashtable<String, Hashtable> myenv =scopereturns.environment;
				if(property.expr!==null){
					var expr = property.expr
					mylist= expr.s_interprete(scopereturns.environment, variables, "List") as List<?>
			 	}
				else{
					if(property.py!==null){
						var expr = property.py
						var PyExpression_interpreter xi = new PyExpression_interpreter(jep)
						mylist= xi.main_list(expr, scopereturns.environment) as List<?>
			 		}
				}
				println("mylist : "+mylist)
			if(mylist!==null){
				//long_log.append("the list for \'"+property.name+"\' property is : "+ mylist.toString+"\n")
				debug_log.append("the list for \'"+property.name+"\' property is : "+ mylist.toString+"\n")
				var int j= mylist.size()
				var int i=0;
				
				if (qresult){/** Forall Property */
					var Forall_Result composite = new Forall_Result(cr.scope, cr.iScope, true, id);
					composite.parent=cr;
					cr.add(composite)
					while (i<j){//evaluate the property on each list element
						var Hashtable<String, Hashtable> newenv = new Hashtable<String, Hashtable>();
						newenv.putAll(myenv);
						// updating the variables list with the list element (id, value)
						variables.put(id, mylist.get(i))
						var prop_result= new Prop_Result(cr.scope, cr.iScope, true)
						prop_result.update_vars(id, mylist.get(i));
						prop_result.parent= composite
						composite.add(prop_result)
						var newcomposites = new ArrayList<Composite_Result>
						newcomposites.add(prop_result)
						var boolean newresult = qproperty.interprete(tracelist, newcomposites)
						qresult = newresult && qresult
						if(!newresult){
							short_log.append("where " + id + " = "+mylist.get(i)+"\n")
						}
						else{
							long_log.append("where " + id + " = "+mylist.get(i)+"\n")
						}
						i++
					}
				}
				else{/**   Exists Property */
					var Exists_Result composite = new Exists_Result(cr.scope, cr.iScope, true, id);
					composite.parent=cr;
					cr.add(composite)
					while (i<j){//evaluate the property on each list element
						var Hashtable<String, Hashtable> newenv = new Hashtable<String, Hashtable>();
						newenv.putAll(myenv);
						// updating the variables list with the list element (id, value)
						variables.put(id, mylist.get(i))
						var prop_result= new Prop_Result(cr.scope, cr.iScope, true)
						prop_result.update_vars(id, mylist.get(i));
						prop_result.parent= composite
						composite.add(prop_result)
						var newcomposites = new ArrayList<Composite_Result>
						newcomposites.add(prop_result)
						var boolean newresult = qproperty.interprete(tracelist, newcomposites)
						qresult = newresult || qresult
						if(!newresult){
							short_log.append("where " + id + " = "+mylist.get(i)+"\n")
						}
						else{
							long_log.append("where " + id + " = "+mylist.get(i)+"\n")
						}
						i++
					}
					
				}
				result = result && qresult
			}
			else{
				logger.warning("error in list definition: not found list")
			}
			
		}
		
		}
		if (property instanceof  ref_named_Property){
			debug_log.append("a Reference property")
			var named_prop = property as ref_named_Property
			var newcomposite= new ArrayList<Composite_Result>
			for (ref_result:limitlist){
				var prop_result= new Prop_Result(ref_result.scope, ref_result.iScope, true)
				prop_result.name= named_prop.named.name
				prop_result.parent= ref_result;
				ref_result.add(prop_result);
				newcomposite.add(prop_result);
			}
			result=named_prop.named.property.interprete(tracelist,newcomposite)
		}
		if (property instanceof  Pattern){
			debug_log.append("a pattern")
			var pattern= property as Pattern
			result = pattern.evaluates(tracelist, limitlist)
		}
		if (property instanceof  Par_property){
			debug_log.append("a parantheses property")
			var pprop= property.property
			result = pprop.interprete(tracelist,limitlist)
		}
		}
		return result
	}
	

	
/******************************* */	
@SuppressWarnings("rawtypes")
	def List<Composite_Result> interprete(Expression expr, List<Composite_Result> limitlist){
		var List<Composite_Result> checked_list = new ArrayList<Composite_Result>();
		if(limitlist!==null){
			debug_log.append("Interpreting an Expression: "+NodeModelUtils.getNode(expr).getText().trim())
			for (Composite_Result cr : limitlist) {
				var scopereturns=cr.scope
				if (scopereturns!==null){
					var Hashtable<String, Hashtable> myenv =scopereturns.environment;
					println("env :"+myenv)
					var result = expr.s_interprete(myenv,variables, "Boolean") as Boolean
					println("result :"+result)
					if (result!==null){
						if (result){
							println("true res :"+result)
							checked_list.add(cr)
						}
					}
				
				}
				
			}
		
		}
		return checked_list
		
	}
	
	/******************************* */	
@SuppressWarnings("rawtypes")
	def List<Scopereturns> rinterprete(Expression expr, List<Scopereturns> limitlist){
		var List<Scopereturns> checked_list = new ArrayList<Scopereturns>();
		if(limitlist!==null){
			debug_log.append("Interpreting an Expression: "+NodeModelUtils.getNode(expr).getText().trim())
			for (Scopereturns scopereturns : limitlist) {
				if (scopereturns!==null){
					var Hashtable<String, Hashtable> myenv =scopereturns.environment;
					var result = expr.s_interprete(myenv,variables, "Boolean") as Boolean
					if (result!==null){
						if (result){
							checked_list.add(scopereturns)
						}
					}
				
				}
			}
		}
		return checked_list
	}
	
/****************************** */	
/** Interpreting an expression on an environement. */
/** return type is an object: we mainly look for boolean results in event expressins 
 * and list results in quantified properties
 */
def private <T> Object  s_interprete(Expression expr, Hashtable<String,Hashtable> env, Hashtable variable, String aclass){
	debug_log.append("Sub Expression: "+NodeModelUtils.getNode(expr).getText().trim()+"| Xtext Type: "+expr.class)
	try{
		switch (expr){
			IDConstant:{
				var String id=expr.value
				if (variable.containsKey(id)){
					var value=  variable.get(id);
					return value
				}
				else {
					return id
				}
				}
				
			DoubleConstant:{
				var Double value = expr.value as Double
				return value
			}
			
			IntConstant:{
				if (aclass.equals("Double")){
					var value = new Double(expr.value)
					return value
				}
				else{
					var value = Long.valueOf(expr.value.longValue())
					return value
					}
			}
			
			StringConstant:{
					var String value = expr.value 
					return value
			}
			
			BoolConstant:{
					var Boolean value = Boolean.parseBoolean(expr.value)
					return value
			}
			
			And_expr:{
				if(expr.right!==null){
		     		var x= (expr.left).s_interprete(env, variable, "Boolean") 
		     		var y = (expr.right).s_interprete(env, variable, "Boolean") 
		     		if(x!==null && y!==null){
		     			return ((x as Boolean) && (y as Boolean))
					}
					else{
						return null
					}
					}
					else{
						return (expr.left.s_interprete(env, variable, "Object"))
					}
				}
				
			Or_expr:{
				if(expr.right!==null){
		     		var x= (expr.left).s_interprete(env, variable, "Boolean") 
		     		var y = (expr.right).s_interprete(env, variable, "Boolean") 
		     		if(x!==null && y!==null){
		     			return ((x as Boolean) || (y as Boolean))
					}
					else{
						return null
					}
				}
				else{
					return (expr.left.s_interprete(env, variable, "Object"))
				}
				}
				
			Equal_expr:{
				if(expr.right!==null){
		     		switch (expr.sign){
							case '>':	{
								var x= (expr.left).s_interprete(env, variable, "Double") 
		     					var y = (expr.right).s_interprete(env, variable, "Double") 
		     					if(x!==null && y!==null){
		     						return ((x as Double) > (y as Double))
								}
								else{
									return null
								}
							}
							case '<':	{
								var x= (expr.left).s_interprete(env, variable, "Double") 
		     					var y = (expr.right).s_interprete(env, variable, "Double") 
		     					if(x!==null && y!==null){
		     						return ((x as Double) < (y as Double))
								}
								else{
									return null
								}
							}
							case '>=':	{
								var x= (expr.left).s_interprete(env, variable, "Double") 
		     					var y = (expr.right).s_interprete(env, variable, "Double") 
		     					if(x!==null && y!==null){
		     						return ((x as Double) >= (y as Double))
								}
								else{
									return null
								}
							}
							case '<=':	{
								var x= (expr.left).s_interprete(env, variable, "Double") 
		     					var y = (expr.right).s_interprete(env, variable, "Double") 
		     					if(x!==null && y!==null){
		     						return ((x as Double) <= (y as Double))
								}
								else{
									return null
								}
							}
							case '==':	{
								var x= (expr.left).s_interprete(env, variable, "Object") 
		     					var y = (expr.right).s_interprete(env, variable, "Object") 
		     					if (x!==null && y!==null){
									return (x.equals(y))
								}
								else{
									return null
								}
							}
							case '!=':	{
								var x= (expr.left).s_interprete(env, variable, "Object") 
		     					var y = (expr.right).s_interprete(env, variable, "Object")
		     					if(x!==null && y!==null){
									return (!x.equals(y))
								}
								else{
									return null
								}
							}
						}
					}
					else{
						return (expr.left.s_interprete(env, variable, "Object"))
					}
				
				}
					
				Add_expr:{
					if(expr.right!==null){
		     			switch (expr.sign){
							case '+':	{
								var x= (expr.left).s_interprete(env, variable, "Double") 
		     					var y = (expr.right).s_interprete(env, variable, "Double") 
		     					if(x!==null && y!==null){
		     						return ((x as Double) + (y as Double))
								}
								else{
									return null
								}
							}
							case '-':	{
								var x= (expr.left).s_interprete(env, variable, "Double") 
		     					var y = (expr.right).s_interprete(env, variable, "Double") 
		     					if(x!==null && y!==null){
									return ((x as Double) - (y as Double))
								}
								else{
									return null
								}
							}
							case '%':	{
								var x= (expr.left).s_interprete(env, variable, "Double") 
		     					var y = (expr.right).s_interprete(env, variable, "Double") 
		     					if(x!==null && y!==null){
									return ((x as Double) % (y as Double))
								}
								else{
									return null
								}
							}
							case '/':	{
								var x= (expr.left).s_interprete(env, variable, "Double") 
		     					var y = (expr.right).s_interprete(env, variable, "Double") 
		     					if(x!==null && y!==null){
									return ((x as Double) / (y as Double))
								}
								else{
									return null
								}
							}
							case '*':	{
								var x= (expr.left).s_interprete(env, variable, "Double") 
		     					var y = (expr.right).s_interprete(env, variable, "Double") 
		     					if(x!==null && y!==null){
									return ((x as Double) > (y as Double))
								}
								else{
									return null
								}
							}
						}
					}
					else{
						return (expr.left.s_interprete(env, variable, "Object"))
					}
				}
				
				Unary_expr:{
					if(expr.sign!==null){
						switch(expr.sign){
							case 'not':{
								var x= expr.right_u.s_interprete(env, variable, "Boolean")
									return (! (x as Boolean))
							}
							case '-':{
								var x= expr.right_u.s_interprete(env, variable, aclass)
								return (- (x as Double))
							}
							default: return null
						}
					}
					else{
						return (expr.right_u.s_interprete(env, variable, aclass))
					}
				}
				
				Reference_expr:{
					if (expr.right!==null){
						var x = (expr.right.s_interprete(env, variable, "Integer")) as Integer
						var y = (expr.left.s_interprete(env, variable, "List")) as List<?>
						if(x!==null && y!==null){
							var z= x.intValue();
							return y.get(z)
						}
						else{
							return null
						}
					}
					else{
						return (expr.left.s_interprete(env, variable, aclass))
					}
				}
				
				Record_expr:{
					if(expr.right!==null){
						var x = expr.left.s_interprete(env, variable, "String") as String
						if (x!==null && env.containsKey(x)){
							var y = env.get(x)
							if(y.containsKey(expr.right)){
								try{
									var z=y.get(expr.right)
									if(z instanceof Long && aclass.equals("Double")){
										return new Double(z as Long)
									}
									else{
										return  z
									}
								} catch(ClassCastException e) {}
							}
							else{
								return null
							}
						}
						else{
							println("error in condition where: no viable event name : "+  x);
							throw new InterruptedException()
						}
					}
					else{
						return (expr.left.s_interprete(env, variable, aclass))
					}
				}
				
				Terminal:{
					if (expr.sexpr!==null){
						return (expr.sexpr.s_interprete(env, variable, aclass))
					}
				}
				}
				} catch(ClassCastException e) {}
	}
	
/****************************** */	
/** Method used to evaluate a scope in a set of defined scopes "limitlist" 
 * many classes and methods in partrap.utils are used mainly Class IScope
 * expressions in events are evaluated according to their types partrap or python */
	def private List<Composite_Result> evaluates(Scope scope,List<Hashtable> tracelist, List<Composite_Result> limitlist){
		var List<Scopereturns> newscope= new ArrayList<Scopereturns>()
		debug_log.append("Evaluating a scope: "+NodeModelUtils.getNode(scope).getText().trim())
		var List<Composite_Result> newresult= new ArrayList()
		var IDuration duration = null
		if(scope.duration!==null){
			duration = new IDuration(scope.duration.value, scope.duration.unit)
		}
		var Occurrence occ=null
		if(scope.occ!==null){
			occ=Occurrence.convert(scope.occ.toString)
		}
		if(scope.event2===null){
			if(scope.event.sevent!==null){
				var ievent1 = new IEvent(scope.event.sevent.type.toString, scope.event.sevent.name, null)
				/************* */
				//not null expression
					/**********   LAST *************/
					if(scope.occ.toString.equals("last")){
						for (Composite_Result cresult: limitlist){
							var Scopereturns scope0= cresult.scope;
							if(scope0!==null){
								var IScope iscope= new IScope(scope.name, Occurrence.convert("each"), ievent1, null, duration, null , true);
								var List<Scopereturns> nlimitlists = iscope.evaluate(tracelist,scope0);
								var List<Composite_Result> Climitlists= iscope.create_composite(nlimitlists);
			  		  			if (scope.event.sevent.expr!==null){
									var expression = scope.event.sevent.expr
									Climitlists = expression.interprete(Climitlists)
								}
								if (scope.event.sevent.py!==null){
									var expression = scope.event.sevent.py.toString
									var eee= new Evaluate_pyexpr()
									Climitlists = eee.cevaluate(Climitlists, expression,jep, variables)
								}
								if (!Climitlists.isEmpty()){
									var Composite_Result xx = Climitlists.get(Climitlists.size()-1)
									cresult.add(xx)
									xx.parent= cresult
									newresult.add(xx)
								}
								else{
									var Null_Result nr= new Null_Result(cresult,iscope);
									cresult.add(nr);
								}
							}
						}
					}
					/**********   FIRST *************/
					if(scope.occ.toString.equals("first")){
						for (Composite_Result cresult: limitlist){
							var Scopereturns scope0= cresult.scope;
							if(scope0!==null){
								var IScope iscope= new IScope(scope.name, Occurrence.convert("each"), ievent1, null, duration, null , true);
								var List<Scopereturns> nlimitlists = iscope.evaluate(tracelist,scope0);
								var List<Composite_Result> Climitlists= iscope.create_composite(nlimitlists);
			  		  			if (scope.event.sevent.expr!==null){
									var expression = scope.event.sevent.expr
									Climitlists = expression.interprete(Climitlists)
								}
								if (scope.event.sevent.py!==null){
									var expression = scope.event.sevent.py.toString
									var eee= new Evaluate_pyexpr()
									Climitlists = eee.cevaluate(Climitlists, expression,jep, variables)
								}
								if (!Climitlists.isEmpty()){
									var Composite_Result xx = Climitlists.get(0)
									cresult.add(xx)
									xx.parent= cresult
									newresult.add(xx)
								}
								else{
									var Null_Result nr= new Null_Result(cresult,iscope);
									cresult.add(nr);
								}
							}
						}
					}
					/**********   EACH *************/
					if(scope.occ.toString.equals("each")){
						var IScope iscope= new IScope(scope.name, Occurrence.convert("each"), ievent1, null, duration, null , true);
						for (Composite_Result cresult: limitlist){
							var Scopereturns scope0= cresult.scope;
							if(scope0!==null){
								var List<Scopereturns> nlimitlists = iscope.evaluate(tracelist,scope0);
								var List<Composite_Result> Climitlists= iscope.create_composite(nlimitlists);
								if (scope.event.sevent.expr!==null){
									var expression = scope.event.sevent.expr
									Climitlists = expression.interprete(Climitlists)
								}
								if (scope.event.sevent.py!==null){
									var expression = scope.event.sevent.py.toString
									var eee= new Evaluate_pyexpr()
									Climitlists = eee.cevaluate(Climitlists, expression,jep, variables)
								}
								if (!Climitlists.isEmpty()){
									cresult.addAll(Climitlists)
									for (xx: Climitlists){
										xx.parent= cresult
									}
									newresult.addAll(Climitlists)
									
								}
								else{
									var Null_Result nr= new Null_Result(cresult,iscope);
									cresult.add(nr);
								}
							}
						}
					}
					
			}
		}
		else{
			if (scope.event.sevent!==null && scope.event2.sevent!==null){
				var ievent1 = new IEvent(scope.event.sevent.type.toString, scope.event.sevent.name, null)
				var ievent2 = new IEvent(scope.event2.sevent.type.toString, scope.event2.sevent.name, null)
				/***************   BETWEEN   ***********/
				if (scope.name.equals("between")){
					var IScope iscope0= new IScope(scope.name, null, ievent1, ievent2, duration, null , true)
					if(scope.event.sevent.expr===null && scope.event2.sevent.expr===null && scope.event.sevent.py===null && scope.event2.sevent.py===null){
						newresult= iscope0.cevaluates(tracelist,limitlist)
					}
					else{
						for (Composite_Result cresult: limitlist){
							var Scopereturns scope0= cresult.scope;
							if(scope0!==null){
								var IScope iscope= new IScope("after", Occurrence.convert("each"), ievent1, null, duration, null , true);
								var List<Scopereturns> nlimitlists = iscope.evaluate(tracelist,scope0);
								if (scope.event.sevent.expr!==null){
									var expression = scope.event.sevent.expr
									nlimitlists = expression.rinterprete(nlimitlists)
								}
								if (scope.event.sevent.py!==null){
									var expression = scope.event.sevent.py.toString
									var eee= new Evaluate_pyexpr()
									nlimitlists = eee.evaluate(nlimitlists, expression, jep, variables)
								}
								var List<Composite_Result> Climitlists= iscope.create_composite(nlimitlists);
			  		  			
								for (Composite_Result cr1: Climitlists){ 
										var scope1=cr1.scope
										if(scope1!==null){
											var IScope iscope1= new IScope("before", Occurrence.convert("each"), ievent2, null, duration, null , false);
											var List<Scopereturns> nlimitlists1 = iscope1.evaluate(tracelist,scope1);
											if (scope.event2.sevent.expr!==null){
												var expression = scope.event2.sevent.expr
												nlimitlists1 = expression.rinterprete(nlimitlists1)
											}
											if (scope.event2.sevent.py!==null){
												var expression = scope.event2.sevent.py.toString
												var eee= new Evaluate_pyexpr()
												nlimitlists1 = eee.evaluate(nlimitlists1, expression, jep, variables)
											}
											var List<Composite_Result> Climitlists1= iscope0.create_composite(nlimitlists1);
			  		  						if (!Climitlists1.isEmpty()){
												var xx = Climitlists1.get(0)
												cresult.add(xx)
												xx.parent=cresult
												newresult.add(xx)
											}
										}
									}
									if(cresult.children.isEmpty){
										var Null_Result nr= new Null_Result(cresult,iscope0);
										cresult.add(nr);
									}
							}
						}
					}
				}
				/***************   SINCE   ***********/
				if (scope.name.equals("since")){
					/* Same code as between */
					var IScope iscope0= new IScope(scope.name, null, ievent1, ievent2, duration, null , true)
					if(scope.event.sevent.expr===null && scope.event2.sevent.expr===null && scope.event.sevent.py===null && scope.event2.sevent.py===null){
						newresult= iscope0.cevaluates(tracelist,limitlist)
					}
					else{
						for (Composite_Result cresult: limitlist){
							var Scopereturns scope0= cresult.scope;
							if(scope0!==null){
								var IScope iscope= new IScope("after", Occurrence.convert("each"), ievent1, null, duration, null , true);
								var List<Scopereturns> nlimitlists = iscope.evaluate(tracelist,scope0);
								if (scope.event.sevent.expr!==null){
									var expression = scope.event.sevent.expr
									nlimitlists = expression.rinterprete(nlimitlists)
								}
								if (scope.event.sevent.py!==null){
									var expression = scope.event.sevent.py.toString
									var eee= new Evaluate_pyexpr()
									nlimitlists = eee.evaluate(nlimitlists, expression, jep, variables)
								}
								var List<Composite_Result> Climitlists= iscope.create_composite(nlimitlists);
			  		  			if(scope.event2.sevent.expr!==null || scope.event2.sevent.py!==null){
									for (Composite_Result cr1: Climitlists){ 
										var scope1=cr1.scope
										if(scope1!==null){
											var IScope iscope1= new IScope("before", Occurrence.convert("each"), ievent2, null, duration, null , false);
											var List<Scopereturns> nlimitlists1 = iscope1.evaluate(tracelist,scope1);
											if (scope.event2.sevent.expr!==null){
												var expression = scope.event2.sevent.expr
												nlimitlists1 = expression.rinterprete(nlimitlists1)
											}
											else{
												var expression = scope.event2.sevent.py.toString
												var eee= new Evaluate_pyexpr()
												nlimitlists1 = eee.evaluate(nlimitlists1, expression, jep, variables)
											}
											var List<Composite_Result> Climitlists1= iscope0.create_composite(nlimitlists1);
			  		  						
											if (!Climitlists1.isEmpty()){
												var xx = Climitlists1.get(0)
												cresult.add(xx)
												xx.parent=cresult
												newresult.add(xx)
											}
										}
									}
								}
								else{
									var IScope iscope1= new IScope("before", Occurrence.convert("first"), ievent2, null, duration, null , false)
									var List<Scopereturns> nlimitlists1 = iscope1.evaluates(tracelist,nlimitlists);
									var List<Composite_Result> Climitlists1= iscope0.create_composite(nlimitlists1);
									for (xx: Climitlists1){
										xx.parent= cresult
									}
									cresult.addAll(Climitlists1)
									newresult.addAll(Climitlists1)
								}
								/*************** other side of since */
								var List<Composite_Result> Climitlists2 = new ArrayList<Composite_Result>()
								var from= 0;
								if(!newresult.isEmpty && newresult!==null){
									var lastscope= newresult.get(newresult.size-1).scope
									from= lastscope.hevent2.get("trace_occ_index") as Integer
								}
								scope0= new Scopereturns(from, scope0.to, scope0.environment, scope0.hevent1, scope0.hevent2);
									var IScope iscope2= new IScope("after", Occurrence.convert("each"), ievent1, null, duration, null , false)
									var List<Scopereturns> nlimitlists2 = iscope2.evaluate(tracelist,scope0)
									if (scope.event.sevent.expr!==null){
										var expression = scope.event.sevent.expr
										nlimitlists2 = expression.rinterprete(nlimitlists2)
									}
									if (scope.event.sevent.py!==null){
										var expression = scope.event.sevent.py.toString
										var eee= new Evaluate_pyexpr()
										nlimitlists2 = eee.evaluate(nlimitlists2, expression, jep, variables)
									}
									Climitlists2 = iscope0.create_composite(nlimitlists2);
									if (!Climitlists2.isEmpty()){
										for (xx: Climitlists2){
											xx.parent= cresult
										}
										newresult.addAll(Climitlists2)
									}
							}
							if(cresult.children.isEmpty){
								var Null_Result nr= new Null_Result(cresult,iscope0);
								cresult.add(nr);
							}
						}
					}
				}
			}
		}
		var Scope_display dis= new Scope_display();
		if( scope.name.equals("since") || scope.name.equals("between")){
			long_log=dis.display(scope.name,null,  newscope);}
		else{
			long_log=dis.display(scope.name, scope.occ.toString, newscope);
		}
		logger.fine(long_log.toString)
		long_log= new StringBuilder
		return newresult
	}
	

	
/****************************** */	
/** Method used to evaluate a pattern on a set of scopes and return a boolean value: verdict result 
 * IPattern class from partrap.util is used and expressions are evaluated according to their types : partrap or python*/
 @SuppressWarnings("rawtypes")
	def private  boolean evaluates(Pattern pattern,List<Hashtable> tracelist, List<Composite_Result> limitlist){
		var boolean result= true
		debug_log.append("Evaluating a pattern: "+NodeModelUtils.getNode(pattern).getText().trim())
		var IDuration duration = null
		if(pattern.duration!==null){
			duration = new IDuration(pattern.duration.value, pattern.duration.unit)
		}
		var int n= 1
		if(pattern.n!==null){
			n= pattern.n.n 
		}
		logger.finer("Evaluating pattern: "+ pattern.type+"\n")
		/********* Just one event (occurrence and absence) */
		if(pattern.event2===null){
			if(pattern.event.sevent!==null){
				var String exprText=""
				if (pattern.event.sevent.expr!==null){
					exprText = NodeModelUtils.getNode(pattern.event.sevent.expr).getText().trim()
				}
				if (pattern.event.sevent.py!==null){
					exprText = pattern.event.sevent.py.replace("$","")
				}
				var ievent1 = new IEvent(pattern.event.sevent.type.toString, pattern.event.sevent.name, exprText)
				var IPattern ipattern= new IPattern( pattern.type, ievent1, null, duration,n);
				if(!limitlist.isEmpty){
					for (Composite_Result cr: limitlist){
						var scopereturn= cr.scope
						var Map<Hashtable,Scopereturns> final_occ =ipattern.final_occurrences(tracelist, scopereturn.from, scopereturn.to ,scopereturn.environment);
						long_log.append("Scope from "+scopereturn.from+" to "+scopereturn.to+", environment: "+ scopereturn.environment+"\n");	
						var Convert_occurrences c_o= new Convert_occurrences();
						var List<Scopereturns> plimitlists= c_o.convert(final_occ);
						//var List<Scopereturns> plimitlists= ipattern.evaluate(tracelist,scopereturn.from, scopereturn.to ,scopereturn.environment)
						if (pattern.event.sevent.expr!==null ){
							var expression = pattern.event.sevent.expr
							plimitlists = expression.rinterprete(plimitlists)
							final_occ= c_o.reconvert(final_occ, plimitlists)
						}
						if (pattern.event.sevent.py!==null ){
							var expression = pattern.event.sevent.py.toString
							var eee= new Evaluate_pyexpr()
							plimitlists = eee.evaluate(plimitlists, expression, jep, variables)
							final_occ= c_o.reconvert(final_occ, plimitlists)
						}
						var boolean iverdict = ipattern.cond_verdict(plimitlists);
						var List<Hashtable> occ_events= c_o.convert_to_list(final_occ);
						var Low_Result low = new Low_Result(ipattern,occ_events, null)
						cr.add(low)
						low.parent=cr
						low.verdict=iverdict
						result=iverdict && result
						if(!iverdict){
							low.update_false_verdict;
							var Short_Result_Explanation sre= new Short_Result_Explanation();
							var StringBuilder sr= sre.explain(scopereturn,occ_events, ipattern,tracelist);
							short_log.append(sr.toString());
							}
						else{
							var Long_Result_Explanation lre= new Long_Result_Explanation();
							var StringBuilder lr= lre.explain(scopereturn,occ_events, ipattern,tracelist);
							long_log.append(lr.toString());
						}
						result=ipattern.cond_verdict(plimitlists) && result
					}
				}
				else{
					result= true && result;
					var Long_Result_Explanation lre= new Long_Result_Explanation();
					var StringBuilder lr= lre.explain(null,null, ipattern,tracelist);
					long_log.append(lr.toString());
					long_log.append("No scope is found to evaluate the pattern");
				}
			}
		}
		/********* binary patterns: prevents, followed_by and preceded_by */
		else{
			if(pattern.event.sevent!==null && pattern.event2.sevent!==null){
				var String exprText=""
				if (pattern.event.sevent.expr!==null){
					exprText = NodeModelUtils.getNode(pattern.event.sevent.expr).getText().trim()
				}
				if (pattern.event.sevent.py!==null){
					exprText = pattern.event.sevent.py.replace("$","")
				}
				var String exprText2=""
				if (pattern.event2.sevent.expr!==null){
					exprText2 = NodeModelUtils.getNode(pattern.event2.sevent.expr).getText().trim()
				}
				if (pattern.event2.sevent.py!==null){
					exprText2 = pattern.event2.sevent.py.replace("$","")
				}
				var ievent1 = new IEvent(pattern.event.sevent.type.toString, pattern.event.sevent.name, exprText)
				var ievent2 = new IEvent(pattern.event2.sevent.type.toString, pattern.event2.sevent.name, exprText2)
				var IPattern ipattern= new IPattern( pattern.type, ievent1, ievent2, duration,n);
				if (!limitlist.isEmpty) {
					for (Composite_Result cr: limitlist){
						var scopereturn= cr.scope
						var List<Scopereturns> plimitlists= ipattern.evaluate(tracelist,scopereturn)
						/** PRECEDED_BY */
						if (pattern.type.equals("preceded_by")){
							if (pattern.event.sevent.expr!==null ){
								var expression = pattern.event.sevent.expr
								plimitlists = expression.rinterprete(plimitlists)
							}
							if (pattern.event.sevent.py!==null ){
								var expression = pattern.event.sevent.py.toString
								var eee= new Evaluate_pyexpr()
								plimitlists = eee.evaluate(plimitlists, expression, jep, variables)
							}
							/*****/
							if(!plimitlists.isEmpty){
								for (Scopereturns scope: plimitlists){
									var Map<Hashtable,Scopereturns> final_occ =ipattern.final_occurrences(tracelist, scope.from, scope.to ,scope.environment);
									logger.fine("Scope from "+scope.from+" to "+scope.to+", environment: "+ scope.environment+"\n");	
									var Convert_occurrences c_o= new Convert_occurrences();
									var List<Scopereturns> occ_list= c_o.convert(final_occ);
									if (pattern.event2.sevent.expr!==null ){
										var expression = pattern.event2.sevent.expr
										occ_list = expression.rinterprete(occ_list)
										final_occ= c_o.reconvert(final_occ, occ_list)
									}
									if (pattern.event2.sevent.py!==null ){
										var expression = pattern.event2.sevent.py.toString
										var eee= new Evaluate_pyexpr()
										occ_list = eee.evaluate(occ_list, expression, jep, variables)
										final_occ= c_o.reconvert(final_occ, occ_list)
									}
									var boolean iverdict = ipattern.cond_verdict(occ_list);
									var List<Hashtable> occ_events= c_o.convert_to_list(final_occ);
									var Low_Result low = new Low_Result(ipattern,occ_events, scope)
									cr.add(low)
									low.parent=cr
									low.verdict=iverdict
									result=iverdict && result
									if(!iverdict){
										low.update_false_verdict;
										var Short_Result_Explanation sre= new Short_Result_Explanation();
										var StringBuilder sr= sre.explain(scope,occ_events, ipattern,tracelist);
										short_log.append(sr.toString());
									}
									else{
										var Long_Result_Explanation lre= new Long_Result_Explanation();
										var StringBuilder lr= lre.explain(scope,occ_events, ipattern,tracelist);
										long_log.append(lr.toString());
									}
								}
							}
							else{
								var Low_Result low = new Low_Result(ipattern,null, null)
									cr.add(low)
									low.parent=cr
									low.verdict=true
									long_log.append("No scope is found to evaluate the pattern");
							}
						}
						/**   Followed and prevents */
						else{
							if (pattern.event.sevent.expr!==null ){
								var expression = pattern.event.sevent.expr
								plimitlists = expression.rinterprete(plimitlists)
							}
							if (pattern.event.sevent.py!==null ){
								var expression = pattern.event.sevent.py.toString
								var eee= new Evaluate_pyexpr()
								plimitlists = eee.evaluate(plimitlists, expression, jep, variables)
							}
							/*****/
							if(!plimitlists.isEmpty){
								for (Scopereturns scope: plimitlists){
									var Map<Hashtable,Scopereturns> final_occ =ipattern.final_occurrences(tracelist, scope.from, scope.to ,scope.environment);
									//logger.fine("Scope from "+scope.from+" to "+scope.to+", environment: "+ scope.environment+"\n");	
									var Convert_occurrences c_o= new Convert_occurrences();
									var List<Scopereturns> occ_list= c_o.convert(final_occ);
									if (pattern.event2.sevent.expr!==null ){
										var expression = pattern.event2.sevent.expr
										occ_list= expression.rinterprete(occ_list)
										final_occ= c_o.reconvert(final_occ, occ_list)
									}
									if (pattern.event2.sevent.py!==null ){
										var expression = pattern.event2.sevent.py.toString
										var eee= new Evaluate_pyexpr()
										occ_list = eee.evaluate(occ_list, expression, jep, variables)
										final_occ= c_o.reconvert(final_occ, occ_list);
									}
									var boolean iverdict = ipattern.cond_verdict(occ_list);
									var List<Hashtable> occ_events= c_o.convert_to_list(final_occ);
									var Low_Result low = new Low_Result(ipattern,occ_events, scope)
									cr.add(low)
									low.parent=cr
									low.verdict=iverdict
									result=iverdict && result
									if(!iverdict){
										low.update_false_verdict;
										var Short_Result_Explanation sre= new Short_Result_Explanation();
										var StringBuilder sr= sre.explain(scope,occ_events, ipattern,tracelist);
										short_log.append(sr.toString());
									}
									else{
										var Long_Result_Explanation lre= new Long_Result_Explanation();
										var StringBuilder lr= lre.explain(scope,occ_events, ipattern,tracelist);
										long_log.append(lr.toString());
										println("long4: "+long_log.toString)
									}
								}
							/** */
							}
							else{
								var Low_Result low = new Low_Result(ipattern,null, null)
									cr.add(low)
									low.parent=cr
									low.verdict=true
									long_log.append("No scope is found to evaluate the pattern");
							}
						}
					}
				}
				else{
					result= true && result;
					var Long_Result_Explanation lre= new Long_Result_Explanation();
					var StringBuilder lr= lre.explain(null,null, ipattern,tracelist);
					long_log.append(lr.toString());
				}
			}
		}
		return result
	}
/********************** */
/** constructs the tree of files using the directory path and produces a new tree with default false results */
def public TreeNode<String> listf(String directoryName, TreeNode<String> files) {
    var File directory = new File(directoryName);

    // get all the files from a directory
    var File[] fList = directory.listFiles();
    for (File file : fList) {
        if (file.isFile()) {
        	if(file.name.toLowerCase.endsWith("json")){
        		var String filepath= file.getAbsolutePath()
				var String filepath2=filepath.replace("\\","/")
				var HashMap<String, Boolean> leaf= new HashMap <String, Boolean>();
				leaf.put(filepath2, false);
				//println("file json : "+filepath2)
				files.addleaf(leaf)
            }
        } else if (file.isDirectory()) {
        	var String filepath= file.name
			var TreeNode<String> newchild= new  TreeNode<String>(filepath);
        	files.addChild(newchild)
        	//println("directory : "+filepath)
            listf(file.getAbsolutePath(), newchild);
        }
    }
    println(files)
    return files
}

/*********************** */
/** Uses a tree of files with false default result
 * execute on each file and return a tree of updated results for each trace file */
	def public TreeNode<String> execute_on_tree(named_Property nproperty, TreeNode<String> trace_tree) {
		var new_tree= new TreeNode(trace_tree.getdata)
    	var leaves = trace_tree.getleaves
    	var children= trace_tree.children
    	for (leaf: leaves){
    		var List<Hashtable> traceEventslist = new ArrayList<Hashtable>()
			var FileReader rtrace = null
			var stracefile =leaf.keySet.toArray().get(0) as String
			logger.info(short_log.toString+"------applied on trace file \'"+stracefile+"\' :")
			short_log= new StringBuilder();
			var File tracefile = new File(stracefile)
			try {
				rtrace = new FileReader(stracefile)
				traceEventslist = Convertjsonlist.convert(rtrace)
			}
			catch (FileNotFoundException e) {
				e.printStackTrace()
			}
			var List<Composite_Result> limitlists0= new ArrayList<Composite_Result>()
			var Scopereturns initscope = new Scopereturns(0,traceEventslist.size-1, null, null, null)
			var initcomposite= new Prop_Result(initscope, null, true)
			initcomposite.trace_path= stracefile
			initcomposite.name= nproperty.name;
			limitlists0.add(initcomposite)
			var Boolean value= nproperty.property.interprete(traceEventslist,limitlists0)
			//System.out.println("------------------------------")
			System.out.println("after composite : ")
			var StringBuilder st = new StringBuilder
			var new_leaf = new HashMap<String, Boolean>
			new_leaf.put(tracefile.name, value)
			new_tree.addleaf(new_leaf)
			/*  */
			var file_name = nproperty.name+"_"+FilenameUtils.getBaseName(tracefile.getName())+".txt";
			var String results_path = mypath+"/results/"+model_name+"/"+nproperty.name+"/"+file_name;
			new File(mypath+"/results/"+model_name+"/"+nproperty.name).mkdirs();
			System.out.println("resultspath : "+results_path);
			var Create_result_file crf= new Create_result_file();
			var prop=  NodeModelUtils.getNode(nproperty.property).getText().trim()
			st.append(nproperty.name+": "+prop); 
			st.append(";\n");
			if(value){
				st=initcomposite.traverse(st,0,true)
				crf.create_link_file(st, results_path);
				html_result.append("<tr><td><a href=\""+nproperty.name+"/"+file_name+"\"style=\"visited.color:mediumseagreen; color: mediumseagreen;\">OK</a></td></tr>");
				logger.info("Final verdict for trace : " + tracefile.getName()+" is : "+value+"\n\n -----------------------");
				System.out.println("after composite : "+initcomposite.verdict)
				logger.info(st.toString)
				}
			else{
				st=initcomposite.traverse(st,0,false)
				crf.create_link_file(st, results_path);
				html_result.append("<tr><td><a href=\""+nproperty.name+"/"+file_name+"\" style=\"visited.color:#cc0000; color: #cc0000;\">KO</a></td></tr>" +"");
				logger.warning("False on trace "+tracefile.getName()+":\n"+st.toString()+"\n\n -----------------------");
				logger.fine(long_log.toString)
				}
				short_log=new StringBuilder();
				long_log=new StringBuilder();
		}
    	for (child: children ){
    		var new_child= execute_on_tree(nproperty, child)
    		new_tree.addChild(new_child)
    	}
		return new_tree
	}
	
	
}
