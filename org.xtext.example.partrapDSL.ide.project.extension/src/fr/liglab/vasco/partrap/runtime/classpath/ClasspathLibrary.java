/*-
 * #%L
 * This file is part of Partrap IDE
 * Please visit http://vasco.imag.fr/tools/partrap/ for further information
 * 
 * Authors : Yoann Blein , Ansem Ben Cheikh 
 *           Laboratoire d'Informatique de Grenoble, Team VASCO
 * %-
 * Copyright (C) 2017 - 2019 University of Grenoble Alpes
 * %-
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package fr.liglab.vasco.partrap.runtime.classpath;

import java.io.File;
import java.io.FilenameFilter;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jdt.core.IClasspathContainer;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.JavaCore;

public class ClasspathLibrary implements IClasspathContainer {


	/**
	 * The path of this class path container
	 */
	public static final IPath CONTAINER_ID 	= new Path("fr.liglab.vasco.partrap.runtime.classpath.container");

	/**
	 * The ID of the Partrap runtime plugin
	 */
	private static final String ID	= "org.xtext.example.partrapDSL";
	
	/**
	 * Valid entries
	 */
	private static FilenameFilter CLASSPATH_FILTER  = new FilenameFilter() {
		
		@Override
		public boolean accept(File dir, String name) {
			return name.toLowerCase().endsWith("jar");
		}
	};
	
	/**
	 * Return the list of class path entries to the Partrap runtime libraries
	 */
	@Override
	public IClasspathEntry[] getClasspathEntries() {
		
		
		URI libraryLocation	= ClasspathLibrary.getRuntimeEntry("/META-INF/",true);
		if (libraryLocation == null)
			return new IClasspathEntry[0];
		
		/*
		 * Iterate over all the embedded jars in the library directory 
		 */
		List<IClasspathEntry> classpathEntries = new ArrayList<IClasspathEntry>();
		File libraryDirectory 	= new File(libraryLocation);
		if (libraryDirectory.isDirectory() && libraryDirectory.canRead()) {
			for (File library : libraryDirectory.listFiles(CLASSPATH_FILTER)) {
				IClasspathEntry libraryEntry = JavaCore.newLibraryEntry(new Path(library.getAbsolutePath()),null,null);
				classpathEntries.add(libraryEntry);
			}
		}
		
		return classpathEntries.toArray(new IClasspathEntry[classpathEntries.size()]);
	}

	@Override
	public String getDescription() {
		return "Partrap runtime";
	}

	@Override
	public int getKind() {
		return IClasspathContainer.K_APPLICATION;
	}

	@Override
	public IPath getPath() {
		return CONTAINER_ID;
	}
	

	/**
	 * Gets the URI of an entry inside the deployed partrap bundle, can optionally request
	 * to get a file
	 */
	public static URI getRuntimeEntry(String path, boolean fileURL) {
		
		try {
			/*
			 * Get the URL for the entry in the deployed plugin
			 */
			URL entryURL = Platform.getBundle(ID).getEntry(path);
			if (fileURL)
				entryURL = FileLocator.toFileURL(entryURL);
			
			/*
			 * Build a properly escaped URI from the given URL
			 */
			return new URI(entryURL.getProtocol(),entryURL.getAuthority(),entryURL.getPath(),entryURL.getQuery(),entryURL.getRef());
			
		} catch (Exception ignored) {
			return null;
		} 
	}

}
