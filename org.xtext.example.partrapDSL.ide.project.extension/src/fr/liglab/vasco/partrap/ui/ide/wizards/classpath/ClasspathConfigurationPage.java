/*-
 * #%L
 * This file is part of Partrap IDE
 * Please visit http://vasco.imag.fr/tools/partrap/ for further information
 * 
 * Authors : Yoann Blein , Ansem Ben Cheikh 
 *           Laboratoire d'Informatique de Grenoble, Team VASCO
 * %-
 * Copyright (C) 2017 - 2019 University of Grenoble Alpes
 * %-
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package fr.liglab.vasco.partrap.ui.ide.wizards.classpath;

import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.ui.wizards.IClasspathContainerPage;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import fr.liglab.vasco.partrap.runtime.classpath.ClasspathLibrary;

public class ClasspathConfigurationPage extends WizardPage implements IClasspathContainerPage {

	public ClasspathConfigurationPage() {
		super("Partrap runtime","Partrap Runtime", (ImageDescriptor) null);
		setPageComplete(true);
	}

	/**
	 * Return the configured class path entry for the Partrap runtime container
	 */
	@Override
	public IClasspathEntry getSelection() {
		return JavaCore.newContainerEntry(ClasspathLibrary.CONTAINER_ID);
	}

	/**
	 * The Partrap classpath container is not editable, so we don't need to handle selection
	 */
	@Override
	public void setSelection(IClasspathEntry containerEntry) {
	}
	
	@Override
	public void createControl(Composite parent) {
		
        Composite composite = new Composite(parent, SWT.NULL);
        composite.setLayout(new GridLayout());
        composite.setLayoutData(new GridData(GridData.VERTICAL_ALIGN_FILL
                | GridData.HORIZONTAL_ALIGN_FILL));
        composite.setFont(parent.getFont());

        Label description = new Label(composite, SWT.NONE);
        description.setText("Add to classptah all the required libraries of the Partrap runtime");

        setControl(composite);    
	}

	/**
	 * This container doesn't have configuration information, so there is nothing to validate
	 */
	@Override
	public boolean finish() {
		return true;
	}
	
}
