/*-
 * #%L
 * This file is part of Partrap IDE
 * Please visit http://vasco.imag.fr/tools/partrap/ for further information
 * 
 * Authors : Yoann Blein , Ansem Ben Cheikh 
 *           Laboratoire d'Informatique de Grenoble, Team VASCO
 * %-
 * Copyright (C) 2017 - 2019 University of Grenoble Alpes
 * %-
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package fr.liglab.vasco.partrap.ui.ide.wizards.project;


import org.eclipse.jface.layout.GridLayoutFactory;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;

import org.eclipse.ui.IWorkbenchPropertyPage;

import fr.liglab.vasco.partrap.project.configuration.TemplateExpander;

import java.io.File;
import java.net.URI;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;

import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;

import org.eclipse.core.runtime.NullProgressMonitor;

public class PythonPropertyPage extends org.eclipse.ui.dialogs.PropertyPage implements IWorkbenchPropertyPage {

	/**
	 * The template expander used for configuring this project 
	 */
	protected final TemplateExpander expander;

	protected Button enablePythonButton;
	
	public PythonPropertyPage() {
		
		this.expander 	= new TemplateExpander();
		
		noDefaultAndApplyButton();
	}

	@Override
	public String getDescription() {
		return "Configure support for Python expression in ParTrap properties";
	}


	@Override
	protected Control createContents(Composite parent) {
		
		Composite composite = new Composite(parent, SWT.NONE);

        GridLayoutFactory.fillDefaults().generateLayout(composite);
		
        enablePythonButton = createCheckBox(composite, "Enable Python support", isEnabled());
		return composite;
	}

	protected Button createCheckBox(Composite composite, String text, boolean selected) {
		
		Button checkbox= new Button(composite, SWT.CHECK);
		checkbox.setFont(composite.getFont());
		checkbox.setText(text);
		checkbox.setSelection(selected);
		
		GridData gd= new GridData();
		gd.horizontalSpan= 2;
		gd.horizontalAlignment= GridData.FILL;
		checkbox.setLayoutData(gd);
		
		
		return checkbox;
	}
	

	@Override
	protected void performApply() {
		perform();
	}
	
	@Override
	public boolean performOk() {
		return perform();
	}

	protected boolean perform() {
		return enablePythonButton.getSelection() ? enable() : disable();
	}

	protected IProject getProject() {
		return getElement().getAdapter(IProject.class);
	}
	

	
	protected boolean enable() {
		try {
			
			/*
			 * Calculate the name of the system from the name of the project
			 * 
			 */
			expander.resetVariables();
			expander.setVariable("name",getProject().getName());
			
			/*
			 * Expand the basic layout template of the python dircetory
			 */
			URI templateLocation = ProjectWizard.getRuntimeEntry("/templates/python",true);
			File pythonTemplate = new File(templateLocation);

			expander.expand(getProject(), pythonTemplate, new NullProgressMonitor());
			
			return true;
		} catch (CoreException | InterruptedException e) {
			return false;
		}
	}
	
	protected boolean disable() {
		
		URI templateLocation = ProjectWizard.getRuntimeEntry("/templates/python",true);
		File pythonTemplate = new File(templateLocation);

		/*
		 * Remove the paths added by the template
		 */
		boolean allDeleted = true;
		
		for (File templatePath : pythonTemplate.listFiles()) {
			try {
				IPath projectPath 	= new Path(templatePath.getName());
				IResource resource	= templatePath.isDirectory() ? getProject().getFolder(projectPath) : getProject().getFile(projectPath) ;
				
				resource.delete(true, new NullProgressMonitor());
				
			} catch (CoreException e) {
				allDeleted = false;
			}
			
		}
		
		return allDeleted;
	}
	
	protected boolean isEnabled() {
		
		URI templateLocation = ProjectWizard.getRuntimeEntry("/templates/python",true);
		File pythonTemplate = new File(templateLocation);

		/*
		 * Verify the paths added by the template exists
		 */
		boolean allExists = true;
		
		for (File templatePath : pythonTemplate.listFiles()) {
			IPath projectPath 	= new Path(templatePath.getName());
			IResource resource	= templatePath.isDirectory() ? getProject().getFolder(projectPath) : getProject().getFile(projectPath) ;
				
			allExists = allExists && resource.exists();	
		}
		
		return allExists;
	}
}
