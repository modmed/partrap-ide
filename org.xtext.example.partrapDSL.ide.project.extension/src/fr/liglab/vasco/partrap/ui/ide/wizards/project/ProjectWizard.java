/*-
 * #%L
 * This file is part of Partrap IDE
 * Please visit http://vasco.imag.fr/tools/partrap/ for further information
 * 
 * Authors : Yoann Blein , Ansem Ben Cheikh 
 *           Laboratoire d'Informatique de Grenoble, Team VASCO
 * %-
 * Copyright (C) 2017 - 2019 University of Grenoble Alpes
 * %-
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package fr.liglab.vasco.partrap.ui.ide.wizards.project;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.core.resources.ICommand;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRunnable;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExecutableExtension;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.jobs.ISchedulingRule;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.ui.wizards.NewJavaProjectWizardPageOne;
import org.eclipse.jdt.ui.wizards.NewJavaProjectWizardPageTwo;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.operation.IThreadListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.IWorkingSet;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.ide.IDE;
import org.eclipse.ui.wizards.newresource.BasicNewProjectResourceWizard;
import org.eclipse.ui.wizards.newresource.BasicNewResourceWizard;
import org.eclipse.xtext.ui.XtextProjectHelper;

import fr.liglab.vasco.partrap.project.configuration.TemplateExpander;
import fr.liglab.vasco.partrap.runtime.classpath.ClasspathLibrary;

public class ProjectWizard extends Wizard implements INewWizard, IExecutableExtension, IWorkspaceRunnable {

	/**
	 * The wizard pages
	 */
	private NewJavaProjectWizardPageOne	javaProjectPage;
	private NewJavaProjectWizardPageTwo javaSettingsPage;
	
	/**
	 * The current configuration when the wizard was launched
	 */
	private IWorkbench 					workbench;
	private IConfigurationElement 		configuration;
	private IStructuredSelection 		selection;

	/**
	 * The template expander used for configuring this project 
	 */
	protected final TemplateExpander expander;

	
	/**
	 * The list of resources to display after project creation
	 * 
	 */
	List<IResource> resourcesToShow;
	
	public ProjectWizard() {
		javaProjectPage		= null;
		javaSettingsPage	= null;
		
		this.expander 	= new TemplateExpander();
	}

	@Override
	public void init(IWorkbench workbench, IStructuredSelection selection) {
		this.workbench	= workbench;
		this.selection	= selection;
	}

	@Override
	public void setInitializationData(IConfigurationElement cfig, String propertyName, Object data) {
		configuration= cfig;
	}
	
	public IStructuredSelection getSelection() {
		return selection;
	}

	public IWorkbench getWorkbench() {
		return workbench;
	}
	
	public IConfigurationElement getConfiguration() {
		return configuration;
	}

	private IWorkbenchPart getActivePart() {
		IWorkbenchWindow activeWindow= getWorkbench().getActiveWorkbenchWindow();
		if (activeWindow != null) {
			IWorkbenchPage activePage= activeWindow.getActivePage();
			if (activePage != null) {
				return activePage.getActivePart();
			}
		}
		return null;
	}
	
	public void addPages() {
		
		javaProjectPage = new NewJavaProjectWizardPageOne() {
			
			/**
			 * Returns the default class path entries to be added on new projects. 
			 */
			@Override
			public IClasspathEntry[] getDefaultClasspathEntries() {
				List<IClasspathEntry> defaultPartrapClasspath = new ArrayList<IClasspathEntry>();
				defaultPartrapClasspath.addAll(Arrays.asList(super.getDefaultClasspathEntries()));
				defaultPartrapClasspath.add(JavaCore.newContainerEntry(ClasspathLibrary.CONTAINER_ID));
				return defaultPartrapClasspath.toArray(new IClasspathEntry[defaultPartrapClasspath.size()]);
			}
			
		};
		
		addPage(javaProjectPage);

		javaSettingsPage = new NewJavaProjectWizardPageTwo(javaProjectPage);
		addPage(javaSettingsPage);

		javaProjectPage.init(getSelection(),getActivePart());
	}


	@Override
	public boolean performFinish() {

		/*
		 * Create the project in a background job
		 */
		try {
			
			boolean isInJob			= (Job.getJobManager().currentJob() != null);
			boolean hasRule			= isInJob && (Job.getJobManager().currentJob().getRule() != null);
			ISchedulingRule rule 	= hasRule ? Job.getJobManager().currentJob().getRule() : ResourcesPlugin.getWorkspace().getRoot();

			getContainer().run(true,true,new CreateProjectJob(rule,hasRule));
			
		} catch (InvocationTargetException e) {
			Throwable realException = e.getTargetException();
			MessageDialog.openError(getShell(), "Error",
					realException.getMessage());
			return false;
		} catch  (InterruptedException e) {
			return false;
		}
		
		/*
		 * Show the project in the foreground job
		 */
		try {
			showProject(resourcesToShow);
		} catch (Exception e) {
			MessageDialog.openError(getShell(),"Error", e.getMessage());
			return false;
		}
		return true;
	}
	
	/**
	 * An adapter class to perform the finish action in a background job
	 * 
	 * @author vega
	 *
	 */
	public class CreateProjectJob implements IRunnableWithProgress, IThreadListener  {
		
		private ISchedulingRule rule;
		private boolean transfer;
		
		public CreateProjectJob(ISchedulingRule rule, boolean transfer) {
			this.rule 				= rule;
			this.transfer			= transfer;
		}
		
		public void run(IProgressMonitor monitor) throws InvocationTargetException, InterruptedException {
			try {
				JavaCore.run(ProjectWizard.this,rule,monitor);
			} catch (OperationCanceledException e) {
				throw new InterruptedException(e.getMessage());
			} catch (CoreException e) {
				throw new InvocationTargetException(e);
			}
		}
		@Override
		public void threadChange(Thread thread) {
			if (transfer)
				Job.getJobManager().transferRule(rule, thread);
		}
		
		
	}
	
	/**
	 * The workspace action to execute at finish
	 */
	@Override
	public void run(IProgressMonitor monitor) throws CoreException {
		try {
			createProject(monitor);
			configureProject(monitor);
		} catch (InterruptedException e) {
			throw new OperationCanceledException(e.getMessage());
		}
	}
	
	@Override
	public boolean performCancel() {
		javaSettingsPage.performCancel();
		return super.performCancel();
	}

	/**
	 * Create a new project
	 */
	private void createProject(IProgressMonitor monitor) throws InterruptedException, CoreException {
		
		/*
		 * Just reuse the standard JDT wizard action
		 */
		javaSettingsPage.performFinish(monitor);
	}
	
	/**
	 * Configures the new project
	 */
	private void configureProject(IProgressMonitor monitor) throws InterruptedException, CoreException {
		resourcesToShow = configure(javaSettingsPage.getJavaProject(),monitor);
	}
	
	/**
	 * Configures the created project from the specified parameters.
	 * 
	 * It returns the list of created resources that must be displayed to the user.
	 */
	public List<IResource> configure(IJavaProject project, IProgressMonitor monitor) throws CoreException, InterruptedException {
		
		/*
		 * Add xtext nature and builder 
		 */
		configureXtext( project.getProject());

		/*
		 * Calculate the name of the system from the name of the project
		 * 
		 */
		expander.resetVariables();
		expander.setVariable("name",project.getElementName());
		
		/*
		 * Expand the basic layout template of the project
		 */
		URI templateLocation = getRuntimeEntry("/templates/project",true);
		File projectTemplate = new File(templateLocation);
		expander.expand(project.getProject(), projectTemplate, monitor);

		IPath displayedLocation		= new Path(expander.expand("${name}.partrap",monitor));
		IResource displayedResource = getSources(project).findMember(displayedLocation);
		
		return Collections.singletonList(displayedResource);
	}

	private static void configureXtext(IProject project) throws CoreException {
		
        IProjectDescription description = project.getDescription();
        
        if (! XtextProjectHelper.hasNature(project)) {

            String[] natures		= description.getNatureIds();
            String[] newNatures		= new String[natures.length + 1];
            System.arraycopy(natures, 0, newNatures, 0, natures.length);

            newNatures[natures.length] = XtextProjectHelper.NATURE_ID;

            IStatus status = project.getWorkspace().validateNatureSet(newNatures);

            if (status.getCode() == IStatus.OK) {
                description.setNatureIds(newNatures);
            }

        }

        if (! XtextProjectHelper.hasBuilder(project)) {

			ICommand xtextBuilder = description.newCommand();
			xtextBuilder.setBuilderName(XtextProjectHelper.BUILDER_ID);

        	ICommand[] builders		= description.getBuildSpec();
        	ICommand[] newBuilders	= new ICommand[builders.length + 1];
            System.arraycopy(builders, 0, newBuilders, 0, builders.length);

			newBuilders[builders.length] = xtextBuilder;
			description.setBuildSpec(newBuilders);

        }
        
        project.setDescription(description, null);

	}


	/**
	 * Get the source folder configured in the project
	 */
	private static IFolder getSources(IJavaProject project) throws JavaModelException {
		/*
		 * Find the source folder of the project
		 */

		IPackageFragmentRoot sourceRoot = null;
		
		IPackageFragmentRoot[] roots = project.getJavaProject().getPackageFragmentRoots();
		for (IPackageFragmentRoot root : roots) {
			if (root.getKind() == IPackageFragmentRoot.K_SOURCE) {
				sourceRoot = root;
				break;
			}
		}
		
		return sourceRoot != null ? (IFolder)sourceRoot.getUnderlyingResource() : null;

	}
	
	/**
	 * Show the project in the workspace, updating the specified working sets
	 */
	private void showProject(List<IResource> resourcesToShow) throws CoreException, InterruptedException {

		
		/*
		 * Show the project
		 */
		IWorkingSet[] workingSets= javaProjectPage.getWorkingSets();
		if (workingSets.length > 0) {
			PlatformUI.getWorkbench().getWorkingSetManager().addToWorkingSets(javaSettingsPage.getJavaProject(), workingSets);
		}

		BasicNewProjectResourceWizard.updatePerspective(getConfiguration());
		BasicNewResourceWizard.selectAndReveal(javaSettingsPage.getJavaProject().getProject(), getWorkbench().getActiveWorkbenchWindow());
		
		/*
		 * Open the specified resources in the editor
		 */
		for (IResource resourceToShow : resourcesToShow) {
			if ( !(resourceToShow instanceof IFile))
				continue;
			
			IFile fileToShow = (IFile) resourceToShow;
			IDE.openEditor(getWorkbench().getActiveWorkbenchWindow().getActivePage(),fileToShow,true);
			
		}
		
	}
	
	/**
	 * The ID of this plugin
	 */
	private static final String PLUGIN_ID	= "org.xtext.example.partrapDSL.ide.project.extension";

	
	/**
	 * Gets the URI of an entry inside this deployed plugin, can optionally request
	 * to get a file
	 */
	public static URI getRuntimeEntry(String path, boolean fileURL) {
		
		try {
			/*
			 * Get the URL for the entry in the deployed plugin
			 */
			URL entryURL = Platform.getBundle(PLUGIN_ID).getEntry(path);
			if (fileURL)
				entryURL = FileLocator.toFileURL(entryURL);
			
			/*
			 * Build a properly escaped URI from the given URL
			 */
			return new URI(entryURL.getProtocol(),entryURL.getAuthority(),entryURL.getPath(),entryURL.getQuery(),entryURL.getRef());
			
		} catch (Exception ignored) {
			return null;
		} 
	}
	
}
