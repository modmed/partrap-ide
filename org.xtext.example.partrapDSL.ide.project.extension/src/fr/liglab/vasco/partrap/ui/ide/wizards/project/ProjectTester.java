package fr.liglab.vasco.partrap.ui.ide.wizards.project;

import org.eclipse.core.expressions.PropertyTester;
import org.eclipse.core.resources.IProject;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;

import fr.liglab.vasco.partrap.runtime.classpath.ClasspathLibrary;

public class ProjectTester extends PropertyTester {

	
	@Override
	public boolean test(Object object, String property, Object[] args, Object expectedValue) {

		try {

			if (!(object instanceof IProject)) {
				return false;
			}
			
			IProject project = (IProject) object;
			if (!project.hasNature(JavaCore.NATURE_ID)) {
				return false; 
			}
			
			IJavaProject javaProject = JavaCore.create(project);
			if (javaProject == null) {
				return false;
			}

			for (IClasspathEntry classPathEntry : javaProject.getRawClasspath()) {
				if (classPathEntry.getPath().equals(ClasspathLibrary.CONTAINER_ID)) {
					return true;
				}
			}
			
			return false;
		} catch (Exception e) {
			return false;
		}

	}

}
