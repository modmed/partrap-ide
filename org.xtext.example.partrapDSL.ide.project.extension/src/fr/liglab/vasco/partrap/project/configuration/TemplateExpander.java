/*-
 * #%L
 * This file is part of Partrap IDE
 * Please visit http://vasco.imag.fr/tools/partrap/ for further information
 * 
 * Authors : Yoann Blein , Ansem Ben Cheikh 
 *           Laboratoire d'Informatique de Grenoble, Team VASCO
 * %-
 * Copyright (C) 2017 - 2019 University of Grenoble Alpes
 * %-
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package fr.liglab.vasco.partrap.project.configuration;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;


/**
 * An class to recursively expand a directory of templates.
 * 
 * Templates are simple text files that can contain variable substitution of the form
 * ${var} in their contents or in their file name.
 * 
 * Template files are identified by the extension ".template"
 * 
 */
public class TemplateExpander  {

	/**
	 * The  value of the substitution variables
	 */
	private final Map<String,String> variableValues;
	
	
	public TemplateExpander() {
		this.variableValues = new HashMap<String, String>();
	}
	
	/**
	 * Set the value of a variable
	 */
	public void setVariable(String variable,String value) {
		variableValues.put(variable, value);
	}
	
	/**
	 * Get the value of the variables
	 */
	public String getVariable(String variable) {
		return variableValues.get(variable);
	}
	
	/**
	 * Reset all the values of the variables
	 */
	public void resetVariables() {
		variableValues.clear();
	}
	
	/**
	 * Expands the given string
	 */
	public String expand(String input) {
		try {
			return expand(input, new NullProgressMonitor());
		} catch (InterruptedException e) {
			return input;
		}
	}
	
	/**
	 * Substitute variables in the given  string
	 */
	public String expand(String inputString, IProgressMonitor monitor) throws InterruptedException {
		
		try {
			StringReader input 	= new StringReader(inputString);
			StringWriter output = new StringWriter();
			
			expand(input,output,monitor);
			return output.toString();

		} catch (IOException e) {
			return inputString;
		}
		
	}
	
	/**
	 * Expands the given template in the destination container
	 */
	public void expand(IContainer target, File template, IProgressMonitor monitor) throws CoreException, InterruptedException {
		
		/*
		 * If the template is a directory, recursively expand it
		 */
		if (template.isDirectory())
			expandFolder(target,template,monitor);
		
		/*
		 * If the template is a file just expand it inside the container
		 */
		if (template.isFile()) {
			IPath childPath = new Path(getName(template));
			IFile child = target.getFile(childPath);
			expandFile(child,template,monitor);
			
		}
	}
	
	/**
	 * Recursively expand the template folder into the specified container.
	 * 
	 */
	private void expandFolder(IContainer target, File source, IProgressMonitor monitor) throws CoreException, InterruptedException  {
		for (File childFile : source.listFiles()) {

			/*
			 * Skip unavailable files
			 */
			if (!(childFile.exists() && childFile.canRead()))
				continue;
			
			IPath childPath = new Path(expand(getName(childFile),monitor));
			
			/*
			 * Skip already existing resources
			 */
			if (childFile.isFile() && target.findMember(childPath) != null)
				continue;
			
			/*
			 * Create the resource from the template
			 */
			if (childFile.isDirectory()) {
				IFolder child = target.getFolder(childPath);
				if (!child.exists()) {
					child.create(true,true,monitor);
				}
				expandFolder(child,childFile,monitor);
			}
			
			if (childFile.isFile()) {
				IFile child = target.getFile(childPath);
				expandFile(child,childFile,monitor);
			}
		}
	}
	
	/**
	 * Expands a single template file into the specified container
	 */
	private void expandFile(IFile target, File source, IProgressMonitor monitor) throws CoreException, InterruptedException  {

		InputStream input = null;
		try {
			
			input = new FileInputStream(source);

			/*
			 * If the specified file is not a template just copy the contents, to avoid
			 * unnecessary processing
			 */
			if ( !isTemplate(source)) {
				target.create(input,true,monitor);
				return;
			}
			
			try {
				
				/*
				 * Perform the expansion in a buffer and set the contents
				 * 
				 * NOTE we assume templates are sufficiently small to be handled completely
				 * in memory
				 */
				Writer output	= new StringWriter();
				expand(new InputStreamReader(input),output,monitor);
				target.create(new ByteArrayInputStream(output.toString().getBytes()),true,monitor);
				
			} catch (IOException e) {
				new CoreException(new Status(IStatus.ERROR,"org.xtext.example.partrapDSL",-1,"Error expanding "+source.getName(), e));
			}
			
			
		} catch (FileNotFoundException e) {
		}
		finally {
			try {
				if (input != null)
				input.close();
			} catch (IOException ignored) {
			}
		}
			
		
	}
	
	/**
	 * Determines if the specified file is a template
	 */
	private static boolean isTemplate(File input) {
		return input.getName().endsWith(".template");
	}

	/**
	 * Get the name of a file considering the special case for templates
	 */
	private static String getName(File file) {
		String name = file.getName();
		
		if (isTemplate(file)) 
			name = name.substring(0,name.length() - ".template".length());
		
		return name;
	}
	/**
	 * The states of the variable substitution parser 
	 */
	private enum State { TEXT, BEGIN_VAR, IN_VAR}
	
	private static int EOF = -1;
	
	
	
	/**
	 * Substitutes all occurrences of the variables in the input and write the result to
	 * the output
	 */
	private void expand(Reader inputStream, Writer outputStream, IProgressMonitor monitor) throws IOException, InterruptedException {

		/*
		 * The current state of the recognizer
		 */
		State state = State.TEXT;
		
		/*
		 *	The currently recognized variable name 
		 */
		StringBuffer variableName = new StringBuffer();
		
		boolean ever = true;
		for(;ever;) {

			/*
			 * Just abort in case of requested cancel
			 */
			if (monitor.isCanceled())
				throw new InterruptedException();
			/*
			 * Get input
			 */
			int input = EOF;
			try {
				input = inputStream.read();
			} catch (Exception ignored) {
			}
			
			/*
			 * process according to state
			 */
			switch (state) {
			
				/*
				 * TEXT state just copies input to output until it detects
				 * the beginning of a substitution
				 */
				case TEXT :
					
					if (input == EOF) {
						state = State.TEXT;
					}
					else if (input == '$') {
						state = State.BEGIN_VAR;
					}
					else {
						outputStream.write(input);
						state = State.TEXT;
					}
					
					break;
					
				/*
				 * BEGIN_VAR state looks for the start delimiter "${" of a substitution.
				 * 
				 */
				case BEGIN_VAR:

					
					if (input == EOF) {
						outputStream.write('$');
						state = State.TEXT;
					}
					else if (input == '{') {
						variableName.setLength(0);
						state = State.IN_VAR;
					}
					else {
						outputStream.write('$');
						outputStream.write(input);
						state = State.TEXT;
					}
					break;
					
				/*
				 * IN_VAR state looks for the end delimiter "}" of a substitution, and
				 * then output the substituted value
				 * 
				 */
				case IN_VAR:

					
					if (input == EOF) {
						outputStream.write("${");
						outputStream.write(variableName.toString());
						state = State.TEXT;
					}
					else if (input == '}') {
						String variable = variableName.toString();
						String value 	= variableValues.get(variable);
						
						if (value != null)
							outputStream.write(value);
						else
							outputStream.write("${"+variable+"}");
						
						state = State.TEXT;
					}
					else {
						variableName.append((char)input);
						state = State.IN_VAR;
					}
					break;
						
			}
			
			/*
			 * finish after EOF processing 
			 */
			ever = ! (input == EOF);
		}
	}
}
