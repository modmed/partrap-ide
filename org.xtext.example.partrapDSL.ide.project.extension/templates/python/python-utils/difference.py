###
# #%L
# This file is part of Partrap IDE
# Please visit http://vasco.imag.fr/tools/partrap/ for further information
# 
# Authors : Yoann Blein , Ansem Ben Cheikh 
#           Laboratoire d'Informatique de Grenoble, Team VASCO
# %-
# Copyright (C) 2017 - 2019 University of Grenoble Alpes
# %-
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Lesser Public License for more details.
# 
# You should have received a copy of the GNU General Lesser Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/lgpl-3.0.html>.
# #L%
###
import ast

def calcul_diff(x,y):
    z=x-y;
    return(z);
def is_framed(list):
    verdict = True
    for x in list:
        if(-200<x<250):
            verdict=False
    return verdict
