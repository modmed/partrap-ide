/*-
 * #%L
 * This file is part of Partrap IDE
 * Please visit http://vasco.imag.fr/tools/partrap/ for further information
 * 
 * Authors : Yoann Blein , Ansem Ben Cheikh 
 *           Laboratoire d'Informatique de Grenoble, Team VASCO
 * %-
 * Copyright (C) 2017 - 2019 University of Grenoble Alpes
 * %-
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package partrap.util;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.*;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;


public class Client_socket {


    @SuppressWarnings({ "rawtypes", "unchecked" })
	public static void main(String[] zero) throws InterruptedException{
    	Socket socket;

        try {

        socket = new Socket("localhost",1981);
        BufferedReader plec = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        List<Hashtable<String, Hashtable>> myenv = new ArrayList<Hashtable<String, Hashtable>>();
        Hashtable event= new Hashtable<>();
        event.put("id", "EnterState");
        event.put("time", 123456.9);
        event.put("state", "LoadProfile");
        Hashtable<String, Hashtable> bevent= new Hashtable<>();
        bevent.put("ee", event);
        myenv.add(bevent);
        String python_cond = "ee.state=='LoadProfile'";
        //out = new PrintWriter(socketduserveur.getOutputStream());
        PrintWriter pred = new PrintWriter( socket.getOutputStream());
        pred.println(python_cond);
        pred.flush();
        //String sent= plec.readLine();
        System.out.println("condition sent");
        
        pred.println(myenv);
        
        pred.flush();
        System.out.println("env sent");
             //socket.wait( 10);
        
        String result = plec.readLine();
        System.out.println(result);
        socket.close();

        } catch (IOException e) {
        	e.printStackTrace();

        }

        
     
    }

}
