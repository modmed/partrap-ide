/*-
 * #%L
 * This file is part of Partrap IDE
 * Please visit http://vasco.imag.fr/tools/partrap/ for further information
 * 
 * Authors : Yoann Blein , Ansem Ben Cheikh 
 *           Laboratoire d'Informatique de Grenoble, Team VASCO
 * %-
 * Copyright (C) 2017 - 2019 University of Grenoble Alpes
 * %-
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package partrap.util;


public class IDuration {
	public double value;
	public Unity unit;
	public IDuration(Double val, String u) {
		this.value = val;
		
		 
			  if (u.equals("d")) {
				  this.unit = Unity.d;
			  }
			  if (u.equals("h")) {
				  this.unit = Unity.h;
			  }
			  if (u.equals("min")) {
				  this.unit = Unity.min;
			  }
			  if (u.equals("ms")) {
				  this.unit = Unity.ms;
			  }
			  if (u.equals("s")) {
				  this.unit = Unity.s;
			  }
		  
	}
	public IDuration(Integer val, String u) {
		this.value = val;
		
		 
			  if (u.equals("d")) {
				  this.unit = Unity.d;
			  }
			  if (u.equals("h")) {
				  this.unit = Unity.h;
			  }
			  if (u.equals("min")) {
				  this.unit = Unity.min;
			  }
			  if (u.equals("ms")) {
				  this.unit = Unity.ms;
			  }
			  if (u.equals("s")) {
				  this.unit = Unity.s;
			  }
	}
	
	
	
	public enum Unity {
		ms,
		s,
		h,
		min,
		d;
		
	}

}



