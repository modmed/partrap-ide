/*-
 * #%L
 * This file is part of Partrap IDE
 * Please visit http://vasco.imag.fr/tools/partrap/ for further information
 * 
 * Authors : Yoann Blein , Ansem Ben Cheikh 
 *           Laboratoire d'Informatique de Grenoble, Team VASCO
 * %-
 * Copyright (C) 2017 - 2019 University of Grenoble Alpes
 * %-
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package partrap.util;

import java.util.Hashtable;
import java.util.List;

public class Short_Result_Explanation {
	@SuppressWarnings("rawtypes")
	public StringBuilder explain(Scopereturns scope, List<Hashtable> events_occ,IPattern pattern,List<Hashtable> traceList) {
		if(scope!=null) {
			Integer to= scope.to;
			Integer from = scope.from;
			StringBuilder display_result= new StringBuilder();
			//display_result.append("False because : \n");
			if(!from.equals(-1)) {
				if(to.equals(-1)) {
					to =traceList.size();
				}
				else {
					to=to+1;
				}
				from=from+1;
				if(pattern.Type.equals("absence_of")) {
					String pl="";
					if (events_occ.size()>1) {
						pl="s";
					}
					display_result.append("-In the scope from "+from +" to "+to+ " with the environment:"+scope.environment+"\n");
					display_result.append("found "+events_occ.size() +" event"+pl+" that should not occur: "+"\n");
					for(Hashtable event: events_occ) {
						display_result.append(event+"\n");
					}
				}
				if(pattern.Type.equals("occurrence_of")) {
					if (pattern.n==0) {
						String pl="";
						if (events_occ.size()>1) {
							pl="s";
						}
						display_result.append("-In the scope from "+from +" to "+to+ " with the environment:"+scope.environment+"\n");
						display_result.append("found "+events_occ.size() +" event"+pl+" that should not occur: "+"\n");
						for(Hashtable event: events_occ) {
							display_result.append(event+"\n");
						}
					}
					if (pattern.n==1) {
						display_result.append("-In the scope from "+from +" to "+to+ " with the environment:"+scope.environment+"\n");
						if(!pattern.event.condition.equals("")) {
							display_result.append("No event "+ pattern.event.var+" of type \'"+pattern.event.id +"\' and satisfying the condition \'"+pattern.event.condition+"\' is found"+"\n");
						}
						else {
							display_result.append("No event of type \'"+pattern.event.id +"\' is found"+"\n");
						}
					}
					if (pattern.n>1) {
						display_result.append("-In the scope from "+from +" to "+to+ " with the environment:"+scope.environment+"\n");
						if(events_occ.isEmpty()) {
							if(!pattern.event.condition.equals("")) {
								display_result.append("No event "+ pattern.event.var+" of type \'"+pattern.event.id +"\' and satisfying the condition \'"+pattern.event.condition+"\' is found"+"\n");
							}
							else {
								display_result.append("No event of type \'"+pattern.event.id +"\' is found"+"\n");
							}
						}
						else {
							String pl="";
							if (events_occ.size()>1) {
								pl="s";
							}
							if(!pattern.event.condition.equals("")) {
								display_result.append("Only "+events_occ.size()+" event"+pl+" of type \'"+pattern.event.id +"\' and satisfying the condition \'"+pattern.event.condition.toString()+"\' are found"+"\n");
								for(Hashtable event: events_occ) {
									display_result.append(event+"\n");
								}
							}
							else {
								display_result.append("Only "+events_occ.size()+" event"+pl+" of type \'"+pattern.event.id +"\' are found"+"\n");
								for(Hashtable event: events_occ) {
									display_result.append(event+"\n");
								}
							}
						}
					}
				}
				if(pattern.Type.equals("preceded_by")) {
					Hashtable hevent= traceList.get(to+1);
					display_result.append("-Before the event "+ pattern.event.var+ " : "+hevent.toString()+",\n");
					if(!pattern.event2.condition.equals("")) {
						display_result.append("No event "+ pattern.event2.var+" of type \'"+pattern.event2.id +"\' and satisfying the condition \'"+pattern.event2.condition.toString()+"\' is found"+"\n");
					}
					else {
						display_result.append("No event of type \'"+pattern.event2.id +"\' is found"+"\n");
					}
				}
				if(pattern.Type.equals("followed_by")) {
					Hashtable hevent= traceList.get(from-1);
					display_result.append("-After the event "+ pattern.event.var+ " : "+hevent.toString()+",\n");
					if(!pattern.event2.condition.equals("")) {
						display_result.append("No event "+ pattern.event.var+" of type \'"+pattern.event2.id +"\' and satisfying the condition \'"+pattern.event2.condition+"\' is found"+"\n");
					}
					else {
						display_result.append("No event of type \'"+pattern.event2.id +"\' is found"+"\n");
					}
					
				}
				if(pattern.Type.equals("prevents")) {
					Hashtable hevent= traceList.get(from-1);
					String pl="";
					if (events_occ.size()>1) {
						pl="s";
					}
					display_result.append("-After the event "+ pattern.event.var+ " : "+hevent.toString()+",\n");
					display_result.append("found "+events_occ.size() +" event"+pl+" that should not occur :"+"\n");
					for(Hashtable event: events_occ) {
						display_result.append(event+"\n");
					}
				}
				
			}
			else {
				if(scope.environment.isEmpty()) {
					display_result.append("An empty scope is found"+ "\n");
				}
				else {
					display_result.append("An empty scope is found with the environment"+ scope.environment+  "\n");
				}
			}
			return display_result;
		}
		else {
			StringBuilder display_result = new StringBuilder();
			display_result.append("True result due to a null scope used to evaluate the pattern "+pattern.Type);
			return display_result;
		}
	}
	@SuppressWarnings("rawtypes")
	public static StringBuilder cexplain(Scopereturns scope, List<Hashtable> events_occ,IPattern pattern) {
		StringBuilder display_result= new StringBuilder();
		//display_result.append("False because : \n");
		String var ="";
		if(pattern.event.var!=null) {
			var= pattern.event.var;
		}
				if(pattern.Type.equals("absence_of")) {
					String pl="";
					if (events_occ.size()>1) {
						pl="s";
					}
					display_result.append("found "+events_occ.size() +" event"+pl+" that should not occur: "+"\n");
					for(Hashtable event: events_occ) {
						display_result.append(event+"\n");
					}
				}
				else if(pattern.Type.equals("occurrence_of")) {
					if (pattern.n==0) {
						String pl="";
						if (events_occ.size()>1) {
							pl="s";
						}
						display_result.append("found "+events_occ.size() +" event"+pl+" that should not occur: "+"\n");
						for(Hashtable event: events_occ) {
							display_result.append(event+"\n");
						}
					}
					if (pattern.n==1) {
						if(!pattern.event.condition.equals("")) {
							display_result.append("No event "+ var+" of type \'"+pattern.event.id +"\' and satisfying the condition \'"+pattern.event.condition+"\' is found"+"\n");
						}
						else {
							display_result.append("No event of type \'"+pattern.event.id +"\' is found"+"\n");
						}
					}
					if (pattern.n>1) {
						if(events_occ.isEmpty()) {
							if(!pattern.event.condition.equals("")) {
								display_result.append("No event "+ var+" of type \'"+pattern.event.id +"\' and satisfying the condition \'"+pattern.event.condition+"\' is found"+"\n");
							}
							else {
								display_result.append("No event of type \'"+pattern.event.id +"\' is found"+"\n");
							}
						}
						else {
							String pl="";
							if (events_occ.size()>1) {
								pl="s";
							}
							if(!pattern.event.condition.equals("")) {
								display_result.append("Only "+events_occ.size()+" event"+pl+" of type \'"+pattern.event.id +"\' and satisfying the condition \'"+pattern.event.condition.toString()+"\' are found"+"\n");
							}
							else {
								display_result.append("Only "+events_occ.size()+" event"+pl+" of type \'"+pattern.event.id +"\' are found"+"\n");
							}
						}
					}
				}
				else if(scope!=null) {
			Integer from = scope.from;
			if(!from.equals(-1)) {	
				if(pattern.Type.equals("preceded_by")) {
					display_result.append("-Before the event "+ var+ " : "+scope.hevent1.toString()+",\n");
					if(!pattern.event2.condition.equals("")) {
						display_result.append("No event "+ var+" of type \'"+pattern.event2.id +"\' and satisfying the condition \'"+pattern.event2.condition.toString()+"\' is found"+"\n");
					}
					else {
						display_result.append("No event of type \'"+pattern.event2.id +"\' is found"+"\n");
					}
				}
				if(pattern.Type.equals("followed_by")) {
					display_result.append("-After the event "+var+ " : "+scope.hevent1.toString()+",\n");
					if(!pattern.event2.condition.equals("")) {
						display_result.append("No event "+ var+" of type \'"+pattern.event2.id +"\' and satisfying the condition \'"+pattern.event2.condition+"\' is found"+"\n");
					}
					else {
						display_result.append("No event of type \'"+pattern.event2.id +"\' is found"+"\n");
					}
					
				}
				if(pattern.Type.equals("prevents")) {
					String pl="";
					if (events_occ.size()>1) {
						pl="s";
					}
					display_result.append("-After the event "+ var+ " : "+scope.hevent1.toString()+",\n");
					display_result.append("found "+events_occ.size() +" event"+pl+" that should not occur :"+"\n");
					for(Hashtable event: events_occ) {
						display_result.append(event+"\n");
					}
				}
		}
		else {
			if(scope.environment.isEmpty()) {
				display_result.append("An empty scope is found"+ "\n");
			}
			else {
				display_result.append("An empty scope is found with the environment"+ scope.environment+  "\n");
			}
		}	
		}
		return display_result;
		
	}
	//@SuppressWarnings("rawtypes")
	public static StringBuilder sexplain(Scopereturns scope, IScope iscope) {
		StringBuilder display_result= new StringBuilder();
		if(scope!=null) {
			if(iscope!=null) {
			if(scope.from!=-1) {
				if(scope.environment.isEmpty()) {
					display_result.append("Scope from  "+ (scope.from+1)+ " to "+(scope.to+1)+ " with an empty environment \n");
				}
				else {
					display_result.append("Scope from  "+ (scope.from+1)+ " to "+(scope.to+1)+ " with the environment "+scope.environment+":\n");
				}
				
				String var ="";
				if (iscope.event.var!=null){
					var =iscope.event.var;}
				if (iscope.event2==null) {
					display_result.append(iscope.Type+" the event "+ var+ " : "+scope.hevent1.toString()+",\n");
				}
				else {
					String var2 ="";
					if (iscope.event2.var!=null){var2 =iscope.event2.var;}
					if(scope.hevent2!=null) {
						if (iscope.Type.equals("between")) {
							display_result.append(iscope.Type+" the event "+var+ " : "+scope.hevent1.toString()+" and the event "+ var2+ " : "+scope.hevent2.toString()+",\n");
						}
						else {
							display_result.append(iscope.Type+" the event "+var+ " : "+scope.hevent1.toString()+" until the event "+ var2+ " : "+scope.hevent2.toString()+",\n");
							
						}
					}
					else {
						display_result.append(iscope.Type+" the event "+var+ " : "+scope.hevent1.toString()+",\n");
					}
				}
			}
			else {
				if(scope.environment.isEmpty()) {
					display_result.append("An empty scope is found"+ "\n");
				}
				else {
					display_result.append("An empty scope is found with the environment"+ scope.environment+  "\n");
				}
			}
			}
			else {
				display_result.append("Scope from  "+ (scope.from+1)+ " to "+(scope.to+1)+ " with the environment "+scope.environment+"\n");
			}
		}
		else {
			display_result.append("A null scope is found ");
			return display_result;
		}
		return display_result;
	}

}
