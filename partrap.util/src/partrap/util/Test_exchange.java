/*-
 * #%L
 * This file is part of Partrap IDE
 * Please visit http://vasco.imag.fr/tools/partrap/ for further information
 * 
 * Authors : Yoann Blein , Ansem Ben Cheikh 
 *           Laboratoire d'Informatique de Grenoble, Team VASCO
 * %-
 * Copyright (C) 2017 - 2019 University of Grenoble Alpes
 * %-
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package partrap.util;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

public class Test_exchange {
	  
	public static  void main(String[] args) throws IOException {
		
		String path=null;
		 JFileChooser chooser = chooser();//choose a trace file or a directory of traces
		    
         try {
         	File file = chooser.getSelectedFile();
         	if (file.isDirectory()){
     			//var filter = new FileNameExtensionFilter("Json", "json");
     			 File[] file_list= file.listFiles(new FilenameFilter() {
     				 @Override
     				 public boolean accept(File file, String name) {
     					 return name.toLowerCase().endsWith(".json");
     				 }
     			 });
     			 // while used to guarantee the choice of a directory containing json files
     			 while(file_list.length==0 && file.isDirectory()) {
     				 chooser = chooser();
     				 file = chooser.getSelectedFile();
  	            	if (file.isDirectory()){
  	        			//var filter = new FileNameExtensionFilter("Json", "json");
  	            		file_list=null;
  	        			 file_list= file.listFiles(new FilenameFilter() {
  	        				 @Override
  	        				 public boolean accept(File file, String name) {
  	        					 return name.toLowerCase().endsWith(".json");
  	        				 }
  	        			 });
  	            	}
     			 }
         	}
            	String filepath= chooser.getSelectedFile().getAbsolutePath();
        		String filepath2=filepath.replace("\\","\\\\");
        		path= Test_exchange.class.getProtectionDomain().getCodeSource().getLocation().getPath();
        		path= path.substring(0, path.length()-4);
        		path = path +"\\trace_files.txt";
        		FileWriter fw= new FileWriter(path);
        		fw.write("");
        		
                BufferedWriter out = new BufferedWriter(fw);
                out.flush();
                out.write(filepath2);
                out.close();
            }
            catch (IOException e)
            {
                System.out.println("Exception ");       
            }
            FileReader chosen_file = new FileReader(path);
            		BufferedReader br = new BufferedReader(chosen_file);
            		 StringBuilder sb = new StringBuilder();
                try {
                    
                    String line = br.readLine();
            		sb.append(line);
                    
                	} finally {
                    br.close();
                }
		List<String> traces_list=  new ArrayList<String>();
		String trace = sb.toString();
		File file = new File(trace);
		if (file.isDirectory()){
			
			 File[] file_list= file.listFiles(new FilenameFilter() {
				 @Override
				 public boolean accept(File file, String name) {
					 return name.toLowerCase().endsWith(".json");
				 }
			 });
			for (File tracefile:file_list){
				String filepath= tracefile.getAbsolutePath();
        		String filepath2=filepath.replace("\\","\\\\");
				traces_list.add(filepath2);
			}
			
		}
		else{
			if (file.isFile()){
				traces_list.add(trace);
			}
		}
		System.out.println("this is my trace list : "+ traces_list);
	}
	
	/***********************/
        private static void setFileChooserFont(Component[] comp)
        {  
        	Font font = new Font("SanSerif",Font.PLAIN,24);
          for(int x = 0; x < comp.length; x++)  
          {  
            if(comp[x] instanceof Container) setFileChooserFont(((Container)comp[x]).getComponents());  
            try{comp[x].setFont(font);}  
            catch(Exception e){}//do nothing  
          }  
        }  
        /*****************************/
        private static JFileChooser chooser(){
    		
    		JFileChooser chooser = new JFileChooser();
    		chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
            FileNameExtensionFilter filter = new FileNameExtensionFilter("Json", "json");
            chooser.setFileFilter(filter);
            chooser.setPreferredSize(new Dimension(1200, 800));
            chooser.setDialogTitle("Choose a Trace file or a directory");
            setFileChooserFont(chooser.getComponents());
            int returnVal = chooser.showOpenDialog(null);
            if(returnVal == JFileChooser.APPROVE_OPTION) {
                System.out.println("You chose to open this file: " +
                        chooser.getSelectedFile().getName());
                return chooser;
    		
            }
            else {
            	return null;
            }
    	}

}
