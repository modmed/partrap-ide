/*-
 * #%L
 * This file is part of Partrap IDE
 * Please visit http://vasco.imag.fr/tools/partrap/ for further information
 * 
 * Authors : Yoann Blein , Ansem Ben Cheikh 
 *           Laboratoire d'Informatique de Grenoble, Team VASCO
 * %-
 * Copyright (C) 2017 - 2019 University of Grenoble Alpes
 * %-
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package partrap.util;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.FileNotFoundException;


import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;


public class EventExtract {
	@SuppressWarnings({ "rawtypes" })
	public static void main(String[] args) throws Exception {
		try {
		FileReader trace = new FileReader("C:\\Users\\vasco\\Documents\\traces\\trace1.json");
		List<Hashtable> traceEventslist = Convertjsonlist.convert(trace);
		 List<Scopereturns> limitlists0= new ArrayList<Scopereturns>();
		    Scopereturns initscope = new Scopereturns(0,-1, null, null, null);
		    limitlists0.add(initscope);
		    							
		    System.out.println("indirect pattern");	
          	IDuration duration0 = null; 
         //2
          //	IEvent event0 = new IEvent ("HipCenter", "hh",null); 
          	IEvent event= new IEvent("EnterState", "e");
          	IEvent event2= new IEvent("TrackerDetected", "a");
         IPattern pattern0= new IPattern( "absence_of", event2, null, duration0,null);
         Occurrence oo = Occurrence.convert("last");
 		//IDuration duration = new IDuration(1.5, "min");
 		IScope scope = new IScope("since", oo, event, event2 ,null, null , true );
 		Scopereturns ss= new Scopereturns(0, -1, null, null, null);
 		List<Scopereturns> scopeList =scope.evaluate(traceEventslist,ss);
 		//IScope scope2 = new IScope("before", oo, event, null ,null, null , null );
 		//List<Scopereturns> scopeList2 =scope2.evaluates(traceEventslist,scopeList);
 		//Scope_display x= new Scope_display();
 		//x.display("before", "each", scopeList);
 		 boolean value0 = true;
 		 for (Scopereturns scopereturns: scopeList) {
 			 if (scopereturns!=null) {
 		//System.out.println("before evaluates :"+scopeList.size()+": "+scopereturns.environment+" : this is from to  : "+scopereturns.from+" : " +scopereturns.to);
         //List<Scopereturns> limitlists = pattern0.evaluate(traceEventslist,scopereturns.from, scopereturns.to, scopereturns.environment);
         /*String thisname= limitlists.getClass().getName();
         Object object = Class.forName(thisname).newInstance();
        
         
         System.out.println("Object : "+ object.getClass().getName());*/
         //boolean xx= pattern0.cond_verdict(limitlists);
        // System.out.println("after evaluates :"+limitlists.size()+": "+scopereturns.environment.size()+" : this is from to  : "+scopereturns.from+" : " +scopereturns.to);
         
        
         //value0 = xx && value0;
         }
 			 else {value0=value0 && pattern0.null_verdict();}
         }
           System.out.println("this is my value : "+value0);
		    
		//IEvent event= new IEvent("EnterState", "myenter");
		/*IEvent event2= new IEvent("TrackerDetected", "myexit");
		@SuppressWarnings("null")
		Occurrence oo = Occurrence.convert("each");
		Duration duration = new Duration(1.5, "min");
		IScope scope = new IScope("before", oo, event, null ,null, null , null );
		List<Scopereturns> scopeList =scope.evaluate(traceEventslist,0,-1,null);
		for (Scopereturns scopereturns: scopeList) {
		System.out.println(scopeList.size()+": "+scopereturns.environment.size()+" : this is from to  : "+scopereturns.from+" : " +scopereturns.to);
		}*/
		
		/*String record=event.id;
		String par=event.id;
		for (Scopereturns scopereturns : scopeList) {
			List<Hashtable<String, Hashtable>> myenv =scopereturns.environment;
			boolean name_in_env = false;
			boolean record_exists = false;
			boolean recordtype_exists = false;
			for (Hashtable<String, Hashtable> bevent: myenv) {
				if (bevent.containsKey(record)) {
					Hashtable hevent = bevent.get(record);
					name_in_env = true;
					if (hevent.containsKey(par)) {
						record_exists= true;
						if (hevent.get(par).getClass().toString().equals("ArrayList")) {
							recordtype_exists = true;
						Object valueuu= hevent.get(par);}
					}
						
					
				}
			}
			if (!name_in_env) {
				System.out.println("error in condition where: no viable event name : "+record);
				break;
			}
			else {
			if (!record_exists) {
				System.out.println("error in condition where: no viable record : "+record+"."+par);
				break;
			}
			else {
			if (!recordtype_exists) {
				System.out.println("error in condition where: no viable record Type :"+record+"."+par);
				break;
			}
			}
			}
		}*/
		
		//Pattern pattern = new Pattern("prevents", event2, event, null, null);
		//pattern.evaluate(traceEventslist, 0, -1);
		//String eventname= new String("TrackerDetected");
		//System.out.println("work done done");
		//extractTrace(trace, eventname);
		}
		catch (FileNotFoundException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		 
       /* JSONObject event = new JSONObject();
        event.put("Name", "India");
        event.put("Population", new Integer(1000000));
 
        JSONArray listOfStates = new JSONArray();
        listOfStates.add("Madhya Pradesh");
        listOfStates.add("Maharastra");
        listOfStates.add("Rajasthan");
 
        event.put("States", listOfStates);*/
 
       /* try {
            
            // Writing to a file
            File file=new File("C:\\Users\\vasco\\Documents\\traces\\CountryJSONFile.json");
            file.createNewFile();
            
            FileWriter fileWriter = new FileWriter(file);
            System.out.println("Writing JSON object to file");
            System.out.println("-----------------------");
            System.out.print(event);
 
            fileWriter.write(event.toJSONString());
            fileWriter.flush();
           fileWriter.close();
 
        } catch (IOException e) {
            e.printStackTrace();
        }*/
 
    }
	@SuppressWarnings("unchecked")
	public static void extractTrace(FileReader tracefile, String eventid) throws IOException {
		JSONParser parser = new JSONParser();
		File trace1=new File("C:\\Users\\vasco\\Documents\\traces\\trace1.json");
        trace1.createNewFile();
        FileWriter traceWriter = new FileWriter(trace1);
        JSONArray listOfEvents = new JSONArray();

        try { 
        	JSONArray a = (JSONArray) parser.parse(tracefile);
        	for (Object o : a)
        	  {
        	    JSONObject traceEvent = (JSONObject) o;
        	    //System.out.print(traceEvent);
        	   // ObjectMapper mapper = new ObjectMapper();
        	    //TraceEvent traceEvent2 = mapper.readValue(traceEvent.toString(),TraceEvent.class);
        	   // System.out.println(traceEvent2.state);

        	    String name = (String) traceEvent.get("id");
        	   // System.out.println(name+"="+eventid);
        	    if (name.equals(eventid)) {
        	    listOfEvents.add(traceEvent);
        	    System.out.println(name);
        	    /*Interpreter interpreter = new Interpreter();
        	    interpreter.eval("result = (7+21*6)/(32-27)");
        	    System.out.println(interpreter.get("result"));*/
        	  
                //traceWriter.close();
        	    }
        	  }
        	  traceWriter.write(listOfEvents.toJSONString());
      	      traceWriter.flush();
        	  traceWriter.close();
        	
        }catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
		
		
	}
	
	
	

}
