/*-
 * #%L
 * This file is part of Partrap IDE
 * Please visit http://vasco.imag.fr/tools/partrap/ for further information
 * 
 * Authors : Yoann Blein , Ansem Ben Cheikh 
 *           Laboratoire d'Informatique de Grenoble, Team VASCO
 * %-
 * Copyright (C) 2017 - 2019 University of Grenoble Alpes
 * %-
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package partrap.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


/** This class is used to convert a hashtable that associate an occurred event with scope to alist of scopes
 * the opposite is done in the "reconvert" method to select a set of mappings between events and scopes 
 * according to a set of scopes
 * this class is used by the compiled classes to combine the use of two result types:
 * @Hashtable<Hashtable,Scopereturns>: mapping between events and scopes to analyse and display a result summary
 * @List<Scopereturns>: to execute expressions*/
public class Convert_occurrences {
	@SuppressWarnings("rawtypes")
	public List<Scopereturns> convert(Hashtable<Hashtable,Scopereturns> final_occ){
		if (final_occ!=null) {
			List<Scopereturns> occ_list = new ArrayList<Scopereturns>();
			for (Hashtable hevent: final_occ.keySet()) {
				occ_list.add(final_occ.get(hevent));
				//System.out.println("convert : "+hevent);
			}
			return occ_list;
		}
		else {
		 return null;
		}
	}
	@SuppressWarnings("rawtypes")
	public Hashtable<Hashtable,Scopereturns> reconvert(Hashtable<Hashtable,Scopereturns> final_occ,List<Scopereturns> occ_list){
		if (final_occ!=null) {
			Hashtable<Hashtable,Scopereturns> cond_occ = new Hashtable<Hashtable,Scopereturns>();
			for (Scopereturns scope: occ_list) {
				for (Hashtable hevent: final_occ.keySet()) {
					if(scope.equals(final_occ.get(hevent))) {
						cond_occ.put(hevent, scope);
						//System.out.println("reconvert : "+hevent);
					}
				}
			}
			
			return cond_occ;
		}
		else {
		 return null;
		}
	}
	@SuppressWarnings("rawtypes")
	public List<Hashtable> convert_to_list(Hashtable<Hashtable,Scopereturns> final_occ){
		if (final_occ!=null) {
			List<Hashtable> occ_events= new ArrayList<Hashtable> ();
			for (Hashtable hevent: final_occ.keySet()) {
				occ_events.add(hevent);
				//System.out.println("convert to list : "+hevent);
			}
			
			return occ_events;
		}
		else {
		 return null;
		}
	}
	@SuppressWarnings("rawtypes")
	public List<Scopereturns> convert(Map<Hashtable,Scopereturns> final_occ){
		if (final_occ!=null) {
			List<Scopereturns> occ_list = new ArrayList<Scopereturns>();
			for (Hashtable hevent: final_occ.keySet()) {
				occ_list.add(final_occ.get(hevent));
				//System.out.println("convert : "+hevent);
			}
			return occ_list;
		}
		else {
		 return null;
		}
	}
	@SuppressWarnings("rawtypes")
	public Map<Hashtable,Scopereturns> reconvert(Map<Hashtable,Scopereturns> final_occ,List<Scopereturns> occ_list){
		if (final_occ!=null) {
			Map<Hashtable,Scopereturns> cond_occ = Collections.synchronizedMap( new LinkedHashMap<Hashtable,Scopereturns>());
			for (Scopereturns scope: occ_list) {
				for (Hashtable hevent: final_occ.keySet()) {
					if(scope.equals(final_occ.get(hevent))) {
						cond_occ.put(hevent, scope);
						//System.out.println("reconvert : "+hevent);
					}
				}
			}
			
			return cond_occ;
		}
		else {
		 return null;
		}
	}
	@SuppressWarnings("rawtypes")
	public List<Hashtable> convert_to_list(Map<Hashtable,Scopereturns> final_occ){
		if (final_occ!=null) {
			List<Hashtable> occ_events= new ArrayList<Hashtable> ();
			for (Hashtable hevent: final_occ.keySet()) {
				occ_events.add(hevent);
				//System.out.println("convert to list : "+hevent);
			}
			
			return occ_events;
		}
		else {
		 return null;
		}
	}
}
