/*-
 * #%L
 * This file is part of Partrap IDE
 * Please visit http://vasco.imag.fr/tools/partrap/ for further information
 * 
 * Authors : Yoann Blein , Ansem Ben Cheikh 
 *           Laboratoire d'Informatique de Grenoble, Team VASCO
 * %-
 * Copyright (C) 2017 - 2019 University of Grenoble Alpes
 * %-
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package partrap.util;

public abstract class Result {
    public String trace_path = new String();
    //protected static int level = 1;
    public IScope iScope ;
    public Composite_Issue parent=null;
    public Scopereturns scope;
    
    public boolean verdict=true;
    
    public void update_verdict(boolean x) {
    	verdict=x;
    	
    }
    
    public abstract StringBuilder  traverse(StringBuilder initial,int i,boolean view) ;
    
    public void update_false_verdict() {
    	
    	if((this instanceof Composite_Result)||(this instanceof Prop_Result)||(this instanceof Low_Result)) {
    		update_verdict(false);
    		if(parent!=null) {
        		Composite_Issue iparent= parent;
        		iparent.update_false_verdict();
        		
        	}
    	}
    	else {
    		if(this instanceof Or_Result) {
    			Or_Result result= (Or_Result)this;
    			result.verdict=result.left.verdict||result.right.verdict;
    			if(parent!=null && !result.verdict) {
            		Composite_Issue iparent= parent;
            		iparent.update_false_verdict();
            	}
    		}
    		if(this instanceof Implies_Result) {
    			Implies_Result result= (Implies_Result)this;
    			result.verdict=!result.left.verdict||result.right.verdict;
    			System.out.println("implies verdict: "+result.verdict);
    			if(parent!=null && !result.verdict) {
            		Composite_Issue iparent= parent;
            		iparent.update_false_verdict();
            	}
    			if(parent!=null && verdict) {
            		Composite_Issue iparent= parent;
            		iparent.update_true_verdict();
            	}
    		}
    		if(this instanceof Equiv_Result) {
    			Equiv_Result result= (Equiv_Result)this;
    			result.verdict=(!result.left.verdict||result.right.verdict)&&(!result.right.verdict||result.left.verdict);
    			if(parent!=null && !result.verdict) {
            		Composite_Issue iparent= parent;
            		iparent.update_false_verdict();
            	}
    			if(parent!=null && verdict) {
            		Composite_Issue iparent= parent;
            		iparent.update_true_verdict();
            	}
    		}
    		if(this instanceof And_Result) {
    			And_Result result= (And_Result)this;
    			result.verdict=result.left.verdict&&result.right.verdict;
    			if(parent!=null && !result.verdict) {
            		Composite_Issue iparent= parent;
            		iparent.update_false_verdict();
            	}
    		}
    		if(this instanceof Not_Result) {
    			/*Not_Result result= (Not_Result)this;
    			result.verdict=!result.unary.verdict;
    			if(parent!=null && !verdict) {
            		Composite_Issue iparent= parent;
            		iparent.update_false_verdict();
            	}*/
    		}
    		if(this instanceof Forall_Result) {
    			Forall_Result result= (Forall_Result)this;
    			result.verdict=true;
    			for(Result child: result.children) {
    				result.verdict=result.verdict&&child.verdict;
    			}
    			if(parent!=null && !verdict) {
            		Composite_Issue iparent= parent;
            		iparent.update_false_verdict();
            	}
    		}
    		if(this instanceof Exists_Result) {
    			Exists_Result result= (Exists_Result)this;
    			verdict=true;
    			for(Result child: result.children) {
    				verdict=verdict||child.verdict;
    			}
    			if(parent!=null && !verdict) {
            		Composite_Issue iparent= parent;
            		iparent.update_false_verdict();
            	}
    			if(parent!=null && verdict) {
            		Composite_Issue iparent= parent;
            		iparent.update_true_verdict();
            	}
    		}
    	}
    
    }
    
    public void update_true_verdict() {
    	if((this instanceof Composite_Result)||(this instanceof Prop_Result)||(this instanceof Low_Result)) {
    		update_verdict(true);
    		if(parent!=null) {
        		Composite_Issue iparent= parent;
        		iparent.update_true_verdict();
        		
        	}
    	}
    	else {
    		if(this instanceof Or_Result) {
    			Or_Result result= (Or_Result)this;
    			result.verdict=result.left.verdict||result.right.verdict;
    			if(parent!=null && result.verdict) {
            		Composite_Issue iparent= parent;
            		iparent.update_true_verdict();
            	}
    		}
    		if(this instanceof Implies_Result) {
    			Implies_Result result= (Implies_Result)this;
    			result.verdict=!result.left.verdict||result.right.verdict;
    			System.out.println("implies verdict: "+result.verdict);
    			if(parent!=null && !result.verdict) {
            		Composite_Issue iparent= parent;
            		iparent.update_false_verdict();
            	}
    			if(parent!=null && result.verdict) {
            		Composite_Issue iparent= parent;
            		iparent.update_true_verdict();
            	}
    		}
    		if(this instanceof Equiv_Result) {
    			Equiv_Result result= (Equiv_Result)this;
    			result.verdict=(!result.left.verdict||result.right.verdict)&&(!result.right.verdict||result.left.verdict);
    			if(parent!=null && !result.verdict) {
            		Composite_Issue iparent= parent;
            		iparent.update_false_verdict();
            	}
    			if(parent!=null && result.verdict) {
            		Composite_Issue iparent= parent;
            		iparent.update_true_verdict();
            	}
    		}
    		if(this instanceof And_Result) {
    			And_Result result= (And_Result)this;
    			result.verdict=result.left.verdict&&result.right.verdict;
    			if(parent!=null && !result.verdict) {
            		Composite_Issue iparent= parent;
            		iparent.update_false_verdict();
            	}
    			if(parent!=null && result.verdict) {
            		Composite_Issue iparent= parent;
            		iparent.update_true_verdict();
            	}
    		}
    		if(this instanceof Not_Result) {
    			/*Not_Result result= (Not_Result)this;
    			result.verdict=!result.unary.verdict;
    			if(parent!=null && !verdict) {
            		Composite_Issue iparent= parent;
            		iparent.update_false_verdict();
            	}*/
    		}
    		if(this instanceof Forall_Result) {
    			Forall_Result result= (Forall_Result)this;
    			result.verdict=true;
    			for(Result child: result.children) {
    				result.verdict=result.verdict&&child.verdict;
    			}
    			if(parent!=null && !verdict) {
            		Composite_Issue iparent= parent;
            		iparent.update_false_verdict();
            	}
    			if(parent!=null && verdict) {
            		Composite_Issue iparent= parent;
            		iparent.update_true_verdict();
            	}
    		}
    		if(this instanceof Exists_Result) {
    			Exists_Result result= (Exists_Result)this;
    			verdict=true;
    			for(Result child: result.children) {
    				verdict=verdict||child.verdict;
    			}
    			if(parent!=null && !verdict) {
            		Composite_Issue iparent= parent;
            		iparent.update_false_verdict();
            	}
    			if(parent!=null && verdict) {
            		Composite_Issue iparent= parent;
            		iparent.update_true_verdict();
            	}
    		}
    	}
    }
    
    public boolean not() {
    	return (!verdict);
    }
}




