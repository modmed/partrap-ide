/*-
 * #%L
 * This file is part of Partrap IDE
 * Please visit http://vasco.imag.fr/tools/partrap/ for further information
 * 
 * Authors : Yoann Blein , Ansem Ben Cheikh 
 *           Laboratoire d'Informatique de Grenoble, Team VASCO
 * %-
 * Copyright (C) 2017 - 2019 University of Grenoble Alpes
 * %-
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package partrap.util;

import java.util.Hashtable;
import java.util.List;


	@SuppressWarnings("rawtypes")
public class Low_Result extends Result {
	    public IPattern ipattern;
	    public List<Hashtable> occ_list;
	    public Low_Result(IPattern pattern, List<Hashtable> occurences,Scopereturns scoper) {
	        ipattern = pattern;
	        occ_list = occurences;
	        scope= scoper;
	    }

	    public StringBuilder traverse(StringBuilder initial,int i,boolean view) {
	    	StringBuilder resulttext= initial;
	    	//List<Result> children;
	    	for(int j=1;j<=i; j++) {
	    		resulttext.append("--");
				}
			i++;
	    	if(view) {
	    		if(verdict) {
	    			resulttext.append(Long_Result_Explanation.cexplain(scope,occ_list,ipattern).toString());
	    		}
	    		else {
	    			resulttext.append(Short_Result_Explanation.cexplain(scope,occ_list,ipattern).toString());
	    		}
	    	}
	    	else {
	    		if(!verdict) {
	    			resulttext.append(Short_Result_Explanation.cexplain(scope,occ_list,ipattern).toString());
	    		}
	    	}
	       return resulttext;
	    }
	
}
