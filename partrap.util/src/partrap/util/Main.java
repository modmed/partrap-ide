/*-
 * #%L
 * This file is part of Partrap IDE
 * Please visit http://vasco.imag.fr/tools/partrap/ for further information
 * 
 * Authors : Yoann Blein , Ansem Ben Cheikh 
 *           Laboratoire d'Informatique de Grenoble, Team VASCO
 * %-
 * Copyright (C) 2017 - 2019 University of Grenoble Alpes
 * %-
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package partrap.util;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Hashtable;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;


public class Main {
    static Thread sent;
    static Thread receive;
    static Socket socket;

    public static void main(String args[]){
            try {
                socket = new Socket("localhost",9999);
            } catch (UnknownHostException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            } catch (IOException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
            sent = new Thread(new Runnable() {

                @SuppressWarnings({ "unchecked", "rawtypes" })
				@Override
                public void run() {
try {
    BufferedReader stdIn =new BufferedReader(new InputStreamReader(socket.getInputStream()));
    PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
    Hashtable<String, Hashtable> myenv = new Hashtable<String, Hashtable>();
    Hashtable event= new Hashtable<>();
    event.put("id", "EnterState");
    event.put("time", 123456.9);
    event.put("state", "LoadProfile");
    //Hashtable<String, Hashtable> bevent= new Hashtable<>();
    myenv.put("ee", event);
    
    JSONArray arrj= new JSONArray();
    JSONObject json = new JSONObject();
    
    json.putAll( myenv);
    arrj.add(json);
    System.out.printf( "JSON: %s", arrj.toString() );
    //String step1= StringUtils.join("test","\'");
    //String step2= StringUtils.join("\'",step1);
    System.out.println("envir to send "+ arrj);
    String python_cond = "ee.state=='LoadProfile'";
        System.out.println("Trying to read...");
            String in = stdIn.readLine();
            System.out.println(in);
            out.print(python_cond);
            out.flush();
            System.out.println("Message sent");
            String to = stdIn.readLine();
            System.out.println(to);
            
            out.print(json);
            out.flush();
        

} catch (IOException e) {
    // TODO Auto-generated catch block
    e.printStackTrace();
}


}
});
sent.start();
try {
sent.join();
} catch (InterruptedException e) {
// TODO Auto-generated catch block
e.printStackTrace();
}

}

}
