/*-
 * #%L
 * This file is part of Partrap IDE
 * Please visit http://vasco.imag.fr/tools/partrap/ for further information
 * 
 * Authors : Yoann Blein , Ansem Ben Cheikh 
 *           Laboratoire d'Informatique de Grenoble, Team VASCO
 * %-
 * Copyright (C) 2017 - 2019 University of Grenoble Alpes
 * %-
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package partrap.util;

import java.util.Hashtable;
import java.util.List;

public class Scope_display {

	public StringBuilder display(String name, String occ,List<Scopereturns> limitlists) {
		StringBuilder adisplay= new StringBuilder();
		int t=1;
		String named= name;
		if (occ!=null) {
			named = named+" "+occ;
		}
		adisplay.append("Evaluated Scope : \""+ named + "\" :");
		//System.out.println("Evaluated Scope : \""+ named + "\" :");
		if (limitlists.isEmpty() || limitlists == null) {// an empty list of scopes: every property is true 
			//System.out.println("No found scope");
			adisplay.append("No scope is found");
		}
		for (Scopereturns scope: limitlists) {
			if(scope.from!=-1) {
				if (scope.environment!=null) {
					adisplay.append(t+" : from "+ scope.from+ " to " + scope.to +", environment size : " + scope.environment.size()+ ", environment list : "+ scope.environment.toString()+"\n");
					//System.out.println(t+" : from "+ scope.from+ " to " + scope.to +", environment size : " + scope.environment.size()+ ", environment list : "+ scope.environment.toString()+"\n");
				}
				else {
					adisplay.append(t+" : from "+ scope.from+ " to " + scope.to +", null environment \n");
					//System.out.println(t+" : from "+ scope.from+ " to " + scope.to +", null environment \n");	
				}
				}
				else {
					if (scope.environment!=null) {
						adisplay.append(t+" : empty scope with  " +" environment size : " + scope.environment.size()+ ", environment list : "+ scope.environment.toString()+"\n");
						//System.out.println(t+" : from "+ scope.from+ " to " + scope.to +", environment size : " + scope.environment.size()+ ", environment list : "+ scope.environment.toString()+"\n");
					}
					else {
						adisplay.append(t+" : empty scope with  " +", null environment \n");
						//System.out.println(t+" : from "+ scope.from+ " to " + scope.to +", null environment \n");	
					}
					
				}
				t++;
		}
		return adisplay;

	}
	@SuppressWarnings("rawtypes")
	public void display(Integer from, Integer to, List<Hashtable> tracelist,Hashtable<String,Hashtable> myenv) {
		Integer d=from;
		Integer f=to;
		boolean null_scope=false;
		if (!d.equals(-1)){
			if (f.equals(-1)){
				f=tracelist.size()-1;
			}
		}
		else if(f.equals(-1)){
			null_scope=true;
		}
		if(null_scope) {
			System.out.println("Evaluating the pattern on an empty scope:");
		}
		else {
			System.out.println("Evaluating the pattern on the scope from : "+d+" to : "+f+ "::");
			System.out.println("The environment is : "+myenv);
		}
	}
	public StringBuilder display(IScope iscope, String occ,List<Scopereturns> limitlists,List<Scopereturns> newlimitlists) {
		StringBuilder adisplay= new StringBuilder();
		int t=1;
		String named= iscope.Type;
		if (occ!=null) {
			named = named+" "+occ;
		}
		if (iscope.Type.equals("after")) {
			String duration= "";
			String condition= "";
			if(iscope.duration!=null) {
				duration ="within "+iscope.iduration.value+iscope.iduration.unit.name();
			}
			if(!iscope.event.condition.equals("")) {
				condition ="where "+iscope.event.condition+" ";
			}
			adisplay.append("Evaluated Scope : "+duration+" \""+ named + "\" "+ iscope.event.id+" "+iscope.event.var+condition+" :");
			adisplay.append("Evaluated Scope : ");
		}
		if (limitlists.isEmpty() || limitlists == null) {// an empty list of scopes: every property is true 
			//System.out.println("No found scope");
			adisplay.append("No scope is found");
		}
		for (Scopereturns scope: limitlists) {
			if(scope.from!=-1) {
				if (scope.environment!=null) {
					adisplay.append(t+" : from "+ scope.from+ " to " + scope.to +", environment size : " + scope.environment.size()+ ", environment list : "+ scope.environment.toString()+"\n");
					//System.out.println(t+" : from "+ scope.from+ " to " + scope.to +", environment size : " + scope.environment.size()+ ", environment list : "+ scope.environment.toString()+"\n");
				}
				else {
					adisplay.append(t+" : from "+ scope.from+ " to " + scope.to +", null environment \n");
					//System.out.println(t+" : from "+ scope.from+ " to " + scope.to +", null environment \n");	
				}
				}
				else {
					if (scope.environment!=null) {
						adisplay.append(t+" : empty scope with  " +" environment size : " + scope.environment.size()+ ", environment list : "+ scope.environment.toString()+"\n");
						//System.out.println(t+" : from "+ scope.from+ " to " + scope.to +", environment size : " + scope.environment.size()+ ", environment list : "+ scope.environment.toString()+"\n");
					}
					else {
						adisplay.append(t+" : empty scope with  " +", null environment \n");
						//System.out.println(t+" : from "+ scope.from+ " to " + scope.to +", null environment \n");	
					}
					
				}
				t++;
		}
		return adisplay;

	}

}
