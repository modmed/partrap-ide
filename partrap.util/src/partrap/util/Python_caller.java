/*-
 * #%L
 * This file is part of Partrap IDE
 * Please visit http://vasco.imag.fr/tools/partrap/ for further information
 * 
 * Authors : Yoann Blein , Ansem Ben Cheikh 
 *           Laboratoire d'Informatique de Grenoble, Team VASCO
 * %-
 * Copyright (C) 2017 - 2019 University of Grenoble Alpes
 * %-
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package partrap.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.script.ScriptException;

public class Python_caller {

	
		public static void main(String[] args) throws IOException, ScriptException {
			/*StringWriter writer = new StringWriter(); //ouput will be stored here

		    ScriptEngineManager manager = new ScriptEngineManager(null);
		    ScriptContext context = new SimpleScriptContext();
		    
		    context.setWriter(writer); //configures output redirection
		    ScriptEngine engine = manager.getEngineByName("python");
		    System.out.println("here "+engine); 
		    FileReader xx= new FileReader("C:\\Users\\vasco\\Documents\\projects\\newyear.py");
		    engine.eval(xx, context);
		    System.out.println(writer.toString()); */
		// set up the command and parameter
		String pythonScriptPath = "C:\\Users\\vasco\\newyear.py";
	
		//Process p=Runtime.getRuntime().exec("python test.py"); 
		// create runtime to execute external command
		
		String[] cmd = new String[2];
		cmd[0] = "python"; // check version of installed python: python -V
		cmd[1] = pythonScriptPath;
		 
		// create runtime to execute external command
		Runtime rt = Runtime.getRuntime();
		Process pr = rt.exec(cmd);
		
		 
		// retrieve output from python script
		BufferedReader bfr = new BufferedReader(new InputStreamReader(pr.getInputStream()));
		 
		String line = "";
		while((line = bfr.readLine()) != null) {
		// display each output line form python script
		System.out.println(line);
		}
		System.out.println(line+ " here I am");
		}

	

}
