/*-
 * #%L
 * This file is part of Partrap IDE
 * Please visit http://vasco.imag.fr/tools/partrap/ for further information
 * 
 * Authors : Yoann Blein , Ansem Ben Cheikh 
 *           Laboratoire d'Informatique de Grenoble, Team VASCO
 * %-
 * Copyright (C) 2017 - 2019 University of Grenoble Alpes
 * %-
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package partrap.util;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import org.json.simple.JSONObject;

import jep.Jep;
import jep.JepException;

public class PyExpression_interpreter {
	
	public Jep jep = null;
	public PyExpression_interpreter(Jep myjep) {
		jep=myjep;
	}
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public boolean main(String expr,Hashtable<String, Hashtable> myenv,Hashtable<String,?> variables) {
			boolean result = false;   
			JSONObject json = new JSONObject();
			//System.out.println("here : "+myenv);
		    json.putAll( myenv);
		    String py_expr= expr.replace("$", "");
		    
		    try{
		    	//System.out.println("my path : "+mypath);
		    	
		    	jep.eval("from Interpreter_py import *");
		    	jep.set("arg", py_expr);
		    	jep.set("arg2", json.toString());
		    	jep.eval("dic = Interpreter(arg2)");
		    	jep.eval("globals().update(dic)");
		    	for (String id: variables.keySet()) {
		    		jep.set("xx", variables.get(id));
		    		jep.eval(id+" = xx");
		    	}
		    	jep.eval("x=eval(arg)");
		    	result = ( Boolean) jep.getValue("x");
		    	//System.out.println("my result : "+result);
		    			    	
	        } catch (JepException e) {
	            e.printStackTrace();
	        }
		return result;

	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List main_list(String expr,Hashtable<String, Hashtable> myenv) throws Exception  {
		List result = new ArrayList<>();   
		ArrayList result1= null;
		
		JSONObject json = new JSONObject();
		String py_expr= expr.replace("$", "");
		if(myenv!=null) {
	    
	    json.putAll( myenv);
	    }
		try
        {
	    	//prop3.main(jep);
	    	//System.out.println("my path : "+mypath);
	    	jep.eval("from Interpreter_py import *");
	    	jep.set("arg", py_expr);
	    	jep.set("arg2", json.toString());
	    	jep.eval("dic = Interpreter(arg2)");
	    	jep.eval("globals().update(dic)");
	    	
	    	jep.eval("x=eval(arg)");
	    	result1 = ( ArrayList) jep.getValue("x");
	    	//System.out.println("my result : "+result1+ " ; "+ result1.size());
	    	result= result1;
	    	// using getValue(String) to invoke methods
	    	//Object result2 = jep.getValue("somePyModule.foo2()");

	    	// using invoke to invoke methods
	    	// jep.eval("foo3 = somePyModule.foo3");
	    	//Object result3 = jep.invoke("foo3", obj);

	    	// using runScript
	    	//jep.runScript("path/To/Script");
        } catch (JepException e) {
            e.printStackTrace();
        }
	return result;

}

}
