/*-
 * #%L
 * This file is part of Partrap IDE
 * Please visit http://vasco.imag.fr/tools/partrap/ for further information
 * 
 * Authors : Yoann Blein , Ansem Ben Cheikh 
 *           Laboratoire d'Informatique de Grenoble, Team VASCO
 * %-
 * Copyright (C) 2017 - 2019 University of Grenoble Alpes
 * %-
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package partrap.util;



	import java.io.FileNotFoundException;
	import java.io.FileReader;
import java.io.PrintStream;
import java.util.ArrayList;
	import java.util.Hashtable;
	import java.util.List;

import org.json.simple.JSONObject;

import jep.Jep;
import partrap.util.Convertjsonlist;
	import partrap.util.IDuration;
	import partrap.util.IEvent;
	import partrap.util.IPattern;
	import partrap.util.IScope;
	import partrap.util.Occurrence;
	import partrap.util.Scopereturns;

	@SuppressWarnings("all")
	public class prop3 {
	  public static boolean value;
	  
	  public static boolean state;
	  
	  public static void main(Jep jep) throws Exception {
	    //jep.eval("import sy");
	    jep.eval("import Interpreter_pyy");
	    Hashtable<String, Hashtable> myenv = new Hashtable<String, Hashtable>();
	    Hashtable event= new Hashtable<>();
	    event= new Hashtable<>();
	    event.put("id", "Enter");
	    event.put("time", 123556.9);
	    event.put("state", "LoadP");
	    myenv.put("bb", event);
	    String expr="bb.state=='LoadP'";
	    JSONObject json = new JSONObject();
		String py_expr= expr.replace("$", "");
		if(myenv!=null) {
	    
	    json.putAll( myenv);
	    }
		System.out.println("1&&&&&&&");
		jep.set("arg", py_expr);
		System.out.println("22222222222222");
    	jep.set("arg2", json.toString());
    	System.out.println("33333333333");
    	//System.setOut(new PrintStream(new NullOutputStream())); 
    	jep.eval("x = Interpreter_pyy.Interpreter(arg2, arg)");
    	System.out.println("444444444444");
    	Object result1 =jep.getValue("x");
    	System.out.println("my result : "+result1);
	  
	    
	  }
	


}
