/*-
 * #%L
 * This file is part of Partrap IDE
 * Please visit http://vasco.imag.fr/tools/partrap/ for further information
 * 
 * Authors : Yoann Blein , Ansem Ben Cheikh 
 *           Laboratoire d'Informatique de Grenoble, Team VASCO
 * %-
 * Copyright (C) 2017 - 2019 University of Grenoble Alpes
 * %-
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package partrap.util;

import java.util.Hashtable;
import java.util.List;

public class Prop_Result extends Composite_Result {
	   public Prop_Result(Scopereturns scoper, IScope iscopex, boolean verdictx) {
		super(scoper, iscopex, verdictx);
		// TODO Auto-generated constructor stub
	}
	// public List<Result> children = new ArrayList<>();
	public String name;
	public Hashtable<String,Object> variables=new Hashtable<>();
	public void update(Hashtable<String,Object> ivariables) {
		variables=ivariables;
	}
	public void update_vars(String id,Object var) {
		variables.put(id, var);
	}
    

	public void add(Result c) {
        children.add(c);
    }
	public void addAll(List<Composite_Result> cl) {
		for (Composite_Result c: cl) {
			children.add(c);
		}
    }
	@Override
    public StringBuilder  traverse(StringBuilder initial,int i,boolean view) {
    	StringBuilder resulttext= initial;
    	if(parent==null) {
    		resulttext.append("The property \'"+name+"\' is executed on trace \'"+trace_path+"\'\n");
    		resulttext.append("The property \'"+name+"\' is "+verdict+" ");
    		resulttext.append(Short_Result_Explanation.sexplain(this.scope,this.iScope).toString());
    		for (Result result : children) {
        		result.traverse(resulttext, i,view);
        	}
    	}
    	else {
    		if(view || !verdict) {
    			
    			if (name!=null) {
    				for(int j=1;j<=i; j++) {
        				initial.append("--");
    					}
        			i++;
    				resulttext.append("The property \'"+name+"\' is "+verdict+" ");
    			}
    	
    			if(!variables.isEmpty()) {
    				for(int j=1;j<=i; j++) {
        				initial.append("--");
    					}
        			i++;
    				for(String id: variables.keySet()) {
    					resulttext.append("for "+id+"="+variables.get(id)+", ");
    				}
    			}
    			resulttext.append("\n ");
    			for (Result result : children) {
    	    		result.traverse(resulttext, i,view);
    	    	}
    		}
    	}
    	return resulttext;
    }

   
}
