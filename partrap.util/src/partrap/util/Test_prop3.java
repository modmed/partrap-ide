/*-
 * #%L
 * This file is part of Partrap IDE
 * Please visit http://vasco.imag.fr/tools/partrap/ for further information
 * 
 * Authors : Yoann Blein , Ansem Ben Cheikh 
 *           Laboratoire d'Informatique de Grenoble, Team VASCO
 * %-
 * Copyright (C) 2017 - 2019 University of Grenoble Alpes
 * %-
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package partrap.util;


import jep.Jep;
import jep.JepConfig;
import jep.JepException;

public class Test_prop3 {

	public static void main(String[] args) {
		long startTime = System.nanoTime(); 
		String mypath = "C:\\Users\\vasco\\eclipse-workspace\\thisDSL\\org.xtext.example.mydsl\\org.xtext.example.mydsl\\python-utils";
		 JepConfig config = new JepConfig().setIncludePath(mypath).setInteractive(false).setRedirectOutputStreams(false);
		for(int i=0;i<2000; i++) {
			try(Jep jep = new Jep(config))
	        {
        	System.out.println(i);
           // prop3.main(jep);
        	jep.eval("import Interpreter_pyx");
        	jep.eval("x = Interpreter_pyx.calcul()");
        	
        	Object result1 =jep.getValue("x");
        	System.out.println("my result : "+result1);
        	//Object x=jep.getValue("x");
        	//System.out.println(x);
        	
       
        } catch (JepException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		}
		long endTime = System.nanoTime();
		System.out.println("Took "+(endTime - startTime)/1000000 + " ms"); 

	}

}
