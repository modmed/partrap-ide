/*-
 * #%L
 * This file is part of Partrap IDE
 * Please visit http://vasco.imag.fr/tools/partrap/ for further information
 * 
 * Authors : Yoann Blein , Ansem Ben Cheikh 
 *           Laboratoire d'Informatique de Grenoble, Team VASCO
 * %-
 * Copyright (C) 2017 - 2019 University of Grenoble Alpes
 * %-
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package partrap.util;

import java.io.File;
import java.util.HashMap;

public class Construct_tree {
	public TreeNode<String> listf(String directoryName, TreeNode<String> files) {
	    File directory = new File(directoryName);

	    // get all the files from a directory
	    File[] fList = directory.listFiles();
	    for (File file : fList) {
	        if (file.isFile()) {
	        	if(file.getName().toLowerCase().endsWith("json")){
	        		String filepath= file.getAbsolutePath();
					String filepath2=filepath.replace("\\","/");
					HashMap<String, Boolean> leaf= new HashMap <String, Boolean>();
					leaf.put(filepath2, false);
					//System.out.println("file json : "+filepath2);
					files.addleaf(leaf);
	            }
	        } else {
	        	if (file.isDirectory()) {
	        		String filepath= file.getName();
	        		TreeNode<String> newchild= new  TreeNode<String>(filepath);
	        		files.addChild(newchild);
	        		listf(file.getAbsolutePath(), newchild);
	        	}
	        }
	    }
	    return files;
	}

}
