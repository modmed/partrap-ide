/*-
 * #%L
 * This file is part of Partrap IDE
 * Please visit http://vasco.imag.fr/tools/partrap/ for further information
 * 
 * Authors : Yoann Blein , Ansem Ben Cheikh 
 *           Laboratoire d'Informatique de Grenoble, Team VASCO
 * %-
 * Copyright (C) 2017 - 2019 University of Grenoble Alpes
 * %-
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package partrap.util;

import java.util.logging.ConsoleHandler;
import java.util.logging.ErrorManager;
import java.util.logging.Level;
import java.util.logging.LogRecord;

public class MyConsoleHandler extends ConsoleHandler {
	public MyConsoleHandler() {
		super();
		//this.setOutputStream(System.out);
		//this.setLevel(Level.CONFIG);
		/// a changer eerr
	}
	/*@Override
	public void publish(LogRecord record)
	{
		if (getFormatter() == null)
		{
        setFormatter(new SimpleFormatter());
		}

    try {
        String message = getFormatter().format(record);
        if (record.getLevel().intValue() >= Level.WARNING.intValue())
        {
            System.err.write(message.getBytes());                       
        }
        else
        {
        	 if (record.getLevel().intValue() < Level.WARNING.intValue() &&record.getLevel().intValue() >= Level.CONFIG.intValue() ) {
            System.out.write(message.getBytes());
        	 }
        }
    } catch (Exception exception) {
        reportError(null, exception, ErrorManager.FORMAT_FAILURE);
    }

	}*/
	 @Override
	    public void publish(LogRecord record)
	    {
	        try
	        {
	            if(record.getLevel().intValue() >= this.getLevel().intValue())
	            {
	                String message = getFormatter().format(record);
	                if (record.getLevel().intValue() >= Level.WARNING.intValue())
	                {
	                    System.err.write(message.getBytes());                       
	                }
	                else
	                {
	                    System.out.write(message.getBytes());
	                }
	            }
	        } catch (Exception exception)
	        {
	            reportError(null, exception, ErrorManager.FORMAT_FAILURE);
	            return;
	        }
	    }
}
