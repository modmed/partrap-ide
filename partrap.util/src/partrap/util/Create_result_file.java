/*-
 * #%L
 * This file is part of Partrap IDE
 * Please visit http://vasco.imag.fr/tools/partrap/ for further information
 * 
 * Authors : Yoann Blein , Ansem Ben Cheikh 
 *           Laboratoire d'Informatique de Grenoble, Team VASCO
 * %-
 * Copyright (C) 2017 - 2019 University of Grenoble Alpes
 * %-
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package partrap.util;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;

public class Create_result_file {
	public void create(Hashtable<String, Boolean> results, String path) {
		
		try {
			FileWriter fw= new FileWriter(path);
			fw.write("");
			BufferedWriter out = new BufferedWriter(fw);
			out.flush();
			for (String key:results.keySet()) {
				out.write("{\'"+key+"\' : "+results.get(key)+"}");
				out.newLine();
			}
			//out.write(results.toString());
			out.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
	}
	public void create_link_file(StringBuilder results, String path) {
		try {
			FileWriter fw= new FileWriter(path);
			fw.write("");
			BufferedWriter out = new BufferedWriter(fw);
			out.flush();
			out.write(results.toString());
			
			out.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
	}
	public void creates(Hashtable<String,Hashtable<String, Boolean>> results, String path) {
		//path= path.substring(0, path.length()-5);
		//path = path +"\\results_file.txt";
		try {
			FileWriter fw= new FileWriter(path);
			fw.write("");
			BufferedWriter out = new BufferedWriter(fw);
			out.flush();
			for (String key:results.keySet()) {
				Hashtable<String, Boolean> result= results.get(key);
				out.write("**********\'"+key+"\' : ************* ");
				out.newLine();
				for (String akey:result.keySet()) {
					out.write("{\'"+akey+"\' : "+result.get(akey)+"}");
					out.newLine();
				}
			}
			//out.write(results.toString());
			out.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
	}
	public void create_from_tree(Hashtable<String,TreeNode<String>> results, String path) {
		try {
			FileWriter fw= new FileWriter(path);
			fw.write("");
			BufferedWriter out = new BufferedWriter(fw);
			out.flush();
			for (String key:results.keySet()) {
				TreeNode<String> result= results.get(key);
				out.write("**********\'"+key+"\' : ************* ");
				out.newLine();
				out.write("The main Trace Directory \'"+result.getdata()+"\' : ");
				out.newLine();
				List<Integer> summary = new ArrayList<>();
				summary.add(0);
				summary.add(0);
				List<Integer> nsummary= write_summary_from_tree(result,summary);
				Integer true_results = nsummary.get(0);
				Integer false_results = nsummary.get(1);
				out.write(true_results+" traces are true and "+false_results+" traces are false ");
				out.newLine();
				write_from_tree(result, out);
			}
			//out.write(results.toString());
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
        
	}
	public StringBuilder create_from_tree2(Hashtable<String,TreeNode<String>> results) {
		StringBuilder sbBuilder = new StringBuilder();
		try {
			for (String key:results.keySet()) {
				TreeNode<String> result= results.get(key);
				sbBuilder.append("<p>The main Trace Directory \'"+result.getdata()+"\' :</p> ");
				List<Integer> summary = new ArrayList<>();
				summary.add(0);
				summary.add(0);
				List<Integer> nsummary= write_summary_from_tree(result,summary);
				Integer true_results = nsummary.get(0);
				Integer false_results = nsummary.get(1);
				sbBuilder.append("<p>"+true_results+" traces are true and "+false_results+" traces are false "+"</p>");
			}
		
		} catch (IOException e) {
			e.printStackTrace();
		}
		return sbBuilder;
        
	}
	
	public float create_percentage(Hashtable<String,TreeNode<String>> results) {
		float percent=0;
		try {
			for (String key:results.keySet()) {
				TreeNode<String> result= results.get(key);
				List<Integer> summary = new ArrayList<>();
				summary.add(0);
				summary.add(0);
				List<Integer> nsummary= write_summary_from_tree(result,summary);
				Integer true_results = nsummary.get(0);
				Integer false_results = nsummary.get(1);
				percent= (true_results*100)/(true_results+false_results);
			}
		
		} catch (IOException e) {
			e.printStackTrace();
		}
		return percent;
        
	}
	public StringBuilder create_trace_table(TreeNode<String> trace_tree,StringBuilder sb) {
		StringBuilder sbBuilder = sb;
		
			List<HashMap<String, Boolean>> leaves= trace_tree.getleaves(); 
			List<TreeNode<String>> children= trace_tree.getChildren();
			for (HashMap<String, Boolean>  leaf:leaves) {
				String trace= (String) leaf.keySet().toArray()[0];
				sbBuilder.append("<tr><td><a href=\""+trace+"\" style=\"visited.color:cornflowerblue; color: cornflowerblue;\">"+trace+"</a></td></tr>" );
			}
			for (TreeNode<String> child:children) {
				sbBuilder= create_trace_table(child, sbBuilder);
			}
			
		
		return sbBuilder;
        
	}
	
	public void write_from_tree(TreeNode<String> tree,BufferedWriter out ) throws IOException {
		List<HashMap<String, Boolean>> leaves = tree.getleaves();
		List<TreeNode<String>>  children= tree.getChildren();
		for(HashMap<String, Boolean> leaf : leaves) {
			String stracefile =(String) leaf.keySet().toArray()[0];
			Boolean result = leaf.get(stracefile);
			out.write("{\'"+stracefile+"\' : "+result+"}");
			out.newLine();
		}
		for (TreeNode<String> child : children) {
			out.write("Under directory : \'"+child.getdata()+"\' ***************");
			out.newLine();
			write_from_tree(child, out);
		}
	}

	public List<Integer> write_summary_from_tree(TreeNode<String> tree,List<Integer> num_results) throws IOException {
		List<HashMap<String, Boolean>> leaves = tree.getleaves();
		List<TreeNode<String>>  children= tree.getChildren();
		Integer true_results = num_results.get(0);
		Integer false_results = num_results.get(1);
		List<Integer> numbered_results= new ArrayList<>();
		for(HashMap<String, Boolean> leaf : leaves) {
			String stracefile =(String) leaf.keySet().toArray()[0];
			Boolean result = leaf.get(stracefile);
			if(result.equals(true)) {
				true_results++;
			}
			if(result.equals(false)) {
				false_results++;
			}
		
		}
		numbered_results.add(true_results);
		numbered_results.add(false_results);
		for (TreeNode<String> child : children) {
			
			numbered_results=write_summary_from_tree(child,numbered_results);
		}
		
		return numbered_results;
	}
	public StringBuilder create_traces_column(List<HashMap<String, Boolean>> results, StringBuilder body_html) {
		StringBuilder result_html= body_html;
		result_html.append("<table style=\"float: left\" border = \"1\">");
		result_html.append("<tr><td>Property:</td></tr>" );
		for (HashMap<String, Boolean> leaf:results) {
			String key = (String) leaf.keySet().toArray()[0];
			result_html.append("<tr><td>"+key+"</td></tr>");
		
		}
		result_html.append("</table>");
		return result_html;
	}

public StringBuilder create_result_column(Hashtable<String, Boolean> results, StringBuilder body_html) {
	StringBuilder result_html= new  StringBuilder();
	result_html.append("<table style=\"float: left\" border = \"1\">");
	result_html.append("<tr><td>Property:</td></tr>" );
	for (String key:results.keySet()) {
		result_html.append("<tr><td>"+key+"</td></tr>");
	
	}
	result_html.append("</table>");
	return result_html;
}
}
