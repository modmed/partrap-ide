/*-
 * #%L
 * This file is part of Partrap IDE
 * Please visit http://vasco.imag.fr/tools/partrap/ for further information
 * 
 * Authors : Yoann Blein , Ansem Ben Cheikh 
 *           Laboratoire d'Informatique de Grenoble, Team VASCO
 * %-
 * Copyright (C) 2017 - 2019 University of Grenoble Alpes
 * %-
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package partrap.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class IPattern {
	  public String Type;
	  
	  public IEvent event;
	  
	  public IEvent event2;
	  
	  public IDuration iduration;
	  
	  public Integer n;
	 
	  public IPattern(String type, IEvent ev1, IEvent ev2, IDuration dur, Integer nb) {
		  Type = type;
		  event = ev1;
		  event2 = ev2;
		  iduration = dur;
		  n = nb;
	  }
	  
	  @SuppressWarnings({ "rawtypes" })
		public  List<Scopereturns> evaluates( final List<Hashtable> traceList,List<Scopereturns> mylists) throws Exception {
		  /* Evaluates a pattern on a list of scopes*/
		  List<Scopereturns> returnlists= new ArrayList<Scopereturns>();
		  if (!mylists.isEmpty()) {
		  	  for (Scopereturns myscope: mylists) {
		  		List<Scopereturns> newvalue = evaluate(traceList, myscope);
		  		returnlists.addAll(newvalue);
		  		//System.out.println("evaluated Pattern : "+ Type +" : return list size => "+ returnlists.size());
		  		}
		   }
		  else {
			 //System.out.println("Your Scope is empty");
		  }
		  return returnlists;
	  }
	  @SuppressWarnings({ "rawtypes" })
		public  List<Scopereturns> evaluate( final List<Hashtable> traceList, Scopereturns scope) throws Exception {
		 /* Evaluates a pattern on a single scope*/
		 // System.out.println("***********************");
		  int d=scope.from;
		  int f=scope.to;
		  Hashtable<String, Hashtable> myenv =scope.environment;
		  Scope_display sdDisplay= new Scope_display();
		  sdDisplay.display(d,f,traceList,myenv);
		  List<Scopereturns> returnlists= new ArrayList<Scopereturns>();
		  if(Type.equals("occurrence_of")) {
			 //System.out.println("Pattern type is \"occurrence_of\" : "+"Looking for event occurrences ");
			  List<Scopereturns> occ_list = occurrences(traceList, d, f,  myenv);
			  returnlists=occ_list;
			  //System.out.println(occ_list.size()+": size :"+occ_list);
		  }
		  if(Type.equals("absence_of")) {
			 // System.out.println("Pattern type is \"absence_of\" : "+"Looking for event occurrences ");
			  List<Scopereturns> occ_list = occurrences(traceList, d, f, myenv);
			  returnlists=occ_list;
		  }
		  
		  if(Type.equals("followed_by")) {
			  //System.out.println("Pattern type is \"followed_by\" : "+"Looking for scopes \"after each\" ");
			  IScope iscope = new IScope("after", Occurrence.each, event, null, iduration, null, true);
			  List<Scopereturns> occ_arrays = iscope.after_each(traceList, scope, event);
			  //System.out.println("list size : "+ occ_arrays.size());
			  if (occ_arrays!=null) {
				  returnlists=occ_arrays;
			 
			  }
			 
		}
		  
		  if(Type.equals("preceded_by")) {
			  //System.out.println("Pattern type is \"precedes\" : "+"Looking for scopes \"before each\" ");
			  IScope iscope = new IScope("before", Occurrence.each, event, null, iduration, null, true);
			  
			  List<Scopereturns> occ_arrays = iscope.evaluate(traceList, scope);
			  if (occ_arrays!=null) {
				  returnlists=occ_arrays;
			   }
		  }
		  
		  if(Type.equals("prevents")) {
			  //System.out.println("Pattern type is \"prevents\" : "+"Looking for scopes \"after each\" ");
			  IScope iscope = new IScope("after", Occurrence.each, event, null, iduration, null, true);
			  List<Scopereturns> occ_arrays = iscope.after_each(traceList, scope,event);
			  if (occ_arrays!=null) {
				  returnlists=occ_arrays;
			   }
			 			 
		  }
		  return returnlists;
	  }
	  
	  
	  
	  @SuppressWarnings({ "rawtypes" })
		public  boolean final_verdict( final List<Hashtable> traceList,List<Scopereturns> mylists) throws Exception {
		  /* gives directly the pattern boolean value without using scope lists
		   * it is useful in the case of patterns without conditional expressions*/
			  boolean returnvalue= true;
			  if (!mylists.isEmpty()) {
			  	  for (Scopereturns myscope: mylists) {
			  		  boolean  newvalue = verdict(traceList, myscope);
			  		 //System.out.println("Pattern to evaluate : "+ Type +" : "+ " from "+ myscope.from +" to " +myscope.to+" => "+ newvalue);
			  		  returnvalue = returnvalue && newvalue;
			  	  }
			  }
			  else {
				  returnvalue=false;
				 // System.out.println("Your Scope is empty");
			  }
			  
			  return returnvalue;
	  }
	  
	  @SuppressWarnings({ "rawtypes" })
	public  boolean verdict(final List<Hashtable> traceList,Scopereturns scope) throws Exception {
		  boolean value =false;
		  int d=scope.from;
		  int f=scope.to;
		  Hashtable<String, Hashtable> myenv =scope.environment;
		  if(Type.equals("occurrence_of")) {
			  value =false;
			  //System.out.println("Pattern type is \"occurrence_of\" : "+"Looking for event occurrences ");
			  List<Scopereturns> occ_list = occurrences(traceList, d, f,  myenv);
			  //System.out.println("event occurrences in the scope from :" +d+" to :"+f+" : " +occ_list.size());
			  if (n== null) {
				 if(!occ_list.isEmpty()) {
					  value = true;
					  Occurrence_display.display(occ_list);
				  }
			  }
			  else {
				  if(n<=occ_list.size() && n>0){
					  Occurrence_display.display(occ_list);
					  value=true;
				  }
				  
			  }
			  //System.out.println(occ_list.size()+": size :"+occ_list);
		  }
		  if(Type.equals("absence_of")) {
			  //System.out.println("Pattern type is \"absence_of\" : "+"Looking for event occurrences ");
			  List<Scopereturns> occ_list = occurrences(traceList, d, f,myenv);
			  if(occ_list.isEmpty()) {
				  value = true;
			  }
		  }
		  
		  if(Type.equals("followed_by")) {
			  //System.out.println("Pattern type is \"followed_by\" : "+"Looking for scopes \"after each\" ");
			  IScope iscope = new IScope("after", Occurrence.each, event, null, iduration, null, true);
			  value = true;
			  List<Scopereturns> occ_arrays = iscope.after_each(traceList, scope, event );
			  if (occ_arrays!=null) {
				  for (Scopereturns b_list: occ_arrays) {
					  List<Scopereturns> occ_list= occurrences(traceList,b_list.from,b_list.to, b_list.environment);
					  if(occ_list.isEmpty()) {
	    					  value = false;
	    					  //System.out.println(b_list.from+"<<"+b_list.to+": size :");
					  }
	    		}
			  }
			  /*else {
				  if (traceList.get(f).get("id").equals(event.id)) {
					  value = false;
					 }
				  }*/
			 }
		  
		  if(Type.equals("preceded_by")) {
			 // System.out.println("Pattern type is \"precedes\" : "+"Looking for scopes \"before each\" ");
			  IScope iscope = new IScope("before", Occurrence.each, event, null, iduration, null, true);
			  value = true;
			  List<Scopereturns> occ_arrays = iscope.evaluate(traceList, scope);
			  if (occ_arrays!=null) {
			 
			  for (Scopereturns b_list: occ_arrays) {
	    			List<Scopereturns> occ_list= occurrences(traceList,b_list.from,b_list.to, b_list.environment);
	    			if(occ_list.isEmpty()) {
	    					  value = false;
	    			}
	    			
	    		}
			  }
			  
		  }
		  
		  if(Type.equals("prevents")) {
			  //System.out.println("Pattern type is \"prevents\" : "+"Looking for scopes \"after each\" ");
			  IScope iscope = new IScope("after", Occurrence.each, event, null, iduration, null, true);
			  value = true;
			  List<Scopereturns> occ_arrays = iscope.after_each(traceList, scope, event );
			 for (Scopereturns b_list: occ_arrays) {
				 List<Scopereturns> occ_list= occurrences(traceList,b_list.from,b_list.to, b_list.environment);
				 if(!occ_list.isEmpty()) {
	    					  value = false;
	    					
	    				}
	    		}
		  }
		
		  //System.out.println(value);
		  return value;
		  
	  }
	  
	  @SuppressWarnings({ "rawtypes", "unchecked" })
	  public  List<Scopereturns> occurrences( final List<Hashtable> traceList, Integer d1, Integer f1,final Hashtable<String,Hashtable> myenv) throws Exception {
	  	/* returns the occurrences of the event in a certain scope
	  	 * A ScopeReturns instance is created for each event occurrence*/
		  List<Scopereturns> occ_list = new ArrayList<Scopereturns>();
		  List<Integer> index_list= new ArrayList<>();
	  		IEvent myevent;
	  		if (Type.equals("occurrence_of")||Type.equals("absence_of"))
	  			{myevent= event;}
	  		else{
	  			myevent= event2;
	  			}
	  	 if(d1!=-1) { 
	  	    int i = d1;
	  	    int j;
	  	    if (f1==-1) {
	  	    	j= traceList.size()-1;
	  	    }
	  	    else {j = f1;}
	  		while(i<=j){
	  			Hashtable hevent = traceList.get(i);
	  			if (hevent.get("id").equals(myevent.id)){
	  				hevent.put("trace_occ_index", i+1);
	  				index_list.add(i);
	  				Hashtable<String,Hashtable> env = calculate_env(myevent.var, hevent, myenv);
	  				 Scopereturns occ_bind = new Scopereturns(d1, f1,env,hevent,null);
					occ_list.add(occ_bind);
					
	  			}
	  			i++;
	  		}
	  	}
	  	//System.out.println("event \'"+ myevent.id+ "\' " +"occurrences from: " +d1+" to : "+f1+" is " +occ_list.size()+ " -> "+index_list);
	  	return occ_list;
	  }
	  
	  @SuppressWarnings({ "rawtypes", "unchecked" })
	public  List<Hashtable> occurr_list( final List<Hashtable> traceList, Integer d1, Integer f1,final Hashtable<String,Hashtable> myenv) throws Exception {
		  	/* returns the occurrences of the event in a certain scope
		  	 * A ScopeReturns instance is created for each event occurrence*/
			  List<Hashtable> occ_list = new ArrayList<Hashtable>();
			  List<Integer> index_list= new ArrayList<>();
		  		IEvent myevent;
		  		if (Type.equals("occurrence_of")||Type.equals("absence_of"))
		  			{myevent= event;}
		  		else{
		  			myevent= event2;
		  			}
		  	 if(d1!=-1) { 
		  	    int i = d1;
		  	    int j;
		  	    if (f1==-1) {
		  	    	j= traceList.size()-1;
		  	    }
		  	    else {j = f1;}
		  		while(i<=j){
		  			Hashtable hevent = traceList.get(i);
		  			if (hevent.get("id").equals(myevent.id)){
		  				hevent.put("trace_occ_index", i+1);
		  				index_list.add(i);
		  				occ_list.add(hevent);
						
		  			}
		  			i++;
		  		}
		  	}
		  	//System.out.println("event \'"+ myevent.id+ "\' " +"occurrences from: " +d1+" to : "+f1+" is " +occ_list.size()+ " -> "+index_list);
		  	return occ_list;
		  }
	  
	  @SuppressWarnings({ "unchecked", "rawtypes" })
	public  Map<Hashtable,Scopereturns> final_occurrences( final List<Hashtable> traceList, Integer d1, Integer f1,final Hashtable<String,Hashtable> myenv) throws Exception {
		  	/* returns the occurrences of the event in a certain scope*/
		  Map<Hashtable, Scopereturns> occc_list= Collections.synchronizedMap( new LinkedHashMap<Hashtable,Scopereturns>());
			  List<Scopereturns> occ_list = new ArrayList<Scopereturns>();
			  List<Integer> index_list= new ArrayList<>();
		  		IEvent myevent;
		  		if (Type.equals("occurrence_of")||Type.equals("absence_of"))
		  			{myevent= event;}
		  		else{
		  			myevent= event2;
		  			}
		  	
		  	
		  	 if(d1!=-1) { 
		  	    int i = d1;
		  	    int j;
		  	    if (f1==-1) {
		  	    	j= traceList.size()-1;
		  	    }
		  	    else {j = f1;}
		  		while(i<=j){
		  			Hashtable hevent = traceList.get(i);
		  			if (hevent.get("id").equals(myevent.id)){
		  				hevent.put("trace_occ_index", i+1);
		  				index_list.add(i);
		  				Hashtable<String,Hashtable> env = calculate_env(myevent.var, hevent, myenv);
		  				Scopereturns occ_bind = new Scopereturns(d1, f1,env,hevent,null);
		  				occc_list.put(hevent, occ_bind);
		  				occ_list.add(occ_bind);
					}
		  			i++;
		  		}
		  	}
		  	//System.out.println("event '"+ myevent.id+ "' " +"occurrences from: " +d1+" to : "+f1+" is " +occ_list.size()+ " -> "+index_list);
		  	return occc_list;
		  }
	  
	  public  boolean cond_verdict(List<Scopereturns> occ_lists) {
		  /* Using the occurrences of the events to calculate the pattern boolean values */
		  boolean value =false;
		  if(Type.equals("occurrence_of")) {
			  if(n>=1) {
				  if(occ_lists.size()>=n){
					  value =true;
					 // Occurrence_display.display(occ_lists);
				  }
			  }
			  if(n==0) {
				  if (occ_lists.isEmpty()) {
						 value= true;
						// Occurrence_display.display(occ_lists);
					 }
			  }
		  }
		  if(Type.equals("absence_of")) {
			  if (occ_lists.isEmpty()) {
					 value= true;
					 //Occurrence_display.display(occ_lists);
				 }
			  
			  
		  }
		  if(Type.equals("followed_by")) {
			  if (!occ_lists.isEmpty()) {
					 value= true;
					// Occurrence_display.display(occ_lists);
				 }
	  
		  }
		  if(Type.equals("preceded_by")) {
			  if (!occ_lists.isEmpty()) {
					 value= true;
					 //Occurrence_display.display(occ_lists);
				 }
			  
		  }
		  if(Type.equals("prevents")) {
			  //Occurrence_display.display(occ_lists);
			  if (occ_lists.isEmpty()) {
					 value= true;
				 }
			  
		  }
		  //System.out.println("verdict for this scope : " +value);
		return value;
		  
	  }
	  
public  boolean null_verdict() {
	/*  what value to return when the pattern 's internal scope is null*/
	boolean verdict = false;
	 if(Type.equals("prevents")) {
		 verdict= true;
	 }
	 if(Type.equals("followed")) {
		 verdict=false;
	 }
	 if(Type.equals("preceded_by")) {
		 verdict=false;
	 }
	 if(Type.equals("occurrence_of")) {
		 verdict=false;
	 }
	 if(Type.equals("absence_of")) {
		 verdict=true;
	 }
	//System.out.println("verdict for this scope : " +verdict);
	return verdict;
}

@SuppressWarnings("rawtypes")
private static Hashtable<String, Hashtable> calculate_env(String event_var, Hashtable hevent, Hashtable<String, Hashtable> myenv) throws Exception{
	  Hashtable<String, Hashtable> env = new Hashtable<String, Hashtable>();
	  String name;
	    if (event_var == null) {
	    	 if (myenv !=null) {
			    	env.putAll(myenv);
			    }
	    }
	    else {
	    	name = event_var;
	    	if (myenv ==null) {
	    		env.put(name, hevent);
	    	}
	    	else {
	    		if(myenv.containsKey(name)) {
	    			System.out.println("Error in event declaration this event name already exists : "+ name);
	    			throw new Exception("event name : "+ name);
	    		}
	    		else {
	    		env.putAll(myenv);
	    		env.put(name, hevent);
	    		}
	    	}
	    }
	    return env;
 }

}
