/*-
 * #%L
 * This file is part of Partrap IDE
 * Please visit http://vasco.imag.fr/tools/partrap/ for further information
 * 
 * Authors : Yoann Blein , Ansem Ben Cheikh 
 *           Laboratoire d'Informatique de Grenoble, Team VASCO
 * %-
 * Copyright (C) 2017 - 2019 University of Grenoble Alpes
 * %-
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package partrap.util;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;


import org.json.simple.*;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Convertjsonlist {

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public  static List<Hashtable> convert(FileReader tracefile) {
		//FileReader tracefile = new FileReader("C:\\Users\\vasco\\Documents\\traces\\bo1.json");
		List<Hashtable> listOfEvents = new ArrayList<Hashtable>();
		JSONParser parser = new JSONParser();
		
        //JSONArray listOfEvents = new JSONArray();

        try { 
        	JSONArray a = (JSONArray) parser.parse(tracefile);
        	for (Object o : a)
        	  {
        	    JSONObject traceEvent = (JSONObject) o;
        	    Hashtable event = new Hashtable();
        	 
        	   Iterable keys = traceEvent.keySet();
        	

        	    for (Object key : keys)
        	    {
        	        Object value = traceEvent.get(key);
        	        event.put(key.toString(), value);
        	      // System.out.println(key.toString()+":"+event.get(key)+":");
        	        // Determine type of value and do something with it...
        	    }
        	    listOfEvents.add(event);
        	    //System.out.println("size :"+listOfEvents.size());
        	   
        	    
        	  }	  //System.out.println("work done");
        	
        }catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
		return listOfEvents;
		// TODO Auto-generated method stub

	}

}
