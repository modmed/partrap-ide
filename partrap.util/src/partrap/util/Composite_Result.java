/*-
 * #%L
 * This file is part of Partrap IDE
 * Please visit http://vasco.imag.fr/tools/partrap/ for further information
 * 
 * Authors : Yoann Blein , Ansem Ben Cheikh 
 *           Laboratoire d'Informatique de Grenoble, Team VASCO
 * %-
 * Copyright (C) 2017 - 2019 University of Grenoble Alpes
 * %-
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package partrap.util;

import java.util.List;

public class Composite_Result extends Composite_Issue {
	   // public List<Result> children = new ArrayList<>();
	    public Composite_Result(Scopereturns scoper,IScope iscopex,boolean verdictx) {
	        scope = scoper;
	        iScope=iscopex;
	        verdict= verdictx;
	    }

		public void add(Result c) {
	        children.add(c);
	    }
		public void addAll(List<Composite_Result> cl) {
			for (Composite_Result c: cl) {
				children.add(c);
			}
	    }

	    public StringBuilder  traverse(StringBuilder initial,int i,boolean view) {
	    	StringBuilder resulttext= initial;
	    	//List<Result> children;
	    	
			//resulttext.append("class: "+this.getClass()+" "+verdict+"\n");
	    	if(view || !verdict) {
	    		if(iScope!=null) {
	    			for(int j=1;j<=i; j++) {
	    				initial.append("--");
					}
	    			i++;
	    			resulttext.append(Short_Result_Explanation.sexplain(this.scope,this.iScope).toString());
	    		}
	    	for (Result result : children) {
	    		result.traverse(resulttext, i,view);
	    	}
	    	}
	    	
	    return resulttext;
	    }
	   
}


