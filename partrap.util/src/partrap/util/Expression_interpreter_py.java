/*-
 * #%L
 * This file is part of Partrap IDE
 * Please visit http://vasco.imag.fr/tools/partrap/ for further information
 * 
 * Authors : Yoann Blein , Ansem Ben Cheikh 
 *           Laboratoire d'Informatique de Grenoble, Team VASCO
 * %-
 * Copyright (C) 2017 - 2019 University of Grenoble Alpes
 * %-
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package partrap.util;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import org.json.simple.JSONObject;

import jep.Jep;
import jep.JepConfig;
import jep.JepException;

public class Expression_interpreter_py {
	
	public String path = null;
	public Expression_interpreter_py(String mypath) {
		path=mypath;
	}
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public boolean main(String expr,Hashtable<String, Hashtable> myenv, String[] package_py) {
			boolean result = false;   
			JSONObject json = new JSONObject();
		    json.putAll( myenv);
		    String mypath =path;
		    String py_expr= expr.replace("$", "");
		    
		    try(Jep jep = new Jep(false))
	        {		    	
		    	jep.eval("import sys,os");
		    	//jep.eval("pathname = os.path.dirname(sys.argv[0])");
		    	if (mypath==null) {
		    		jep.eval("print (\'full path =\', os.path.abspath(\'\'))");
		    		jep.eval("ipath = os.path.abspath(\'\')");
		    		mypath = (String) jep.getValue("ipath")+"\\python-utils";
		    	}
		    	else {
		    		mypath=mypath+"\\python-utils";
		    	}
		    	jep.close();
		    	
	        } catch (JepException e) {
	            e.printStackTrace();
	        }
		    JepConfig config = new JepConfig().setRedirectOutputStreams(true).setIncludePath(mypath);
		    try(Jep jep = new Jep(config)){
		    	//System.out.println("my path : "+mypath);
		    	if (package_py!=null) {
		    		for (String mod: package_py) {
		    			String mod_import = "import "+mod;
		    			System.out.println("I will import : "+mod_import);
		    			jep.eval(mod_import);
		    		}
		    	}
		    	jep.eval("import Interpreter_pyy");
		    	jep.set("arg", py_expr);
		    	jep.set("arg2", json.toString());
		    	jep.eval("x = Interpreter_pyy.Interpreter(arg2, arg)");
		    	result = ( Boolean) jep.getValue("x");
		    	//System.out.println("my result : "+result);
		    			    	
	        } catch (JepException e) {
	            e.printStackTrace();
	        }
		return result;

	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List main_list(String expr,Hashtable<String, Hashtable> myenv, String[] package_py) throws Exception  {
		List result = new ArrayList<>();   
		ArrayList result1= null;
		String mypath =path;
		JSONObject json = new JSONObject();
		String py_expr= expr.replace("$", "");
		if(myenv!=null) {
	    
	    json.putAll( myenv);
	    }
		if (mypath==null) {
			try(Jep jep = new Jep(false))
			{		    	
	    		jep.eval("import sys,os");
	    		//jep.eval("pathname = os.path.dirname(sys.argv[0])");
	    	
	    		jep.eval("print (\'full path =\', os.path.abspath(\'\'))");
	    		jep.eval("ipath = os.path.abspath(\'\')");
	    		mypath = (String) jep.getValue("ipath");
	    	
	    		jep.close();
	    	
        	} catch (JepException e) {
        		e.printStackTrace();
        	}
		}
    	else {
    		mypath=mypath+"\\python-utils";
    	}
	    JepConfig config = new JepConfig().setRedirectOutputStreams(true).setIncludePath(mypath);
	    try(Jep jep = new Jep(config))
        {
	    	//prop3.main(jep);
	    	//System.out.println("my path : "+mypath);
	    	jep.eval("import Interpreter_pyy");
	    	if (package_py!=null) {
	    		for (String mod: package_py) {
	    			String mod_import = "import "+mod;
	    			System.out.println("I will import : "+mod_import);
	    			jep.eval(mod_import);
	    		}
	    	}
	    	jep.set("arg", py_expr);
	    	jep.set("arg2", json.toString());
	    	jep.eval("x = Interpreter_pyy.Interpreter(arg2, arg)");
	    	result1 = ( ArrayList) jep.getValue("x");
	    	//System.out.println("my result : "+result1+ " ; "+ result1.size());
	    	result= result1;
	    	// using getValue(String) to invoke methods
	    	//Object result2 = jep.getValue("somePyModule.foo2()");

	    	// using invoke to invoke methods
	    	// jep.eval("foo3 = somePyModule.foo3");
	    	//Object result3 = jep.invoke("foo3", obj);

	    	// using runScript
	    	//jep.runScript("path/To/Script");
        } catch (JepException e) {
            e.printStackTrace();
        }
	return result;

}

}
