/*-
 * #%L
 * This file is part of Partrap IDE
 * Please visit http://vasco.imag.fr/tools/partrap/ for further information
 * 
 * Authors : Yoann Blein , Ansem Ben Cheikh 
 *           Laboratoire d'Informatique de Grenoble, Team VASCO
 * %-
 * Copyright (C) 2017 - 2019 University of Grenoble Alpes
 * %-
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package partrap.util;

import java.util.ArrayList;
import java.util.Hashtable;

public class Test {

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		Expression_interpreter_py test_list= new Expression_interpreter_py(null);
		ArrayList x = (ArrayList) test_list.main_list("[0,1,2,3]", null,null);
 System.out.println(x);
 Hashtable<String, Hashtable> myenv = new Hashtable<String, Hashtable>();
 Hashtable event= new Hashtable<>();
 event.put("id", "EnterState");
 event.put("time", 123456.9);
 event.put("state", "LoadProfile");
 event.put("list", "[5,6,7]");
 //Hashtable<String, Hashtable> bevent= new Hashtable<>();
 myenv.put("ee", event);
 event= new Hashtable<>();
 event.put("id", "Enter");
 event.put("time", 123556.9);
 event.put("state", "LoadP");
 myenv.put("bb", event);
 Expression_interpreter_py interpreter = new Expression_interpreter_py(null);
 ArrayList xs = (ArrayList) interpreter.main_list("ee.list", myenv,null);
 System.out.println(xs);
	}

}
