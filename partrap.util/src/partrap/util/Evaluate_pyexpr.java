/*-
 * #%L
 * This file is part of Partrap IDE
 * Please visit http://vasco.imag.fr/tools/partrap/ for further information
 * 
 * Authors : Yoann Blein , Ansem Ben Cheikh 
 *           Laboratoire d'Informatique de Grenoble, Team VASCO
 * %-
 * Copyright (C) 2017 - 2019 University of Grenoble Alpes
 * %-
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package partrap.util;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import jep.Jep;
import jep.JepException;

public class Evaluate_pyexpr {

	
	public List<Scopereturns> evaluate(List<Scopereturns> mylist,String py_expre, Jep jep,Hashtable<String,?> variables) throws JepException {
		List<Scopereturns> returnlist = new ArrayList<Scopereturns>();
		String py_expr= py_expre.replace("$", "");
		//jep.eval("sqrt(4)");
		//System.out.println("expr to send : " +py_expr);
		if (!mylist.isEmpty()) {
			for (Scopereturns scope: mylist) {
				if(scope!=null) {
					PyExpression_interpreter interpreter = new PyExpression_interpreter(jep);
					boolean result;
					result = interpreter.main( py_expr,scope.environment,variables);
					if(result) {
						returnlist.add(scope);
					}
				}
			}
		}
		return returnlist;
	}
	public List<Scopereturns> evaluate(List<Scopereturns> mylist,String py_expre, String path,String[] package_py) {
		List<Scopereturns> returnlist = new ArrayList<Scopereturns>();
		String py_expr= py_expre.replace("$", "");
		//System.out.println("expr to send : " +py_expr);
		
		if (!mylist.isEmpty()) {
			for (Scopereturns scope: mylist) {
				if(scope!=null) {
					Expression_interpreter_py interpreter = new Expression_interpreter_py(path);
					boolean result;
					result = interpreter.main( py_expr,scope.environment,package_py);
					
					if(result) {
						returnlist.add(scope);
					}
				}
			}
		}
		return returnlist;
	}
	public List<Composite_Result> cevaluate(List<Composite_Result> mylist,String py_expre, Jep jep,Hashtable<String,?> variables) throws JepException {
		List<Composite_Result> returnlist = new ArrayList<>();
		String py_expr= py_expre.replace("$", "");
		//jep.eval("sqrt(4)");
		//System.out.println("expr to send : " +py_expr);
		if (!mylist.isEmpty()) {
			for (Composite_Result cr: mylist) {
				Scopereturns scope= cr.scope;
				if(scope!=null) {
					PyExpression_interpreter interpreter = new PyExpression_interpreter(jep);
					boolean result;
					result = interpreter.main( py_expr,scope.environment,variables);
					if(result) {
						returnlist.add(cr);
					}
				}
			}
		}
		return returnlist;
	}

}

