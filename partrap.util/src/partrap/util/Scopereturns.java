/*-
 * #%L
 * This file is part of Partrap IDE
 * Please visit http://vasco.imag.fr/tools/partrap/ for further information
 * 
 * Authors : Yoann Blein , Ansem Ben Cheikh 
 *           Laboratoire d'Informatique de Grenoble, Team VASCO
 * %-
 * Copyright (C) 2017 - 2019 University of Grenoble Alpes
 * %-
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package partrap.util;

import java.util.Hashtable;
@SuppressWarnings("rawtypes")
public class Scopereturns {
	
	    public final Integer from;
	    public final Integer to;
	    
		public final Hashtable<String,Hashtable> environment;
	    public final Hashtable hevent1;
	    public final Hashtable hevent2;
	   
	   public Scopereturns(Integer  dd, Integer ee, Hashtable<String,Hashtable> n_events,Hashtable heventa,Hashtable heventb) {
	        this.from = dd;
	        this.to= ee;
	        this.environment = n_events;
	        this.hevent1= heventa;
	        this.hevent2=heventb;
	        
	    }
	  
	

}
