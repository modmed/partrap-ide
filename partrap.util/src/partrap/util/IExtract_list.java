/*-
 * #%L
 * This file is part of Partrap IDE
 * Please visit http://vasco.imag.fr/tools/partrap/ for further information
 * 
 * Authors : Yoann Blein , Ansem Ben Cheikh 
 *           Laboratoire d'Informatique de Grenoble, Team VASCO
 * %-
 * Copyright (C) 2017 - 2019 University of Grenoble Alpes
 * %-
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package partrap.util;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

public class IExtract_list {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	@SuppressWarnings("rawtypes")
	public static List<?> extract(Scopereturns scope, String record, String listname) {
		boolean found=false;
		List<?> mylist = new ArrayList<>();
		Hashtable<String,Hashtable> env = scope.environment;
		for (String key : env.keySet()) {
			if (key.equals(record)) {
				
				Hashtable event = env.get(record);
				if (event.containsKey(listname)) {
					if (event.get(listname) instanceof List) {
					found= true;
					mylist = (List<?>) event.get(listname);
					}
					
				}
				
			}
		}
		if (found) {return mylist;}
		else {return null;}
	}

}
