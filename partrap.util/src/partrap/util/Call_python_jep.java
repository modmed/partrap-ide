/*-
 * #%L
 * This file is part of Partrap IDE
 * Please visit http://vasco.imag.fr/tools/partrap/ for further information
 * 
 * Authors : Yoann Blein , Ansem Ben Cheikh 
 *           Laboratoire d'Informatique de Grenoble, Team VASCO
 * %-
 * Copyright (C) 2017 - 2019 University of Grenoble Alpes
 * %-
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package partrap.util;

import java.util.Hashtable;

import org.json.simple.JSONObject;

import jep.Jep;
import jep.JepException;

public class Call_python_jep {

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void main(String[] args) throws JepException {
		
		    //jep.eval("import somePyModule");
		    // any of the following work, these are just pseudo-examples
		    String obj = "(ee.time<=bb.time)";
		    // using eval(String) to invoke methods
		    Hashtable<String, Hashtable> myenv = new Hashtable<String, Hashtable>();
		    Hashtable event= new Hashtable<>();
		    event.put("id", "EnterState");
		    event.put("time", 123456.9);
		    event.put("state", "LoadProfile");
		    //Hashtable<String, Hashtable> bevent= new Hashtable<>();
		    myenv.put("ee", event);
		    event= new Hashtable<>();
		    event.put("id", "Enter");
		    event.put("time", 123556.9);
		    event.put("state", "LoadP");
		    myenv.put("bb", event);
		    JSONObject json = new JSONObject();
		    json.putAll( myenv);
		    String mypath =null;
		    try(Jep jep = new Jep(false))
	        {		    	
		    	jep.eval("import sys,os");
		    	//jep.eval("pathname = os.path.dirname(sys.argv[0])");
		    	jep.eval("print (\'full path =\', os.path.abspath(\'\'))");
		    	jep.eval("ipath = os.path.abspath(\'\')");
		    	mypath = (String) jep.getValue("ipath");
		    	System.out.println("my path : "+mypath);
	        } catch (JepException e) {
	            e.printStackTrace();
	        }
		    try(Jep jep = new Jep(false,mypath))
	        {
		    	jep.eval("import Interpreter_py");
		    	jep.set("arg", obj);
		    	jep.set("arg2", json.toString());
		    	jep.eval("x = Interpreter_py.Interpreter(arg2, arg)");
		    	Object result1 = jep.getValue("x");
		    	System.out.println("my result : "+result1);
		    	// using getValue(String) to invoke methods
		    	//Object result2 = jep.getValue("somePyModule.foo2()");

		    	// using invoke to invoke methods
		    	// jep.eval("foo3 = somePyModule.foo3");
		    	//Object result3 = jep.invoke("foo3", obj);

		    	// using runScript
		    	//jep.runScript("path/To/Script");
	        } catch (JepException e) {
	            e.printStackTrace();
	        }
		

	}

}
