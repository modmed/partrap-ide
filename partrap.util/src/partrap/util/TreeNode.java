/*-
 * #%L
 * This file is part of Partrap IDE
 * Please visit http://vasco.imag.fr/tools/partrap/ for further information
 * 
 * Authors : Yoann Blein , Ansem Ben Cheikh 
 *           Laboratoire d'Informatique de Grenoble, Team VASCO
 * %-
 * Copyright (C) 2017 - 2019 University of Grenoble Alpes
 * %-
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package partrap.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class TreeNode<T>  {

    T data;
    TreeNode<T> parent;
    List<TreeNode<T>> children;
    List<HashMap<String, Boolean>> leaves;

    public TreeNode(T data) {
        this.data = data;
        this.children = new LinkedList<TreeNode<T>>();
        this.leaves= new ArrayList<HashMap<String, Boolean>>();
    }

    public TreeNode<T> addChild(TreeNode<T> child) {
        //TreeNode<T> childNode = new TreeNode<T>(child);
        child.parent = this;
        this.children.add(child);
       // System.out.println(this.data+" add child : "+ child.data);
        return child;
    }
    public void  addleaf(HashMap<String, Boolean> leaf) {
       
        this.leaves.add(leaf);
       // System.out.println(this.data+" add leaf : "+ leaf);
        
    }
    public List<TreeNode<T>> getChildren(){
    	
    	return (List<TreeNode<T>>) this.children;
    }
	public List<HashMap<String, Boolean>> getleaves(){
    	
    	return (List<HashMap<String, Boolean>>) this.leaves;
    }
	public String getdata(){
    	
    	return (String) this.data;
    }



    // other features ...

}

