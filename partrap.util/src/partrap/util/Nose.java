/*-
 * #%L
 * This file is part of Partrap IDE
 * Please visit http://vasco.imag.fr/tools/partrap/ for further information
 * 
 * Authors : Yoann Blein , Ansem Ben Cheikh 
 *           Laboratoire d'Informatique de Grenoble, Team VASCO
 * %-
 * Copyright (C) 2017 - 2019 University of Grenoble Alpes
 * %-
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package partrap.util;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import java.util.logging.StreamHandler;

public class Nose{
    // Obtain a suitable logger.
    /*private static LogRecord logger =LogRecord(Level.INFO, "doing stuff");
    public static void main(String argv[]) {
        // Log a FINE tracing message
        logger= LogRecord(Level.INFO, "doing stuff");
        try{
           String x= "it is "+"me";
           logger.finer(x);
        } catch (Exception ex) {
            // Log the exception
            logger.log(Level.WARNING, "trouble sneezing", ex);
        }
        logger.fine("done");
       // Logger monLog = Logger.getLogger(AppliLogging.class.getName());
        //logger.setLevel(Level.INFO); //pour envoyer les messages de tous les niveaux
       // monLog.setUseParentHandlers(false);
        ConsoleHandler ch = new ConsoleHandler();
        ch.setLevel(Level.INFO); // pour n'accepter que les message de niveau &Ge; INFO
      logger.addHandler(ch);
      // logger.log(Level.INFO, "vvvvvvvvv");
    }*/
	/*public static void main(String [] args){
		   AppliLogging al = new AppliLogging();

		   al.monLog = Logger.getLogger(
		           AppliLogging.class.getName());
		   al.monLog.log(Level.INFO, 
		            démarrage de l'application");
		  
		   al.setVisible(true);
		}*/
	/*private static Logger LOGGER = Logger.getLogger("My Log");
	
	public static void main(String[] args) {
	
	LOGGER.log(Level.INFO,"Lancement de l'application");
	
	} */
	private static Logger LOGGER = Logger.getLogger(Nose.class.getName());
	
	public static void main(String[] args) throws FileNotFoundException, IOException {
	
	/*Handler fh;
	
	try {

	fh = new FileHandler("TestLogging.txt");
	
	LOGGER.addHandler(fh);

	} catch (SecurityException e) {

	LOGGER.severe("Impossible d'associer le FileHandler");

	} catch (IOException e) {

	LOGGER.severe("Impossible d'associer le FileHandler");

	}*/
	/*LOGGER.setLevel(Level.FINE);
	LOGGER.info("Lancement de l'application");
	System.out.println("***************");
	LOGGER.log(Level.INFO,"a debug");
	System.out.println(LOGGER.getLevel());*/
	LOGGER.setLevel(Level.ALL);
	/* ;
   Handler hh = new ConsoleHandler();
   hh.setLevel(Level.WARNING); // pour n'accepter que les message de niveau &Ge; INFO
 LOGGER.addHandler(hh);
    LOGGER.log(Level.WARNING, "config");*/
    MyConsoleHandler ch = new MyConsoleHandler();
    ch.setLevel(Level.INFO); // pour n'accepter que les message de niveau &Ge; INFO
  LOGGER.addHandler(ch);
  LOGGER.info("info");
  LOGGER.log(Level.WARNING, "aconfig");
    //LogRecord vv= new LogRecord(Level.CONFIG, "test");
    
    //ch.publish(vv);
    //System.out.println(ch.getFormatter());
   ObjectOutputStream out = new ObjectOutputStream(
    	    new FileOutputStream("text.txt"));
    StreamHandler ss= new StreamHandler(out, new SimpleFormatter());
    ss.setLevel(Level.FINE); // pour n'accepter que les message de niveau &Ge; INFO
    LOGGER.addHandler(ss);
    LOGGER.fine("Lancement de l'application");
   // LogRecord vvv= new LogRecord(Level.FINEST, "fine test");
   // ss.publish(vvv);
    Handler fh;
   
    try {
       fh = new FileHandler("TestLogging.log");
       fh.setLevel(Level.FINEST);
       LOGGER.addHandler(fh);
       } catch (SecurityException e) {
       LOGGER.severe("Impossible d'associer le FileHandler");
       } catch (IOException e) {
        LOGGER.severe("Impossible d'associer le FileHandler");
        }
    LOGGER.finest("finest");
    LOGGER.fine("fineee");
	}
}
