/*-
 * #%L
 * This file is part of Partrap IDE
 * Please visit http://vasco.imag.fr/tools/partrap/ for further information
 * 
 * Authors : Yoann Blein , Ansem Ben Cheikh 
 *           Laboratoire d'Informatique de Grenoble, Team VASCO
 * %-
 * Copyright (C) 2017 - 2019 University of Grenoble Alpes
 * %-
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package partrap.util;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

public class Remove_event_from_env {
	@SuppressWarnings("rawtypes")
	public Scopereturns remove(Scopereturns scope, String var) {
		
		Hashtable<String, Hashtable> env= scope.environment;
		env.remove(var);
		Scopereturns newscope= new Scopereturns(scope.from, scope.to, env,scope.hevent1,scope.hevent2);
		return newscope;
	}
	
	@SuppressWarnings("rawtypes")
	public List<Scopereturns> remove(List<Scopereturns> scopes, String var) {
		List<Scopereturns> newscopes= new ArrayList<Scopereturns>();
		for(Scopereturns scope : scopes) {
			Hashtable<String, Hashtable> env= scope.environment;
			env.remove(var);
			Scopereturns newscope= new Scopereturns(scope.from, scope.to, env,scope.hevent1,scope.hevent2);
			newscopes.add(newscope);
		}
		return newscopes;
	}
}
