/*-
 * #%L
 * This file is part of Partrap IDE
 * Please visit http://vasco.imag.fr/tools/partrap/ for further information
 * 
 * Authors : Yoann Blein , Ansem Ben Cheikh 
 *           Laboratoire d'Informatique de Grenoble, Team VASCO
 * %-
 * Copyright (C) 2017 - 2019 University of Grenoble Alpes
 * %-
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package partrap.util;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

public class IScope {
	//FileReader trace;
	  public String Type;
	  
	  public Occurrence occur;
	  
	  public IEvent event;
	  
	  public IEvent event2;
	  
	  public Double duration;
	  
	  public String id;
	  
	  public StringBuilder textsolution= new StringBuilder();
	  
	  public IDuration iduration;
	  public boolean realunaryscope = true;// used to test if we are calculating scope for a binary scope so we manage to use the right delimiting events
	  
	  public IScope(String type,Occurrence occurr, IEvent myevent, IEvent myevent2, IDuration dur, String idt, boolean expr ) {
		
		   Type = type;
		  
		   occur = occurr;
		  
		   event = myevent;
		  
		   event2 = myevent2;
		   
		   realunaryscope=expr;
		  
		   iduration=dur;
		   
			  if (dur!=null) {
				  if (dur.unit.toString().equals("d")) {
					  duration = dur.value * 86400;
				  }
				  if (dur.unit.toString().equals("h")) {
					  duration = dur.value * 3600;
				  }
				  if (dur.unit.toString().equals("min")) {
					  duration = dur.value * 60;
				  }
				  if (dur.unit.toString().equals("ms")) {
					  duration = dur.value / 1000;
				  }
				  if (dur.unit.toString().equals("s")) {
					  duration = dur.value;
				  }
			  }
			  else {duration= null;}
		  
		   id = idt;
	
	}
	  
	  
	  @SuppressWarnings({ "rawtypes" })
		public  List<Scopereturns> evaluates( final List<Hashtable> traceList,List<Scopereturns> mylists) throws Exception {
			  List<Scopereturns> returnarray= new ArrayList<Scopereturns>();
			  if (mylists != null) {
			  	  for (Scopereturns myscope: mylists) {
			  		  if(myscope!=null) {
			  		 // System.out.println("Scope to evaluate : "+ Type +" : "+ " from "+ myscope.from +" to " +myscope.to);
			  		  List<Scopereturns> newscope = evaluate(traceList, myscope);
			  		  	if (newscope!=null) {
			  			  returnarray.addAll(newscope);
			  		  	}
			  		  }
			  	  }
			 }
			 return returnarray;
	  }
	  @SuppressWarnings({ "rawtypes" })
		public  List<Composite_Result> cevaluates( final List<Hashtable> traceList,List<Composite_Result> mylists) throws Exception {
			/**
			 * this method evaluates the scope on a list of composite_results(see class Composite_Result) by returning
			 * a new composite_result list also it creates new children for each composite_result
			 * the default verdict for each composite result is true
			 */
			  List<Composite_Result> returnarray= new ArrayList<>();
			  if (mylists != null) {
			  	  for (Composite_Result myscope: mylists) {
			  		  if(myscope!=null) {
			  			  List<Scopereturns> newscope = evaluate(traceList, myscope.scope);
			  			  if (!newscope.isEmpty()) {
			  				List<Composite_Result> newcomposite= create_composite(newscope);
			  				myscope.addAll(newcomposite);
			  				for(Composite_Result xx: newcomposite) {
			  					  xx.parent= myscope;
			  				}
			  				  returnarray.addAll(newcomposite);
			  		  		}
			  			  else {
			  				 Null_Result nr= new Null_Result(myscope,this);
			  				 myscope.add(nr);
			  			  }
			  		  }
			  	  }
			  }
			  return returnarray;
	  }
	  
	public  List<Composite_Result> create_composite( List<Scopereturns> mylists) throws Exception {
		/**
		 * it transforms a list of ScopeReturns to a list of Composite_Result
		 */
		List<Composite_Result> returnarray= new ArrayList<>();
		if (mylists != null) {
			for (Scopereturns myscope: mylists) {
				if(myscope!=null) {
				  	Composite_Result composite_Result= new Composite_Result(myscope, this, true);
				  	returnarray.add(composite_Result);
				}
			}
			return returnarray;
		}
		else {
			return null;
		}
		
	}
	public  List<Composite_Result> icreate_composite( List<Scopereturns> mylists,Composite_Result parent) throws Exception {
		/**
		 * it transforms a list of ScopeReturns to a list of Composite_Result
		 */
		List<Composite_Result> returnarray= new ArrayList<>();
		if (mylists != null) {
			for (Scopereturns myscope: mylists) {
				if(myscope!=null) {
				  	Composite_Result composite_Result= new Composite_Result(myscope, this, true);
				  	composite_Result.parent=parent;
				  	parent.add(composite_Result);
				  	returnarray.add(composite_Result);
				}
			}
			return returnarray;
		}
		else {
			return null;
		}
		
	}
	
			
	  @SuppressWarnings({ "rawtypes" })
	public  List<Scopereturns> evaluate( final List<Hashtable> traceList,Scopereturns scope) throws Exception {
		List<Scopereturns> returnarray= new ArrayList<Scopereturns>();
		int d=scope.from;
		 int f=scope.to;
		 Hashtable<String, Hashtable> myenv =scope.environment;
		if (Type.equals("after")){
/******************************************************************/	    	
	    	
	    	if(occur.toString().equals("each")){
	    		returnarray= after_each(traceList,scope, event);
	    		}
	    	
/********************************************************************************/	  
			if (occur.toString().equals("first")) {
				returnarray= after_first(traceList, scope, event);
	    	}
			
			
/**********************************************************************************************/			
			if (occur.toString().equals("last")) {
				returnarray= after_last(traceList,scope, event);
	    	}
					
				}
/*********************************************************************/
/*********************************************************************/			    
				
		if(Type.equals("before")) {
			if (occur.toString().equals("each")) {
				returnarray= before_each(traceList,scope, event);
	    	}
			
/**********************************************************************/
			
			if (occur.toString().equals("first")) {
				returnarray= before_first(traceList,scope, event);
	    	}
/**************************************************************************/			
			if (occur.toString().equals("last")) {
				returnarray= before_last(traceList,scope, event);
	    		}
		}
/*********************************************************************/
/*********************************************************************/				
		if(Type.equals("given")) {
			
			if (occur.toString().equals("each")) {
				returnarray= event_extract(traceList, d, f, event, myenv);
				}
			
			if (occur.toString().equals("first")) {
				List<Scopereturns> test = event_extract(traceList, d, f, event, myenv);
				if (!test.isEmpty() && test !=null) {
					returnarray.add(test.get(0));
				}
				}
			
			if (occur.toString().equals("last")) {
				List<Scopereturns> test = event_extract(traceList, d, f, event, myenv);
				//System.out.println("given last : "+ test.size());
				if (!test.isEmpty() && test !=null) {
					returnarray.add(test.get(test.size()-1));
				}
				}
			}
		
/*********************************************************************/
/*********************************************************************/		
		if(Type.equals("between")) {
			List<Scopereturns> test= after_each(traceList,scope, event);
			realunaryscope= false;
			for (Scopereturns b_list: test) {
    			if(b_list!=null) {
    				//System.out.println(b_list.from+" to "+ b_list.to+" : "+ b_list.environment);
    				List<Scopereturns> test2= before_first(traceList,b_list, event2);
    				//System.out.println("test 2 : "+test2);
    				if(test2!=null) {
    					returnarray.addAll(test2);
    				}
    			}
    		}
    	}
			
		
		
		if(Type.equals("since")) {
			
			List<Scopereturns> test= after_each(traceList,scope, event);
			realunaryscope= false;
			int from= 0;
    		for (Scopereturns b_list: test) {
    			if(b_list!=null) {
    				List<Scopereturns> test2= before_first(traceList,b_list, event2);
    			if(test2!=null) {
					returnarray.addAll(test2);
				}
    			}
    		}
    			
    			if(!returnarray.isEmpty()) {
    				from = (int)(returnarray.get(returnarray.size()-1).hevent2.get("trace_occ_index"));
    			}
    			System.out.println("from "+from);
    			Scopereturns newscope= new Scopereturns(from, scope.to, scope.environment, scope.hevent1, scope.hevent2);
    			List<Scopereturns> test2= after_each(traceList,newscope, event);
    			System.out.println("test2 "+test2.size());
    			if(test2!=null) {
					returnarray.addAll(test2);
				}
    			
    		
    	}
		
		/******************************************************************/
 	return returnarray;
  	
	}
		
 @SuppressWarnings({ "rawtypes", "unchecked" })
public List<Scopereturns> after_each( final List<Hashtable> traceList, Scopereturns scope,IEvent event) throws Exception {
	 List<Scopereturns> returnlists = new ArrayList<Scopereturns>();
	 int d1=scope.from;
	 int f1=scope.to;
	 Hashtable<String, Hashtable> myenv =scope.environment;
	if(d1!=-1) { 
	    int i = d1;
	    int j;
	    if (f1==-1) {
	    	j= traceList.size()-1;
	    }
	    else {j = f1;}
		while(i<=j){
			Hashtable hevent = traceList.get(i);
			
			int d= -1;
			int f= j;
			if (hevent.get("id").equals(event.id) ){
				if (i==j) {//last element in the trace
			    	Hashtable<String, Hashtable> env = calculate_env(event.var, hevent,i, myenv);
			    	hevent.put("trace_occ_index", i+1);
				    Scopereturns limits_bind = new Scopereturns(-1, -1, env, hevent,null);
				    returnlists.add(limits_bind);
				    //System.out.println(i+", "+j +" : "+ event.id+ " is the last element in the trace");
			    }
				else {
					d= i+1;
					if(duration!=null){
						int k=i+1;
						Hashtable selectedevent= traceList.get(k);
						Double dur=((Number) hevent.get("time")).doubleValue()-((Number) selectedevent.get("time")).doubleValue();
						if (Math.abs(dur)>this.duration) {
							f= -1;
							d=-1;
						}
						else {
							while(k<=j &&  Math.abs(dur)<=duration){
								k++;
								if (k<=j) {selectedevent= traceList.get(k);
								}
									dur= ((Number) hevent.get("time")).doubleValue()-((Number) selectedevent.get("time")).doubleValue();
							}
								f=k-1;
						}
					}
					if (d!=-1) {
						Hashtable<String, Hashtable> env = calculate_env(event.var, hevent,i, myenv);
						hevent.put("trace_occ_index", i+1);
					    Scopereturns limits_bind = new Scopereturns(d, f, env, hevent, null);
						returnlists.add(limits_bind);
					}  
				}
			}
			i++;
		}
		//display_scope("after", "each",  event.id, returnlists);
	return returnlists;
 }
 /********* null entries*/
 //else {return null;}
	return returnlists;
	 
 }

/**
 * @throws Exception **************************************************/
 @SuppressWarnings({ "rawtypes", "unchecked" })
public List<Scopereturns> after_first( final List<Hashtable> traceList, Scopereturns scope, IEvent event) throws Exception {
	 List<Scopereturns> returnlists = new ArrayList<Scopereturns>();
	 int d1=scope.from;
	 int f1=scope.to;
	 Hashtable<String, Hashtable> myenv =scope.environment;
	 if (d1!=-1) {
	    int i = d1;
	    int j;
	    if (f1==-1) {
	    	j= traceList.size()-1;
	    }
	    else {j = f1;}
	    Hashtable hevent = traceList.get(i);
		while(i<j && (! hevent.get("id").equals(event.id))){
			i++;
			hevent = traceList.get(i);
			}
			
		if (hevent.get("id").equals(event.id)){
			int occ=i;
			if (i==j) {//last element in the trace
				Hashtable<String, Hashtable> env = calculate_env(event.var, hevent,occ, myenv);
				hevent.put("trace_occ_index", occ+1);
			    Scopereturns limits_bind = new Scopereturns(-1, -1, env, hevent, null);
			    returnlists.add(limits_bind);
				//System.out.println(i+", "+j +" : the first "+ event.id+ " is the last element in the trace");
			  }
			else {
				i++;
				int d= i;
				int f= j;	
				if(this.duration!=null){
		    		Hashtable selectedevent= traceList.get(i);
					Double dur= ((Number) hevent.get("time")).doubleValue()-((Number) selectedevent.get("time")).doubleValue();
					if (Math.abs(dur)>duration) {
						d= -1;
						f=-1;
					}
					else {
						while( i<=j &&  Math.abs(dur)<=duration){
								i++;
    							if (i<=j) {selectedevent= traceList.get(i);}
    							dur= ((Number) hevent.get("time")).doubleValue()-((Number) selectedevent.get("time")).doubleValue();
    						}
						f=i-1;
						}
					}
					if(d!=-1) {
						Hashtable<String, Hashtable> env = calculate_env(event.var, hevent,occ, myenv);
						hevent.put("trace_occ_index", occ+1);
					    Scopereturns limits_bind = new Scopereturns(d, f, env, hevent, null);
						returnlists.add(limits_bind);
					}
				    i=j;
				}
			}
		//else {
		//	returnlists=null;
		//}
		
		//display_scope("after","first", event.id, returnlists);
	//return returnlists;
 }
 /********* null entries*/
// else {return null;}
	 return returnlists;
 }
 
 /**
 * @throws Exception *****************************************/
 @SuppressWarnings({ "rawtypes", "unchecked" })
public List<Scopereturns> after_last( final List<Hashtable> traceList, Scopereturns scope, IEvent event) throws Exception {
	 List<Scopereturns> returnlists = new ArrayList<Scopereturns>();
	    List<Integer> event_occ = new ArrayList<Integer>();
	    int d1=scope.from;
		int f1=scope.to;
		Hashtable<String, Hashtable> myenv =scope.environment;
	    if (d1!=-1) {
		int i = d1;
	    int j;
	    if (f1==-1) {
	    	j= traceList.size()-1;
	    }
	    else {j = f1;}
	    int d=-1;
		int f=-1;
		Hashtable hevent = traceList.get(i);
		if (traceList.get(j).get("id").equals(event.id)) {//last element in the trace
	    	hevent = traceList.get(j);
	    	Hashtable<String, Hashtable> env = calculate_env(event.var, hevent,j, myenv);
	    	hevent.put("trace_occ_index", j+1);
		    Scopereturns limits_bind = new Scopereturns(-1, -1, env, hevent, null);
		    returnlists.add(limits_bind);
		    //System.out.println(i+", "+j +" : "+ event.id+ " is the last element in the trace");
	    }
	    else {
    		while(i<=j){
    			hevent = traceList.get(i);
    			if (hevent.get("id").equals(event.id)){
    				event_occ.add(i);
    			}
    			i++;
    		}
    		if(!event_occ.isEmpty()) {
    			i = event_occ.get(event_occ.size()-1);
    			hevent = traceList.get(i);
    			int occ =i;
    			i++;
    			if(this.duration==null) {
    				d=i;
    				f=j;
    			}
    			else {
    				Hashtable selectedevent= traceList.get(i);
    				Double dur= ((Number) hevent.get("time")).doubleValue()-((Number) selectedevent.get("time")).doubleValue();
    				if (Math.abs(dur)<=duration) {d = i;
    				}
    				while( i<=j &&  Math.abs(dur)<=duration){
    					i++;
    					if (i<=j) {selectedevent= traceList.get(i);}
    						dur= ((Number) hevent.get("time")).doubleValue()-((Number) selectedevent.get("time")).doubleValue();
    				}
    				f=i-1;
    			}
    			if(d!=-1) {
    				Hashtable<String, Hashtable> env = calculate_env(event.var, hevent,occ, myenv);
    				hevent.put("trace_occ_index", occ+1);
    			    Scopereturns limits_bind = new Scopereturns(d, f, env, hevent, null);
    				returnlists.add(limits_bind);
			   }
    		}
    		
	    }
			//return returnlists;
	    }
	    return returnlists;
	    //display_scope("after","last",  event.id, returnlists);
	    
 }
	  
 /**
 * @throws Exception *****************************************/
 @SuppressWarnings({ "rawtypes", "unchecked" })
public List<Scopereturns> before_each( final List<Hashtable> traceList, Scopereturns scope, IEvent event) throws Exception {
	    List<Scopereturns> returnlists = new ArrayList<Scopereturns>();
	    int d1=scope.from;
		int f1=scope.to;
		Hashtable<String, Hashtable> myenv =scope.environment;
	    if (d1!=-1) {
	    	int i = d1;
	    	int j;
	    	if (f1==-1) {
	    		j= traceList.size()-1;
	    	}
	    	else {j = f1;}
	  
	    	if (traceList.get(d1).get("id").equals(event.id)) {//first element in the trace
	    		Hashtable hevent = traceList.get(d1);
	    		Hashtable<String, Hashtable> env = calculate_env(event.var, hevent,d1, myenv);
	    		hevent.put("trace_occ_index", d1+1);
	    		if (realunaryscope) {
	    			Scopereturns limits_bind = new Scopereturns(-1, -1, env, hevent, null);
	    			returnlists.add(limits_bind);}
	    		else {
	    			Scopereturns limits_bind = new Scopereturns(-1, -1, env, scope.hevent1, hevent);
		    		returnlists.add(limits_bind);
	    		}
	    		//System.out.println(i+", "+j +" : "+ event.id+ " is the first element in the trace");
	    	}
	    	i++;
	    	while (i<=j ) {
	    		int d=-1;
	    		int f=-1;
	    		Hashtable selectedevent = traceList.get(i);		
	    		Hashtable hevent = traceList.get(i);
				if (selectedevent.get("id").equals(event.id)) {
					int occ=i;
					hevent= selectedevent;
					f=i-1;
					if (duration==null) {
						d=d1;
					}
					else {
						int k= f;
						selectedevent = traceList.get(k);
						Double dur= ((Number) selectedevent.get("time")).doubleValue()-((Number) hevent.get("time")).doubleValue();
						if (Math.abs(dur)>this.duration) {f=-1;
							d=-1;}
						else {
							while (k>=d1 && Math.abs(dur)<=duration) {
								selectedevent = (Hashtable) traceList.get(k);
								dur= ((Number) selectedevent.get("time")).doubleValue()-((Number) hevent.get("time")).doubleValue();
								
								k--;
							}
							d=k+1;
						}
					}
					Hashtable<String, Hashtable> env = calculate_env(event.var, hevent,occ, myenv);
					hevent.put("trace_occ_index", occ+1);
					if (realunaryscope) {
					    Scopereturns limits_bind = new Scopereturns(d, f, env,hevent, null);
					    returnlists.add(limits_bind);
					}
					else {
						Scopereturns limits_bind = new Scopereturns(d, f, env,scope.hevent1, hevent);
					    returnlists.add(limits_bind);
					}
				}
				i++;
			}
	    //display_scope("before", "each", event.id, returnlists);
	   // return returnlists;
	    }
	    /********* null entries*/
	    //else {return null;}
	    return returnlists;
	    
 }
	    
/**
 * @throws Exception *******************************************************/
 @SuppressWarnings({ "rawtypes", "unchecked" })
 public List<Scopereturns> before_first( final List<Hashtable> traceList, Scopereturns scope, IEvent event) throws Exception {
 	    List<Scopereturns> returnlists = new ArrayList<Scopereturns>();
 	    int d1=scope.from;
 		int f1=scope.to;
 		Hashtable<String, Hashtable> myenv =scope.environment;
 	    if (d1!=-1) {
 	    	int i = d1;
 	    	int j;
 	    	if (f1==-1) {
 	    		j= traceList.size()-1;
 	    	}
 	    	else {j = f1;}
 	    	int d=-1;
 	    	int f=-1;
		
 	    	Hashtable selectedevent = traceList.get(i);
 	    	if (selectedevent.get("id").equals(event.id)) {
 	    		Hashtable<String, Hashtable> env = calculate_env(event.var, selectedevent,i, myenv);
 	    		selectedevent.put("trace_occ_index", i+1);
 	    		if (realunaryscope) {
 	    			Scopereturns limits_bind = new Scopereturns(-1, -1, env, selectedevent, null);
 	 	    		returnlists.add(limits_bind);
 	    		}
 	    		else {
 	    			Scopereturns limits_bind = new Scopereturns(-1, -1, env,scope.hevent1,selectedevent);
 	    			returnlists.add(limits_bind);
 	    		}
 	    		//System.out.println(i+", "+j +" : "+ event.id+ " is the first element in the trace");
 	    	}
 	    
 	    	else {
 	    		while (i<j && !selectedevent.get("id").equals(event.id)) {
 	    			i++;
 	    			selectedevent = traceList.get(i);
 	    		}
 	    		if (selectedevent.get("id").equals(event.id)) {
 	    			Hashtable hevent = traceList.get(i);
 	    			int occ=i;
 	    			f=i-1;
 	    			if (duration==null) {
						d=d1;
					    }
 	    			else {
 	    				int k= f;
 	    				selectedevent = traceList.get(k);
 	    				Double dur= ((Number) selectedevent.get("time")).doubleValue()-((Number) hevent.get("time")).doubleValue();
 	    				if (Math.abs(dur)>duration) {
 	    					f=-1;
 	    					d=-1;
						}
 	    				else {
 	    					while (k>=d1 && Math.abs(dur)<=duration) {
 	    						k--;
 	    						if (k>=d1) {selectedevent = (Hashtable) traceList.get(k);
 	    							dur= ((Number) selectedevent.get("time")).doubleValue()-((Number) hevent.get("time")).doubleValue();}
 	    							//System.out.println("this is k : "+ k + ",  dur : "+dur);
 	    						}
 	    					d=k+1;
 	    				}
 	    			}
 	    			//System.out.println("var :"+event.var);
 	    			Hashtable<String, Hashtable> env = calculate_env(event.var, hevent,occ, myenv);
 	    			hevent.put("trace_occ_index", occ+1);
 	    			if (realunaryscope) {
		    			Scopereturns limits_bind = new Scopereturns(d, f, env, hevent, null);
		    			returnlists.add(limits_bind);
		    			}
		    		else {
		    			Scopereturns limits_bind = new Scopereturns(d, f, env, scope.hevent1, hevent);
			    		returnlists.add(limits_bind);
		    		}
 				    
				 }
 	    		//else {
 	    		//	returnlists=null;
 	    		//}
 	    	}
 	    	//display_scope("before","first", event.id, returnlists);
 	    	//return returnlists;
	    }
	    /********* null entries*/
	    //else {return null;}
 	   return returnlists;
 }
 
 /**
 * @throws Exception ***********************************************************************/
 @SuppressWarnings({ "rawtypes", "unchecked" })
public List<Scopereturns> before_last( final List<Hashtable> traceList, Scopereturns scope, IEvent event) throws Exception {
	    List<Scopereturns> returnlists = new ArrayList<Scopereturns>();
	    int d1=scope.from;
		int f1=scope.to;
		Hashtable<String, Hashtable> myenv =scope.environment;
	    if (d1!=-1) {
	    	int i = d1;
	    	int j;
	    	if (f1==-1) {
	    		j= traceList.size()-1;
	    	}
	    	else {j = f1;}
	    	int d=-1;
	    	int f=-1;
	    	List<Integer> event_occ = new ArrayList<Integer>();
		/*************** look for event occurrences to choose the last one*/
	    	while(i<=j){
	    		Hashtable hevent = traceList.get(i);
	    		if (hevent.get("id").equals(event.id)){
	    			event_occ.add(i);
	    		}
	    		i++;
	    	}
	    	if (!event_occ.isEmpty()) {
	    		i = event_occ.get(event_occ.size()-1);
	    		Hashtable hevent = traceList.get(i);
	    		if (i==d1) {// the last occurrence is the first element in the trace
	    			Hashtable<String, Hashtable> env = calculate_env(event.var, hevent,i, myenv);
	    			hevent.put("trace_occ_index",i+1);
	    			if (realunaryscope) {
		    			Scopereturns limits_bind = new Scopereturns(-1, -1, env, hevent, null);
		    			returnlists.add(limits_bind);}
		    		else {
		    			Scopereturns limits_bind = new Scopereturns(-1, -1, env, scope.hevent1, hevent);
			    		returnlists.add(limits_bind);
		    		}
	    		}
	    		else {
	    			d=d1;
	    			f=i-1;
	    			if(duration!=null) {
	    				int k= f;
	    				Hashtable selectedevent = traceList.get(k);
	    				Double dur= ((Number) selectedevent.get("time")).doubleValue()-((Number) hevent.get("time")).doubleValue();
	    				if (Math.abs(dur)>duration) {
	    					f=-1;
	    					d=-1;
	    					}
	    				else {
	    					while (k>=d1 && Math.abs(dur)<=duration) {
	    						k--;
	    						if (k>=d1) {selectedevent = (Hashtable) traceList.get(k);
	    						dur= ((Number) selectedevent.get("time")).doubleValue()-((Number) hevent.get("time")).doubleValue();}
	    						//System.out.println("this is k : "+ k + ",  dur : "+dur);
	    					}
	    					d=k+1;
	    				}
	    			}
	    			Hashtable<String, Hashtable> env = calculate_env(event.var, hevent,i, myenv);
	    			hevent.put("trace_occ_index",i+1);
	    			if (realunaryscope) {
		    			Scopereturns limits_bind = new Scopereturns(d, f, env, hevent, null);
		    			returnlists.add(limits_bind);}
		    		else {
		    			Scopereturns limits_bind = new Scopereturns(d, f, env, scope.hevent1, hevent);
			    		returnlists.add(limits_bind);
		    		}
	    		}
	    	}
	    	//else {
	    	//	returnlists = null;
	    	//}
		
	    //display_scope("before", "last", event.id, returnlists);
	    	//return returnlists;
	    }
	    /********* null entries*/
	    //else {return null;}
	    return returnlists;
 }
 @SuppressWarnings({ "rawtypes", "unchecked" })
public static List<Scopereturns> event_extract( final List<Hashtable> traceList, Integer d1, Integer f1, IEvent event,Hashtable<String,Hashtable> myenv) throws Exception {
	    List<Scopereturns> returnlists = new ArrayList<Scopereturns>();
	   
	    if (d1!=-1) {
	    	int i = d1;
	    	int j;
	    	if (f1==-1) {
	    		j=traceList.size()-1;
	    	}
	    	else {j = f1;}
		
	    	while(i<=j){
	    		Hashtable hevent = traceList.get(i);
	    		if (hevent.get("id").equals(event.id)){
	    			Hashtable<String, Hashtable> env = calculate_env(event.var, hevent,i, myenv);
	    			hevent.put("trace_occ_index",i+1);
	    			Scopereturns limits_bind = new Scopereturns(d1, j, env, hevent, null);
	    			returnlists.add(limits_bind);
	    		}
	    		i++;
	    	}
	    	
	    }
	    /********* null entries*/
	   // else {return null;}
	    return returnlists;
 }
 
 /**********************************************************/
 public static void display_scope(String name, String occ, String event_id,List<Scopereturns> returnlists) {
	  
		int t=1;
		String named= name;
		if (occ!=null) {
			named = named+" "+occ;
		}
		System.out.println("Scopes : '"+ named+"' "+event_id+ " : ");
		if (!returnlists.isEmpty()) {
		for (Scopereturns scope: returnlists) {
			if(scope!=null) {
				if (scope.environment!=null) {
					System.out.println(t+" : from "+ scope.from+ " to " + scope.to +", environment size : " + scope.environment.size()+ ", environment list : "+ scope.environment.toString());
				}
				else {
					System.out.println(t+" : from "+ scope.from+ " to " + scope.to +", null environment ");	
				}	
			}
			else {
				System.out.println(t+" : empty scope ");
			}
			t++;
		}
		}
 }
  @SuppressWarnings({ "rawtypes", "unchecked" })
private static Hashtable<String, Hashtable> calculate_env(String event_var, Hashtable hevent,int occ,final Hashtable<String, Hashtable> myenv) throws Exception{
	  Hashtable<String, Hashtable> env = new Hashtable<String, Hashtable>();
	    String name;
	    hevent.put("trace_occ_index", occ+1);
	    if (event_var == null) {
	    	 if (myenv !=null) {
			    	env.putAll(myenv);
			    }
	    }
	    else {
	    	name = event_var;
	    	if (myenv ==null) {
	    		env.put(name, hevent);
	    	}
	    	else {
	    		if(myenv.containsKey(name)) {
	    			System.out.println("Error in event declaration this event name already exists : "+ name);
	    			throw new Exception("event name : "+ name);
	    		}
	    		else {
	    			env.putAll(myenv);
	    			env.put(name, hevent);
	    		}
	    	}
	    }
	    return env;
  }
			

}

