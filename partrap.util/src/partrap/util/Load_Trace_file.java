/*-
 * #%L
 * This file is part of Partrap IDE
 * Please visit http://vasco.imag.fr/tools/partrap/ for further information
 * 
 * Authors : Yoann Blein , Ansem Ben Cheikh 
 *           Laboratoire d'Informatique de Grenoble, Team VASCO
 * %-
 * Copyright (C) 2017 - 2019 University of Grenoble Alpes
 * %-
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package partrap.util;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

public class Load_Trace_file {
	public boolean main(String path) {
		System.out.println("1 firstis  : "+ path);
		JFileChooser chooser = chooser();//choose a trace file or a directory of traces
	    
        try {
        	File file = chooser.getSelectedFile();
        	System.out.println("1 : "+ path);
        	if (file.isDirectory()){
    			//var filter = new FileNameExtensionFilter("Json", "json");
        		System.out.println("2 : "+ path);
    			 File[] file_list= file.listFiles(new FilenameFilter() {
    				 @Override
    				 public boolean accept(File file, String name) {
    					 return name.toLowerCase().endsWith(".json");
    				 }
    			 });
    			 // while is used to guarantee the choice of a directory containing json files
    			 while(file_list.length==0 && file.isDirectory()) {
    				 chooser = chooser();
    				 file = chooser.getSelectedFile();
 	            	if (file.isDirectory()){
 	        			// apply a filter on files selection : only json files are accepted
 	            		file_list=null;
 	        			 file_list= file.listFiles(new FilenameFilter() {
 	        				 @Override
 	        				 public boolean accept(File file, String name) {
 	        					 return name.toLowerCase().endsWith(".json");
 	        				 }
 	        			 });
 	            	}
    			 }
        	}
        	//System.out.println("3 : "+ path);
           	String filepath= chooser.getSelectedFile().getAbsolutePath();
       		String filepath2=filepath.replace("\\","/");
       		//System.out.println("mydsl load path : "+ path);
       		FileWriter fw= new FileWriter(path);
       		fw.write("");
       		
               BufferedWriter out = new BufferedWriter(fw);
               out.flush();
               out.write(filepath2);
               out.close();
           }
           catch (IOException e)
           {
               System.out.println("Exception ");       
           }
        return true;
	}
	/*************************/
	 private static void setFileChooserFont(Component[] comp)
     {  
     	Font font = new Font("SanSerif",Font.PLAIN,15);
       for(int x = 0; x < comp.length; x++)  
       {  
         if(comp[x] instanceof Container) setFileChooserFont(((Container)comp[x]).getComponents());  
         try{comp[x].setFont(font);}  
         catch(Exception e){}//do nothing  
       }  
     }  
     /*****************************/
     private static JFileChooser chooser(){
 		
 		JFileChooser chooser = new JFileChooser();
 		chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
         FileNameExtensionFilter filter = new FileNameExtensionFilter("Json", "json");
         chooser.setFileFilter(filter);
         chooser.setPreferredSize(new Dimension(1200, 800));
         chooser.setDialogTitle("Choose a Trace file or a directory");
         setFileChooserFont(chooser.getComponents());
         int returnVal = chooser.showOpenDialog(null);
         if(returnVal == JFileChooser.APPROVE_OPTION) {
             System.out.println("You chose to open this file: " + chooser.getSelectedFile().getName());
             return chooser;
 		
         }
         else {
         	return null;
         }
 	}

}
