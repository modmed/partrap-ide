/*-
 * #%L
 * This file is part of Partrap IDE
 * Please visit http://vasco.imag.fr/tools/partrap/ for further information
 * 
 * Authors : Yoann Blein , Ansem Ben Cheikh 
 *           Laboratoire d'Informatique de Grenoble, Team VASCO
 * %-
 * Copyright (C) 2017 - 2019 University of Grenoble Alpes
 * %-
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package partrap.util;

import java.util.Hashtable;
import java.util.List;

public class Occurrence_display {

	@SuppressWarnings("rawtypes")
	public static void display(List<Scopereturns> occ_list) {
		
		if (occ_list.isEmpty()) {
			System.out.println("No event occurrence is found");
		}
		else {
			//System.out.println("event occurrences are :");
		for (Scopereturns scope: occ_list) {
			if(scope!=null) {
				if (scope.environment!=null) {
				Hashtable<String,Hashtable> myenv= scope.environment;
				for (String key: myenv.keySet()) {
					Hashtable hevent = myenv.get(key);
					if (hevent.containsKey("trace_occ_index")) {
					Integer index = (Integer) hevent.get("trace_occ_index");
					//hevent.remove("trace_occ_index");
					System.out.println(key +": index in the trace : "+ index+ " : "+ hevent);
					}
				}
				
				}
				
				}
				
		}
		}

	}
}
