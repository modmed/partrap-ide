/*-
 * #%L
 * This file is part of Partrap IDE
 * Please visit http://vasco.imag.fr/tools/partrap/ for further information
 * 
 * Authors : Yoann Blein , Ansem Ben Cheikh 
 *           Laboratoire d'Informatique de Grenoble, Team VASCO
 * %-
 * Copyright (C) 2017 - 2019 University of Grenoble Alpes
 * %-
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package partrap.util;

public class Equiv_Result extends Composite_Issue {
	public Result left;
    public Result right;
		
    public Equiv_Result(Result iright, Result ileft, boolean verdictx) {
	    	left= ileft;
	    	right= iright;
	    	children.add(left);
	    	children.add(right);
	    	verdict= verdictx;
	    	
	}
		
		public StringBuilder  traverse(StringBuilder initial,int i,boolean view) {
	    	StringBuilder resulttext= initial;
	    	//List<Result> children;
	    	if(view || !verdict) {
	    		if(verdict){
	    			//Composite_Result resultl= (Composite_Result)result;
	    			for(int j=1;j<=i; j++) {
	    				initial.append("--");
	    			}
	    			i++;
	    			resulttext.append("Equiv Property: "+verdict);
	    			if(!right.verdict && !left.verdict) {
	    				resulttext.append("True because the two properties are false:");
	    				resulttext.append(right.traverse( new StringBuilder(),i,false).toString()+"\nAND\n"+left.traverse( new StringBuilder(),i,false).toString());
		    		
	    			}
	    			if(right.verdict && left.verdict) {
	    				resulttext.append("True because the two properties are true:");
	    				resulttext.append(right.traverse( new StringBuilder(),i,true).toString()+"\nAND\n"+left.traverse( new StringBuilder(),i,true).toString());
	    			}
	    			
	    			//result.traverse( resulttext,i);
	    		}
	    		else {
	    			for(int j=1;j<=i; j++) {
	    				initial.append("--");
	    			}
	    			i++;
	    			resulttext.append("Equiv Property: "+verdict);
	    			if(right.verdict) {
	    				resulttext.append("False because the first property is false while the second is true:\n");
	    				resulttext.append(left.traverse( new StringBuilder(),i,false).toString());
	    			}
	    			else {
	    				resulttext.append("False because the first property is true while the second is false:\n");
	    				resulttext.append(right.traverse(new StringBuilder(),i,false).toString());
		    		
	    			}
	    		}
	    	}
	    	
	    return resulttext;
	    }
	
	}

