/*-
 * #%L
 * This file is part of Partrap IDE
 * Please visit http://vasco.imag.fr/tools/partrap/ for further information
 * 
 * Authors : Yoann Blein , Ansem Ben Cheikh 
 *           Laboratoire d'Informatique de Grenoble, Team VASCO
 * %-
 * Copyright (C) 2017 - 2019 University of Grenoble Alpes
 * %-
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package partrap.util;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

public class Send_recieve implements Runnable{
    private Socket socket = null;

    private BufferedReader in = null;
    //private String message=null;
    private PrintWriter out = null;
	private Thread t1, t2, t3,t4;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public void run() {
		
		 try {

		        socket = new Socket("localhost",1981);
		        in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		        out = new PrintWriter( socket.getOutputStream());
		        List<Hashtable<String, Hashtable>> myenv = new ArrayList<Hashtable<String, Hashtable>>();
		        Hashtable event= new Hashtable<>();
		        event.put("id", "EnterState");
		        event.put("time", 123456.9);
		        event.put("state", "LoadProfile");
		        Hashtable<String, Hashtable> bevent= new Hashtable<>();
		        bevent.put("ee", event);
		        myenv.add(bevent);
		        String python_cond = "ee.state=='LoadProfile'";
		        //out = new PrintWriter(socketduserveur.getOutputStream());
		        
		        t2= new Thread(new Emission(out,python_cond));
				t2.start();
				try {
					t2.join();
				} catch (InterruptedException e) {
					
					e.printStackTrace();
				}
				System.out.println("condition sent");
				t4= new Thread(new Reception(in));
				System.out.println("1");
				t4.start();
				System.out.println("2");
				try {
					System.out.println("3");
					t4.join();
					System.out.println("4");
				} catch (InterruptedException e) {
					
					e.printStackTrace();
				}
				t3= new Thread(new Emission(out,"environment"));
				t3.start();
				try {
					t3.join();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				 System.out.println("env sent");
				t1= new Thread(new Reception(in));
				t1.start();
				try {
					t1.join();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		       
		        socket.close();

		        } catch (IOException e) {
		        	e.printStackTrace();

		        }
		
	}

}
