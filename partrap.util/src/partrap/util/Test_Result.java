/*-
 * #%L
 * This file is part of Partrap IDE
 * Please visit http://vasco.imag.fr/tools/partrap/ for further information
 * 
 * Authors : Yoann Blein , Ansem Ben Cheikh 
 *           Laboratoire d'Informatique de Grenoble, Team VASCO
 * %-
 * Copyright (C) 2017 - 2019 University of Grenoble Alpes
 * %-
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package partrap.util;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

public class Test_Result {

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//String itrace_path = "C:\\\\Users\\\\vasco\\\\Documents\\\\mytraces\\\\long_traces\\\\bo1.json";
		//String iprop_name="prop";
		IEvent ievent1 = new IEvent("Enter", "ee", "");
		IPattern ipattern= new IPattern( "occurrence_of", ievent1, null, null,1);
		List<Hashtable> occurences = new ArrayList<>();
		 Hashtable event= new Hashtable<>();
		 event.put("id", "EnterState");
		 event.put("time", 123456.9);
		 event.put("state", "LoadProfile");
		 event.put("list", "[5,6,7]");
		 Scopereturns scoper= new Scopereturns(0, 5, event, null, null);
			
		 //Hashtable<String, Hashtable> bevent= new Hashtable<>();
		 occurences.add( event);
		 event= new Hashtable<>();
		 event.put("id", "Enter");
		 event.put("time", 123556.9);
		 event.put("state", "LoadP");
		 occurences.add( event);
		 IScope iscopex = new IScope("after", Occurrence.each, ievent1, null, null, null, true);
		Low_Result lr= new Low_Result(ipattern, occurences, scoper);
		Composite_Result cResult = new Composite_Result(scoper, iscopex, true);
		cResult.add(lr);
		Composite_Result cResult1 = new Composite_Result(scoper, iscopex, true);
		cResult1.add(cResult);
		Composite_Result cResult2 = new Composite_Result(scoper, iscopex, true);
		cResult2.add(cResult);
		cResult2.traverse(new StringBuilder(), 0, true);
		List<Result> isub_results= new ArrayList<>();
		isub_results.add(cResult1);
		isub_results.add(cResult2);
		isub_results.add(cResult);
		//Final_Result fr= new Final_Result(itrace_path, iprop_name, isub_results, true);
		StringBuilder initial= new StringBuilder();
		
		System.out.println(initial.toString());
	}

}
